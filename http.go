package xhttp

import (
	"bytes"
	"crypto/tls"
	"crypto/x509"
	"encoding/json"
	"errors"
	"fmt"
	"gitee.com/xlm516/http/common"
	"gitee.com/xlm516/http/gohttp/gohttpserver"
	"gitee.com/xlm516/xtool/cast"
	"gitee.com/xlm516/xtool/constvar"
	"gitee.com/xlm516/xtool/dbg"
	xfile "gitee.com/xlm516/xtool/file"
	xlist "gitee.com/xlm516/xtool/list"
	xstring "gitee.com/xlm516/xtool/string"
	"gitee.com/xlm516/xtool/sys"
	xtime "gitee.com/xlm516/xtool/time"
	"html/template"
	"io/ioutil"
	"mime/multipart"
	"net"
	"net/url"
	"os"
	"reflect"
	"sort"
	"strings"
	"sync"
)

func IsHttpUrlError(err error) bool {
	var urlErr *url.Error
	ok := errors.As(err, &urlErr)
	if ok {
		if urlErr.Timeout() == false {
			return true
		}
		return urlErr.Timeout()
	}
	return false
}

func IsHttpTimeout(err error) bool {
	var urlErr *url.Error
	ok := errors.As(err, &urlErr)
	if ok {
		return urlErr.Timeout()
	}
	return false
}

func IsHttpConnFailed(err error) bool {
	return IsHttpUrlError(err)
}

type FileHandler struct {
	Name        string
	Uri         string
	Type        string
	Engine      string
	FilePath    string
	File404     string
	LoginScript string //登陆再次通过脚本确认
	NeedLogin   bool
	LoginPage   string
	Lang        string
	SkipLogin   []string
	FileHash    bool
	Callback    RouterCallback `json:"-"`
}

type ECHttpConfig struct {
	FastHttp bool
	File     []*FileHandler
	Http     []*ECHttpBinds
	Https    []*ECHttpBinds
	MemDB    struct {
		Url  string
		User string
		Pwd  string
	}
	filename string
}
type FileHandlerSlice []*FileHandler

func (s FileHandlerSlice) Len() int      { return len(s) }
func (s FileHandlerSlice) Swap(i, j int) { s[i], s[j] = s[j], s[i] }
func (s FileHandlerSlice) Less(i, j int) bool {
	Url1 := ""
	Url2 := ""
	Name1 := ""
	Name2 := ""
	tmpStr := s[i].Uri
	index := strings.IndexByte(tmpStr, '@')
	if index >= 0 {
		Name1 = tmpStr[index+1:]
		Url1 = tmpStr[:index]
	}
	tmpStr = s[j].Uri
	index = strings.IndexByte(tmpStr, '@')
	if index >= 0 {
		Name2 = tmpStr[index+1:]
		Url2 = tmpStr[:index]
	}
	if Name1 > Name2 {
		return false
	}
	if Name1 == Name2 && Url1 > Url2 {
		return false
	}
	return true
}
func (me *ECHttpConfig) SaveToFile(filename string) {
	if filename == "" {
		filename = me.filename
	}
	b, _ := json.MarshalIndent(me, "", "  ")
	if b != nil {
		_ = ioutil.WriteFile(filename, b, 0666)
	}
}
func (me *ECHttpConfig) Sort() {
	sort.Sort(FileHandlerSlice(me.File))
}
func (me *ECHttpConfig) FastConfig(bind string, publicDir string, login bool, loginPage string, skipLogin []string) {
	if skipLogin == nil {
		skipLogin = make([]string, 0)
	}
	if loginPage == "" {
		loginPage = "login"
	}
	if len(me.Http) > 0 {
		me.Http[0].Bind = bind
		me.Http[0].Name = ""
	} else {
		h := new(ECHttpBinds)
		me.Http = append(me.Http, h)
		me.Http[0].Bind = bind
		me.Http[0].Name = ""
	}
	publicUrl := "/public"
	index := strings.IndexByte(publicDir, '@')
	if index >= 0 {
		publicUrl = publicDir[index+1:]
		publicDir = publicDir[:index]
	}
	me.File = make([]*FileHandler, 3)
	for k, _ := range me.File {
		me.File[k] = new(FileHandler)
	}
	me.File[0].FilePath = constvar.ApplicationPath
	me.File[0].Type = "file"
	me.File[0].NeedLogin = login
	me.File[0].Uri = "/"
	me.File[0].SkipLogin = skipLogin
	me.File[0].LoginPage = loginPage
	if xfile.FileExist(constvar.ApplicationPath + "/gvm") {
		me.File[1].FilePath = constvar.ApplicationPath + "/gvm"
		me.File[1].Type = "go"
		me.File[1].NeedLogin = login
		me.File[1].Uri = "/gvm"
		me.File[1].SkipLogin = skipLogin
		me.File[1].LoginPage = loginPage
	} else {
		me.File[1].FilePath = constvar.ApplicationPath + "/gvm"
		me.File[1].Type = "go"
		me.File[1].NeedLogin = login
		me.File[1].Uri = "/gvm"
		me.File[1].SkipLogin = skipLogin
		me.File[1].LoginPage = loginPage
	}
	me.File[2].FilePath = publicDir
	me.File[2].Type = "file"
	me.File[2].NeedLogin = false
	me.File[2].Uri = publicUrl
	me.File[2].SkipLogin = skipLogin
}

type ECHttpBinds struct {
	Name          string
	Bind          string
	MaxUpload     int64
	HttpCert      string
	HttpKey       string
	HttpCA        string
	AuthClient    bool
	Timeout       int
	Debug         int
	EnableWhiteIP bool
	WhiteIPList   []string
	///////////////////
	WhiteIPListMap map[string]int
	ecHttp         *ECHttp             `Json:"-"`
	http           common.ECHttpServer `Json:"-"`
}

type ECHttp struct {
	UserData    sync.Map
	router      *ECRouterMgr
	binds       []*ECHttpBinds
	tlsBinds    []*ECHttpBinds
	session     *ECSession
	compiler    *HtmlCompiler
	cfgName     string
	config      *ECHttpConfig
	httpFile    interface{} //处理静态文件
	globalRoute *ECRouter   //处理静态文件的路由
	env         map[string]uintptr
}

// govm 参考
func GetECHttp() *ECHttp {
	return nil
}

// govm 参考
func GetECHttpConfig() *ECHttpConfig {
	return nil
}

/*
cfg:  len=0, default config
*/
func NewECHttp(cfg ...string) *ECHttp {
	h := new(ECHttp)
	if len(cfg) > 0 {
		h.cfgName = cfg[0]
	} else {
		h.cfgName = "`"
	}
	h.init()
	return h
}
func (me *ECHttp) GetConfigFile() string {
	return me.cfgName
}
func (me *ECHttp) GetHttpConfig() *ECHttpConfig {
	return me.config
}
func (me *ECHttp) loadConfig() {
	if me.cfgName == "" {
		return
	}
	cfgName := me.cfgName
	if cfgName == "`" {
		cfgName = "./echttp.json"
	}
	cfgName = sys.FormatDir(cfgName)
	data, err := xfile.ReadFile(cfgName)
	if err != nil {
		return
	}
	me.config.filename = cfgName
	err = json.Unmarshal(data, &me.config)
	if err != nil {
		dbg.Dbg("%s\n", err.Error())
	} else {
	}
}

func (me *ECHttp) SetEnableMemSessionDB(v bool) {
	if me.session != nil {
		me.session.SetEnableMemDB(v)
	}
}

func (me *ECHttp) ResetSession() {
	if me.session != nil {
		me.session.Reset()
	}
}

func (me *ECHttp) SetConfig(cfg *ECHttpConfig) {
	if cfg != nil {
		me.config = cfg
	}
}

func (me *ECHttp) GetBind(index int) ECHttpBinds {
	if index >= 0 && index < len(me.binds) {
		return *me.binds[index]
	} else {
		return ECHttpBinds{}
	}
}

func (me *ECHttp) GetTLSBind(index int) ECHttpBinds {
	if index >= 0 && index < len(me.tlsBinds) {
		return *me.tlsBinds[index]
	} else {
		return ECHttpBinds{}
	}
}

func (me *ECHttp) GetBindCount() int {
	return len(me.binds)
}

func (me *ECHttp) GetTLSBindCount() int {
	return len(me.tlsBinds)
}

func (me *ECHttp) AddBind(name, host string) *ECHttpBinds {
	b := new(ECHttpBinds)
	b.Name = name
	b.Bind = host
	me.binds = append(me.binds, b)
	b.ecHttp = me
	return b
}

func (me *ECHttp) AddFileHandler(name, uri, filePath, file404 string) *FileHandler {
	if me.config == nil {
		me.config = new(ECHttpConfig)
	}
	httpFile := new(FileHandler)
	httpFile.Name = name
	httpFile.Uri = uri
	httpFile.FilePath = filePath
	httpFile.File404 = file404
	me.config.File = append(me.config.File, httpFile)
	return httpFile
}

func (me *ECHttp) GetRouter() *ECRouterMgr {
	return me.router
}

func (me *ECHttp) SetRouter(r *ECRouterMgr) {
	if r != nil {
		me.router = r
	}
}

/**
AddRouter
handler: func(ECInstance), ECRPCServer, FileHandler *struct
*struct: args： 1， true 设置路由路径首字母小写
*/

func (me *ECHttp) AddRouter(path string, handler interface{}, absPath bool, args ...string) *ECRouter {
	if me.router == nil {
		return nil
	}
	if handler == nil {
		return nil
	}
	handlerType := reflect.TypeOf(handler)
	switch handlerType.Kind() {
	case reflect.Func:
		if handlerType.NumIn() == 0 {
			dbg.DbgMode(dbg.CSWarning, "AddRouter: %s argument 0 must *ECInstance or interface\n", path)
			return nil
		}
		if handlerType.In(0).AssignableTo(reflect.TypeOf((*ECInstance)(nil))) == false && handlerType.In(0).Kind() != reflect.Interface {
			dbg.DbgMode(dbg.CSWarning, "AddRouter: %s argument 0 must *ECInstance or interface\n", path)
			return nil
		}
	case reflect.Struct:
		dbg.DbgMode(dbg.CSWarning, "AddRouter: path=%s, must pointer struct &%s\n", path, handlerType.Name())
		return nil
	case reflect.Ptr:
		_, ok := handler.(*ECRPCServer)
		if ok {
			break
		}
		value := reflect.ValueOf(handler)
		if value.Kind() == reflect.Ptr {
			if value.Elem().Kind() == reflect.Struct {
				toLower := false
				if len(args) > 0 && (args[0] == "1" || args[0] == "true") {
					toLower = true
				}
				me.addRouterStruct(path, handler, toLower, sys.GetCallInfo(2))
				return nil
			}
		}
		dbg.DbgMode(dbg.CSWarning, "AddRouter: %s pointer must be *ECRPCServer\n", path)
		return nil
	default:
		dbg.DbgMode(dbg.CSWarning, "AddRouter: %s invalid handler type: %+v\n", path, handlerType.Kind())
		return nil
	}
	return me.router.AddRouter(path, handler, absPath, args...)
}

func (me *ECHttp) stringFirstLowercase(str string) string {
	if str == "" {
		return str
	}
	if str[0] >= 'A' && str[0] <= 'Z' {
		return string('a'+str[0]-'A') + str[1:]
	}
	return str
}

func (me *ECHttp) AddRouterStruct(path string, inf interface{}, firstLocalCase bool) {
	me.addRouterStruct(path, inf, firstLocalCase, sys.GetCallInfo(2))
}

func (me *ECHttp) addRouterStruct(path string, inf interface{}, firstLocalCase bool, callInfo string) {
	if me.router == nil {
		return
	}
	value := reflect.ValueOf(inf)
	if value.Kind() == reflect.Struct {
		return
	}
	if value.Kind() != reflect.Ptr {
		return
	}
	ty := reflect.TypeOf(inf)
	structName := ty.Elem().Name()
	for i := 0; i < value.NumMethod(); i++ {
		fn := value.Method(i)

		if fn.Type().NumIn() == 0 {
			dbg.DbgMode(dbg.CSWarning, "AddRouterStruct: path=%s,%s.%s argument 0 must *ECInstance\n", path, structName, ty.Method(i).Name)
			continue
		}
		if fn.Type().In(0).AssignableTo(reflect.TypeOf((*ECInstance)(nil))) == false {
			dbg.DbgMode(dbg.CSWarning, "AddRouter: path=%s,%s.%s argument 0 must *ECInstance\n", path, structName, ty.Method(i).Name)
			continue
		}
		if firstLocalCase {
			path = xstring.ConnPath(path, me.stringFirstLowercase(structName), me.stringFirstLowercase(ty.Method(i).Name))
		} else {
			path = xstring.ConnPath(path, structName, ty.Method(i).Name)
		}
		me.router.AddRouter(path, fn.Interface(), true)
		dbg.DbgModeRaw(dbg.CSInfo, "%s add route path: %s\n", callInfo, path)
	}
}

func (me *ECHttp) GetConfig() *ECHttpConfig {
	return me.config
}

func GetListenFd(l net.Listener) (uintptr, error) {
	tcpListen, ok := l.(*net.TCPListener)
	if ok == false {
		return 0, errors.New("GetListenFd Error")
	}
	file, err := tcpListen.File()
	if err != nil {
		return 0, err
	}
	return file.Fd(), nil
}

func (me *ECHttp) StopAll() error {
	for _, v := range me.binds {
		if v.http != nil {
			if v.http.GetListener() != nil {
				v.http.GetListener().Close()
			}
		}
	}
	for _, v := range me.tlsBinds {
		if v.http != nil {
			v.http.CloseListen()
		}
	}
	return nil
}

func (me *ECHttp) GraceRestart(wait bool) {
	go me.GraceRestart2(wait)
}

func (me *ECHttp) AddTLSBind(name, host string) *ECHttpBinds {
	b := new(ECHttpBinds)
	b.Name = name
	b.Bind = host
	me.tlsBinds = append(me.tlsBinds, b)
	b.ecHttp = me
	return b
}

func (me *ECHttp) init() {
	me.env = make(map[string]uintptr)
	me.config = new(ECHttpConfig)
	me.loadConfig()
	me.router = NewECRouterMgr()
	me.session = NewECSession(me.config.MemDB.Url)
	if me.config.FastHttp {
		//me.httpFile = fasthttpserver.NewFastHttpFileHandler(constvar.ApplicationPath)
	} else {
		me.httpFile = gohttpserver.NewGoHttpFileHandler(constvar.ApplicationPath)
	}
	me.globalRoute = me.router.CreateRouter("/", me.httpFile, false, "fs", "./", "")
}

func (me *ECHttpBinds) GetHttpEx() common.ECHttpServer {
	return me.http
}

func (me *ECHttpBinds) GetECHttp() *ECHttp {
	return me.ecHttp
}

func (me *ECHttpBinds) GetBindPort() int {
	port := int64(80)
	index := strings.IndexByte(me.Bind, ':')
	if index >= 0 {
		port, _ = cast.ToInt64(me.Bind[index+1:])
	}
	return int(port)
}

func (me *ECHttpBinds) ServeHTTP(ctx *common.ECHttpContext) {
	if me.ecHttp == nil {
		panic("ECHttp is null")
	}
	if ctx.HttpType == common.GOHTTP {
		defer ctx.Request.CloseBody()
	}
	compiler := me.ecHttp.compiler
	if compiler != nil && compiler.GetLastError() != "" {
		tmpStr := "+++++++++++++++++++++++++++++++++++++++++++++++++++</br>"
		tmpStr += strings.Replace(compiler.GetLastError(), "\n", "</br>", -1)
		tmpStr += "----------------------------------------------------</br>"
		tmpStr += "Compile Failed at " + compiler.GetLastTime()
		_ = ctx.Response.AddHttpHeader("Content-Type", "text/html")
		_ = ctx.Response.SetHttpCode(500)
		_ = ctx.Response.WriteString(tmpStr)
		return
	}
	if me.http != nil {
		me.http.UpdateLastUrl(ctx.Request.RemoteAddress(), ctx.Request.RequestURI())
	}
	if me.ecHttp.router != nil {
		me.ecHttp.router.HttpRouter(me, ctx)
	}
}

func (me *ECHttp) GetEnv() map[string]uintptr {
	envs := os.Environ()
	ret := make(map[string]uintptr)
	fdptr := 3
	for n := range envs {
		str := envs[n]
		pref := str[:3]
		if pref == "_fd" {
			ret[str[3:]] = uintptr(fdptr)
			fdptr++
		}
	}
	return ret
}

func (me *ECHttp) SortRouter() {
	me.router.SortRouter()
}

func (me *ECHttp) Start() error {
	me.compiler = HtmlCompile
	me.env = me.GetEnv()
	if me.router != nil {
		for _, v := range me.config.File {
			v.FilePath = sys.FormatDir(v.FilePath)
			if v.File404 != "" {
				v.File404 = sys.FormatDir(v.File404)
			}
			var fs interface{}
			if me.config.FastHttp {
				//fs = fasthttpserver.NewFastHttpFileHandler(xstring.ConnPath(v.FilePath))
			} else {
				fs = gohttpserver.NewGoHttpFileHandler(xstring.ConnPath(v.FilePath))
			}
			if fs == nil {
				dbg.Dbg("May be disabled handle: %s\n", v.FilePath)
				continue
			}
			index := strings.IndexByte(v.Uri, '@')
			if index <= 0 {
				v.Uri = v.Uri + "@" + v.Name
			}
			r := me.router.AddRouter(v.Uri, fs, false, "fs", v.FilePath, v.File404)
			if r != nil {
				r.rootDir = v.FilePath
				r.callback = v.Callback
				r.FileConfig = v
			}
			dbg.Dbg("Add File Handler: [Uri=%s] [Root=%s] [Type=%s]\n", v.Uri, v.FilePath, v.Type)
		}
		me.router.SortRouter()
	}
	for _, v := range me.config.Http {
		me.binds = append(me.binds, v)
	}
	for _, v := range me.config.Https {
		me.tlsBinds = append(me.tlsBinds, v)
	}
	for _, v := range me.binds {
		var httpEx common.ECHttpServer
		if me.config.FastHttp {
			//httpEx = fasthttpserver.NewFastHttpServer()
		} else {
			httpEx = gohttpserver.NewGoHttpServer()
		}
		httpEx.SetBindHost(v.Bind)
		if v.Timeout > 0 {
			httpEx.SetTcpTimeout(v.Timeout)
		}
		v.ecHttp = me
		v.http = httpEx
		fd, ok := me.env[v.Bind]
		if ok {
			httpEx.SetFd(fd)
		}
		_, err := httpEx.Listen()
		if err != nil {
			return err
		}
		httpEx.SetHandler(v.ServeHTTP)
		v.WhiteIPListMap = make(map[string]int)
		for _, ip := range v.WhiteIPList {
			v.WhiteIPListMap[ip] = 1
		}
		dbg.Dbg("http bind: %s, name=%s\n", v.Bind, v.Name)
		go func() {
			_, err := httpEx.Start()
			if err != nil {
				dbg.Dbg("Error: %s\n", err.Error())
			}
		}()
	}
	for _, v := range me.tlsBinds {
		var httpEx common.ECHttpServer
		if me.config.FastHttp {
			//httpEx = fasthttpserver.NewFastHttpServer()
		} else {
			httpEx = gohttpserver.NewGoHttpServer()
		}
		httpEx.SetBindHost(v.Bind)
		if v.Timeout > 0 {
			httpEx.SetTcpTimeout(v.Timeout)
		}
		v.ecHttp = me
		v.http = httpEx
		fd, ok := me.env[v.Bind]
		if ok {
			httpEx.SetFd(fd)
		}
		var tlsConfig *tls.Config
		if v.HttpCA != "" {
			pool := x509.NewCertPool()
			caCertPath := sys.FormatDir(v.HttpCA)
			caCrt, err := xfile.ReadFile(caCertPath)
			if err != nil {
				dbg.Dbg("ReadFile err: %s\n", err.Error())
				//panic(err)
			} else {
				pool.AppendCertsFromPEM(caCrt)
			}
			tlsConfig = &tls.Config{
				RootCAs: pool,
			}

			if v.AuthClient {
				tlsConfig.ClientAuth = tls.RequireAndVerifyClientCert
			} else {
				tlsConfig.ClientAuth = tls.NoClientCert
			}
		}
		_, err := httpEx.ListenTLS(v.HttpCert, v.HttpKey, tlsConfig)
		if err != nil {
			dbg.Dbg("Error: %s\n", err.Error())
			return nil
		}
		v.WhiteIPListMap = make(map[string]int)
		for _, ip := range v.WhiteIPList {
			v.WhiteIPListMap[ip] = 1
		}
		httpEx.SetHandler(v.ServeHTTP)
		dbg.Dbg("http bindTLS: %s, name=%s\n", v.Bind, v.Name)
		go func() {
			_, err := httpEx.Start()
			if err != nil {
				dbg.Dbg("Error: %s\n", err.Error())
			}
		}()
	}
	return nil
}

func DefaultECConfig(filename string) {
	if sys.FileExist(filename) {
		return
	}
	config := new(ECHttpConfig)
	config.File = make([]*FileHandler, 0)

	f := new(FileHandler)
	f.Type = "js"
	f.Uri = "/jc"
	f.FilePath = constvar.ApplicationPath + "/www"
	config.File = append(config.File, f)

	f = new(FileHandler)
	f.Name = ""
	f.Uri = "/"
	f.FilePath = constvar.ApplicationPath + "/www"
	config.File = append(config.File, f)

	config.Http = make([]*ECHttpBinds, 0)
	config.Https = make([]*ECHttpBinds, 0)
	bind := new(ECHttpBinds)
	bind.Name = ""
	bind.Bind = ":8080"
	config.Http = append(config.Http, bind)
	b, err := json.MarshalIndent(config, "", "  ")
	if err != nil {
		dbg.Dbg("%s\n", err.Error())
	}
	ioutil.WriteFile(filename, b, 0666)
}

func (me *ECHttp) DefaultHandlerMemFile(ins *ECInstance, router *ECRouter, url string, rootDir string, memSys *MemFileSys) (bool, string) {
	if url == "/" {
		url = "/index"
	}
	if memSys == nil {
		return false, "memory file system is nil"
	}
	filename := xstring.ConnPath(rootDir, url)
	exist, isDir := xfile.FileExist2(filename)
	if !exist {
		memFile := memSys.Get(url)
		if memFile != nil {
			me.processMemFileMine(ins, url)
			return false, string(memFile.Data)
		}
		exist, isDir = xfile.FileExist2(filename + ".html")
		if !exist {
			memFile = memSys.Get(url + ".html")
			if memFile != nil {
				me.processMemFileMine(ins, url+".html")
				return false, string(memFile.Data)
			}
		} else {
			url += ".html"
		}
	} else if exist && isDir == false {
	}
	if !exist {
		url = "/"
	}

	if exist && !isDir {
		return true, ""
	}
	if exist == false {
		return true, "not found"
	}
	b, err := me.getFileContent("/index.html", rootDir, memSys)
	if err != nil {
		return false, err.Error()
	}
	return me.renderFolder(ins, url, b, rootDir)
}
func (me *ECHttp) processMemFileMine(ins *ECInstance, url string) {
	ext := strings.ToLower(xstring.GetFileExt(url))
	ext = ext[1:]
	switch ext {
	case "html", "htm":
		ins.AddHttpHeader("Content-Type", "text/html")
	case "js":
		ins.AddHttpHeader("Content-Type", "application/javascript")
	case "css":
		ins.AddHttpHeader("Content-Type", "text/css")
	case "jpg", "jpeg", "png", "gif":
		ins.AddHttpHeader("Content-Type", "image/"+ext)
	default:
		ins.AddHttpHeader("Content-Type", "text/html")
	}
}
func (me *ECHttp) getFileContent(url string, rootDir string, memSys *MemFileSys) ([]byte, error) {
	path := xstring.ConnPath(rootDir, url)
	if xfile.FileExist(path) {
		return ioutil.ReadFile(path)
	} else {
		memFile := memSys.Get(url)
		if memFile != nil {
			return memFile.Data, nil
		}
	}
	return nil, errors.New("not found")
}
func (me *ECHttp) renderFolder(ins *ECInstance, url string, data []byte, rootDir string) (bool, string) {
	ins.AddHttpHeader("Content-type", "text/html; charset=utf-8")
	path := xstring.ConnPath(rootDir + url)
	dirInfo, fileInfo, err := xfile.ShowDir(path, url, "")
	if err != nil {
		return false, err.Error()
	}
	t := template.New("indexHtml")
	t, err = t.Parse(string(data))
	if err != nil {
		return false, err.Error()
	}
	type IndexHtmlArg struct {
		DirInfo   []*xfile.FileInfo
		FileInfo  []*xfile.FileInfo
		RootDir   string
		ParentDir string
		Mode      string
	}
	args := new(IndexHtmlArg)
	args.RootDir = url
	tmpStr, _ := ins.Form("m")
	args.Mode = tmpStr

	parent := url
	parent = strings.TrimRight(parent, " /")
	index := strings.LastIndex(parent, "/")
	if index >= 0 {
		parent = parent[:index]
	}
	if parent == "" {
		parent = "/"
	}
	//Dbg.Dbg("rootDir=%s, parent=%s\n",  url, parent)
	args.FileInfo = fileInfo
	args.DirInfo = dirInfo
	args.ParentDir = parent
	var outBuf bytes.Buffer
	err = t.Execute(&outBuf, args)
	if err != nil {
		return false, err.Error()
	}
	return false, outBuf.String()
}

type HttpClient struct {
	BindName string
	Bind     string
	IP       string
	LastUrl  string
	Time     string
}

func (me *ECHttp) GetHttpClient() []*HttpClient {
	n := 0
	sortList := xlist.NewSortList[*HttpClient](func(new, old interface{}) bool {
		v1, _ := new.(*HttpClient)
		v2, _ := new.(*HttpClient)
		if v1 != nil && v2 != nil {
			if v1.Bind == v2.Bind {
				if v1.IP < v2.IP {
					return true
				}
			} else if v1.Bind > v2.Bind {
				return true
			}
		}
		return false
	})
	for {
		bind := me.GetBind(n)
		if bind.Bind == "" {
			break
		}
		httpEx := bind.GetHttpEx()
		httpEx.Lock()
		clientList := httpEx.GetConnectList()
		for k, v := range clientList {
			client := new(HttpClient)
			client.IP = k
			client.Bind = bind.Bind
			client.BindName = bind.Name
			client.LastUrl = v.LastUrl
			client.Time = xtime.Time2Str(v.UpdateTime)
			sortList.Sort(client)
		}
		httpEx.Unlock()
		n++
	}
	resp := make([]*HttpClient, 0, 10)
	for e := sortList.Front(); e != nil; e = e.Next() {
		client, _ := e.Value.(*HttpClient)
		if client != nil {
			resp = append(resp, client)
		}
	}
	return resp
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

type MultiPartData struct {
	buf *bytes.Buffer
	wr  *multipart.Writer
}

func (m *MultiPartData) init() {
	m.buf = new(bytes.Buffer)
	m.wr = multipart.NewWriter(m.buf)
}

func (m *MultiPartData) AddImageFile(name string, filename string) error {
	if name == "" {
		name = "file"
	}
	tmpFilename := xstring.GetFileName(filename)
	fileContent, err := xfile.ReadFile(filename)
	if err != nil {
		return err
	}

	v := make([]string, 1)
	tmpM := make(map[string][]string)
	v[0] = fmt.Sprintf("form-data; name=\"%s\"; filename=\"%s\"", name, tmpFilename)
	tmpM["Content-Disposition"] = v
	v = make([]string, 1)
	ext := strings.ToLower(xstring.GetFileExt(filename))
	if len(ext) <= 1 {
		ext = ".jpg"
	}
	ext = ext[1:]
	v[0] = "image/" + ext
	tmpM["Content-Type"] = v
	wFile, _ := m.wr.CreatePart(tmpM)
	_, _ = wFile.Write(fileContent)
	return nil
}

func (m *MultiPartData) AddJson(name string, b []byte) {
	v := make([]string, 1)
	tmpM := make(map[string][]string)

	v[0] = fmt.Sprintf("form-data; name=\"%s\"", name)
	tmpM["Content-Disposition"] = v

	v = make([]string, 1)
	v[0] = "application/json"
	tmpM["Content-Type"] = v
	wMessage, _ := m.wr.CreatePart(tmpM)
	_, _ = wMessage.Write(b)
}

func (m *MultiPartData) AddVar(name string, value string) {
	v := make([]string, 1)
	tmpM := make(map[string][]string)

	v[0] = fmt.Sprintf("form-data; name=\"%s\"", name)
	tmpM["Content-Disposition"] = v

	wMessage, _ := m.wr.CreatePart(tmpM)
	_, _ = wMessage.Write([]byte(value))
}

func (m *MultiPartData) GetContentType() string {
	return m.wr.FormDataContentType()
}

func (m *MultiPartData) Dump() []byte {
	_ = m.wr.Close()
	return m.buf.Bytes()
}

func NewMultiPartData() *MultiPartData {
	resp := new(MultiPartData)
	resp.init()
	return resp
}
