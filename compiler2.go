//go:build !EMBED
// +build !EMBED

package xhttp

import (
	"gitee.com/xlm516/xtool/dbg"
	xfile "gitee.com/xlm516/xtool/file"
)

func (me *HtmlCompiler) StartMonitor() error {
	fileMonitor, err := xfile.NewMonitor("")
	if err != nil {
		dbg.Dbg("File Monitor Failed: %s\n", err.Error())
		return err
	}
	fileMonitor.If.IfCreate = me.IfCreate
	fileMonitor.If.IfDelete = me.IfDelete
	fileMonitor.If.IfModify = me.IfModify
	fileMonitor.If.IfRename = me.IfRename
	for _, v := range me.source {
		dbg.Dbg("Add Compiler Path: %s\n", v)
		fileMonitor.AddWatch(v)
	}
	err = fileMonitor.Start()
	me.fileMonitor = fileMonitor
	return err
}
