package xhttp

import (
	"encoding/json"
	"errors"
	"gitee.com/xlm516/xtool/constvar"
	"gitee.com/xlm516/xtool/dbg"
	xfile "gitee.com/xlm516/xtool/file"
	xtime "gitee.com/xlm516/xtool/time"
	"strings"
)

type ECMemDBConfig struct {
	Bind string
	User struct {
		User string
		Pwd  string
	}
}

type ECMemDB struct {
	ecHttp    *ECHttp
	ecSock    *ECSocketServer
	ecRPC     *ECRPCServer
	session   *ECSession
	config    ECMemDBConfig
	lastCheck int64
	cfgName   string
}

func NewECMemDB(cfg string) *ECMemDB {
	db := new(ECMemDB)
	db.session = new(ECSession)
	db.session.init()
	db.cfgName = cfg
	return db
}

func (me *ECMemDB) loadConfig() {
	if me.cfgName == "" {
		me.cfgName = constvar.ApplicationPath + "/memdb.json"
	}
	data, err := xfile.ReadFile(me.cfgName)
	if err != nil {
		return
	}
	json.Unmarshal(data, &me.config)
}

func (me *ECMemDB) GetSessionPtr() map[string]*ECSessionHost {
	return me.session.session
}

func (me *ECMemDB) Lock() {
	me.session.mutex.Lock()
}

func (me *ECMemDB) Unlock() {
	me.session.mutex.Unlock()
}

func (me *ECMemDB) SetCookie(ins *ECInstance, c *ECSessionItem) bool {
	me.deleteTimeoutSession()
	me.session.mutex.Lock()
	defer me.session.mutex.Unlock()
	//Dbg.Dbg("Set Memdb, ip=%s, session=%s\n", c.Ip, c.Session, c.Values)
	if c.Ip == "" || c.Host == "" || c.Session == "" || c.Values == nil {
		dbg.Dbg("invalid session\n")
		return false
	}
	sessHost, ok := me.session.session[c.Ip]
	if ok == false {
		sessHost = new(ECSessionHost)
		sessHost.host = c.Host
		sessHost.lastUpdate = xtime.UnixTime()
		sessHost.sessionHost = make(map[string]*ECSessionItem)
		me.session.session[c.Ip] = sessHost
	}
	sessHost.sessionHost[c.Session] = c
	return true
}

func (me *ECMemDB) GetCookie(ins *ECInstance, ip, session string) *ECSessionItem {
	me.deleteTimeoutSession()
	me.session.mutex.Lock()
	defer me.session.mutex.Unlock()
	//Dbg.Dbg("Get Memdb, ip=%s, session=%s\n", ip, session)
	sessHost, ok := me.session.session[ip]
	if ok == false {
		return nil
	}
	sessItem, ok := sessHost.sessionHost[session]
	if ok {
		nowTime := xtime.UnixTime()
		sessItem.UpdateTime = nowTime
		sessItem.lastNotify = nowTime
		return sessItem
	} else {
		return nil
	}
}

func (me *ECMemDB) deleteTimeoutSession() {
	nowTime := xtime.UnixTime()
	if xtime.CompTime(nowTime, me.lastCheck) < 120 {
		return
	}
	me.lastCheck = nowTime
	me.session.mutex.Lock()
	defer me.session.mutex.Unlock()
	for host, session := range me.session.session {
		if xtime.CompTime(session.lastUpdate, nowTime) > 3600*2 {
			delete(me.session.session, host)
			continue
		}
		for k, v := range session.sessionHost {
			if xtime.CompTime(nowTime, v.UpdateTime) > int64(v.Timeout) {
				delete(session.sessionHost, k)
			}
		}
	}
}

func (me *ECMemDB) Start() error {
	me.ecHttp = NewECHttp("")
	bindHost := me.config.Bind
	if bindHost == "" {
		bindHost = ":7788"
	}
	me.ecHttp.AddBind("", bindHost)
	me.ecSock = NewECSocketServer(":7789")
	me.ecRPC = NewECRPCServer()
	me.ecHttp.GetRouter().AddRouter("/rpc", me.ecRPC, true)
	me.ecSock.GetRouter().AddRouter("/rpc", me.ecRPC, true)
	me.ecRPC.AddFunc("GetCookie", me.GetCookie, 0)
	me.ecRPC.AddFunc("SetCookie", me.SetCookie, 0)
	err := me.ecHttp.Start()

	return err
}

// ///////////////////////////////////////////////////////////////////////////////
type ECMemDBClient struct {
	ecRPCClient *ECRPCClient
	host        string
}

func NewECMemDBClient(host string) *ECMemDBClient {
	c := new(ECMemDBClient)
	c.host = host
	c.init()
	return c
}

func (me *ECMemDBClient) init() {
	me.ecRPCClient = NewECRPCClient(me.host)
}

func (me *ECMemDBClient) SetCookie(c *ECSessionItem) (bool, error) {
	err := me.ecRPCClient.Call("SetCookie", c)
	if err != nil {
		//Dbg.Dbg("%s\n", err.Error())
		return false, err
	} else {
		return true, nil
	}
}

func (me *ECMemDBClient) GetCookie(ip, session string) (*ECSessionItem, error) {
	if session == "" {
		return nil, errors.New("session is empty")
	}
	index := strings.IndexByte(ip, ':')
	if index > 0 {
		ip = ip[:index]
	}
	err := me.ecRPCClient.Call("GetCookie", ip, session)
	if err != nil {
		//Dbg.Dbg("%s\n", err.Error())
		return nil, err
	}
	c := new(ECSessionItem)
	err = me.ecRPCClient.RetASStructIndex(c, 0)
	if err != nil {
		return nil, err
	} else {
		return c, nil
	}
}
