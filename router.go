package xhttp

import (
	"errors"
	"gitee.com/xlm516/xtool/dbg"
	xreflect "gitee.com/xlm516/xtool/reflect"
	xstring "gitee.com/xlm516/xtool/string"
	"gitee.com/xlm516/xtool/sys"
	"reflect"
	"strings"
	"sync"
)

/*
  return  true: 接续处理
  return  false
*/

type RouterCallback func(ins *ECInstance, router *ECRouter, url string) (bool, string)
type WsResponse struct {
	DataType int
	Data     []byte
}
type WsRequest struct {
	DataType      int
	Data          []byte
	handler       interface{}
	remoteAddress string
}

type WsEVent struct {
	Event         int
	EventStr      string
	RemoteAddress string
	Handler       interface{}
}

// return error, close ws connection
type WsHandlerCall func(client *WsClientInfo, request *WsRequest, resp *WsResponse) error
type WsEventCall func(event *WsEVent)

type ECRouter struct {
	bindName     string
	name         string
	path         string
	rootDir      string
	handler      interface{}
	args         []string
	htmlTpl      *HtmlTpl
	compileMutex sync.Mutex
	callback     RouterCallback
	wsCallback   WsHandlerCall
	wsEvent      WsEventCall
	wsRPCServer  *ECRPCServer
	FileConfig   *FileHandler
	fName        string
	fFile        string
	fLine        int
	wsMutex      sync.Mutex
	wsClientMap  map[string]*WsClientInfo
	userData     interface{}
}

/*
start /: 顶层目录
start .: 当前目录
其他：  当前目录
*/
func (me *ECRouter) FormatUrl(url string) string {
	if url == "url" {
		return ""
	}
	if url[0] == '/' {

	} else if url[0] == '.' {
		url = url[1:]
	} else {
		url = xstring.ConnPath(me.GetPath(), url)
	}
	return url
}
func (me *ECRouter) GetPath() string {
	return me.path
}
func (me *ECRouter) GetName() string {
	return me.name
}
func (me *ECRouter) GetRoot() string {
	return me.rootDir
}
func (me *ECRouter) SetRoot(v string) {
	me.rootDir = v
}
func (me *ECRouter) GetFuncInfo() (name string, file string, line int) {
	name = me.fName
	file = me.fFile
	line = me.fLine
	return
}
func (me *ECRouter) GetAbsPath(url string) string {
	tmpPath := me.path
	l := len(tmpPath)
	if l > 0 && tmpPath[l-1] != '/' {
		tmpPath += "/"
		l++
	}
	if len(url) >= l {
		url = url[l-1:]
	}
	return url
}

func (me *ECRouter) GetHandler() interface{} {
	return me.handler
}

func (me *ECRouter) GetFileRoot() string {
	if me.args == nil || len(me.args) <= 0 {
		return ""
	}
	if len(me.args) == 0 {
		return ""
	}
	return me.args[0]
}

type ECCookieRouter struct {
	uri        string
	cookiePath string
}

type ECRouterMgr struct {
	absRouters     map[string]*ECRouter
	routers        []*ECRouter
	routerError    *ECRouter
	cookieRouter   []*ECCookieRouter
	defaultBind    string
	enableTpl      bool
	enableCompiler bool
	filterAction   func(ins *ECInstance, router *ECRouter) bool
	preRouter      func(ins *ECInstance, router *ECRouter) bool
	NotFoundFunc   func(ins *ECInstance, router *ECRouter) bool
	routerDebug    bool
	crossOrigin    string //跨域
	crossMethod    string //跨域
	crossHeaders   string //跨域

	mutex sync.Mutex
}

func NewECRouterMgr() *ECRouterMgr {
	r := new(ECRouterMgr)
	r.init()
	return r
}

func (me *ECRouterMgr) init() {
	me.absRouters = make(map[string]*ECRouter)
	me.enableTpl = true
	me.enableCompiler = true
	me.routerDebug = false
}

func (me *ECRouterMgr) SetEnableDebug(v bool) {
	me.routerDebug = v
}

func (me *ECRouterMgr) SetDefaultBind(v string) {
	me.defaultBind = v
}

func (me *ECRouterMgr) SetEnableTPL(v bool) {
	me.enableTpl = v
}

func (me *ECRouterMgr) SetEnableCompiler(v bool) {
	me.enableCompiler = v
}

func (me *ECRouterMgr) SetActionFilter(filter func(ins *ECInstance, router *ECRouter) bool) {
	me.filterAction = filter
}

func (me *ECRouterMgr) SetPreRouter(filter func(ins *ECInstance, router *ECRouter) bool) {
	me.preRouter = filter
}

func (me *ECRouterMgr) CreateRouter(path string, handler interface{}, absPath bool, args ...string) *ECRouter {
	bindName := me.defaultBind
	nameIndex := strings.IndexByte(path, '@')
	if nameIndex > 0 {
		bindName = path[nameIndex+1:]
		path = path[:nameIndex]
	}
	if absPath {
		l := len(path)
		if path[l-1] == '/' {
			path = path[:l-1]
		}
	}
	router := new(ECRouter)
	router.handler = handler
	router.path = path
	router.bindName = bindName
	if len(args) > 0 {
		router.name = args[0]
		tmpLen := len(path)
		/* for http FileServer */
		if args[0] == "fs" && tmpLen > 0 {
			if path[tmpLen-1] != '/' {
				path += "/"
			}
			rootDir := ""
			if len(args) > 1 {
				for i := 1; i < len(args); i++ {
					router.args = append(router.args, args[i])
				}
				rootDir = sys.FormatDir(args[1])
			}
			router.htmlTpl = NewHtmlTpl(rootDir)
		}
	}

	return router
}

/*
path: uri@name
uri: http uri
name: server name, optional
*/
func (me *ECRouterMgr) AddRouter(path string, handler interface{}, absPath bool, args ...string) *ECRouter {
	if path == "" {
		return nil
	}
	me.mutex.Lock()
	defer me.mutex.Unlock()
	bindName := me.defaultBind
	nameIndex := strings.IndexByte(path, '@')
	if nameIndex > 0 {
		bindName = path[nameIndex+1:]
		path = path[:nameIndex]
	}
	if absPath {
		l := len(path)
		if path[l-1] == '/' {
			path = path[:l-1]
		}
	}
	router := new(ECRouter)
	router.handler = handler
	if absPath {
	}
	if len(args) > 0 {
		router.name = args[0]
		tmpLen := len(path)
		/* for http FileServer */
		if args[0] == "fs" && tmpLen > 0 {
			if path[tmpLen-1] != '/' {
				path += "/"
			}
			rootDir := ""
			if len(args) > 1 {
				for i := 1; i < len(args); i++ {
					router.args = append(router.args, args[i])
				}
				rootDir = sys.FormatDir(args[1])
			}
			router.htmlTpl = NewHtmlTpl(rootDir)
		}
	}
	router.path = path
	router.bindName = bindName
	router.fName, router.fFile, router.fLine = sys.GetFuncInfo(handler)
	if absPath {
		me.absRouters[path] = router
	} else {
		existIndex := -1
		for index, v := range me.routers {
			if v.path == router.path && v.bindName == router.bindName {
				existIndex = index
			}
		}
		if existIndex >= 0 {
			*me.routers[existIndex] = *router
		} else {
			me.routers = append(me.routers, router)
		}
	}
	return router
}

func (me *ECRouterMgr) DeleteRouter(path string) bool {
	if path == "" {
		return false
	}
	me.mutex.Lock()
	defer me.mutex.Unlock()
	//Dbg.Dbg("start delete route: %s\n", path)
	_, ok := me.absRouters[path]
	if ok {
		//Dbg.Dbg("delete route: %s\n", path)
		delete(me.absRouters, path)
		return true
	}
	index := -1
	for i, v := range me.routers {
		if v.path == path {
			index = i
			break
		}
	}
	if index >= 0 {
		//Dbg.Dbg("delete route: %s\n", path)
		me.routers = append(me.routers[:index], me.routers[index+1:]...)
		return true
	}
	return false
}

func (me *ECRouterMgr) ResetRouter() {
	me.mutex.Lock()
	defer me.mutex.Unlock()
	me.absRouters = make(map[string]*ECRouter, 0)
	me.routers = make([]*ECRouter, 0)
}

// AddRouterByData  带上用户自定义数据
func (me *ECRouterMgr) AddRouterByData(path string, handler interface{}, absPath bool, data interface{}, args ...string) *ECRouter {
	if path == "" {
		return nil
	}
	me.mutex.Lock()
	defer me.mutex.Unlock()
	bindName := me.defaultBind
	nameIndex := strings.IndexByte(path, '@')
	if nameIndex > 0 {
		bindName = path[nameIndex+1:]
		path = path[:nameIndex]
	}
	if absPath {
		l := len(path)
		if path[l-1] == '/' {
			path = path[:l-1]
		}
	}
	router := new(ECRouter)
	router.handler = handler
	router.userData = data
	if absPath {
	}
	if len(args) > 0 {
		router.name = args[0]
		tmpLen := len(path)
		/* for http FileServer */
		if args[0] == "fs" && tmpLen > 0 {
			if path[tmpLen-1] != '/' {
				path += "/"
			}
			rootDir := ""
			if len(args) > 1 {
				for i := 1; i < len(args); i++ {
					router.args = append(router.args, args[i])
				}
				rootDir = sys.FormatDir(args[1])
			}
			router.htmlTpl = NewHtmlTpl(rootDir)
		}
	}
	router.path = path
	router.bindName = bindName
	router.fName, router.fFile, router.fLine = sys.GetFuncInfo(handler)
	if absPath {
		me.absRouters[path] = router
	} else {
		existIndex := -1
		for index, v := range me.routers {
			if v.path == router.path && v.bindName == router.bindName {
				existIndex = index
			}
		}
		if existIndex > 0 {
			*me.routers[existIndex] = *router
		} else {
			me.routers = append(me.routers, router)
		}
	}
	return router
}

func (me *ECRouterMgr) AddCookieRouter(uri, path string) {
	cr := new(ECCookieRouter)
	cr.uri = uri
	cr.cookiePath = path
	me.cookieRouter = append(me.cookieRouter, cr)
}

func (me *ECRouterMgr) GetCookieRouter(uri string) string {
	for i := 0; i < len(me.cookieRouter); i++ {
		//Dbg.Dbg(tmpUrl, ":", me.routers[i].path)
		index := strings.Index(uri, me.cookieRouter[i].uri)
		if index == 0 {
			return me.cookieRouter[i].cookiePath
		}
	}
	return "/"
}

func (me *ECRouterMgr) SetRouterError(handler interface{}) {
	if handler == nil {
		return
	}
	me.routerError = new(ECRouter)
	me.routerError.path = ""
	me.routerError.handler = handler
}

func (me *ECRouterMgr) GetRouter(bindName, path string) (*ECRouter, string) {
	if path == "" {
		return me.routerError, ""
	}
	me.mutex.Lock()
	defer me.mutex.Unlock()
	secondUrl := ""
	tmpUrl := path
	router, ok := me.absRouters[tmpUrl]
	if ok && (router.bindName == "" || bindName == router.bindName) {
		return router, ""
	}
	l := len(path)
	if path != "/" && path[l-1] == '/' {
		//path = path[:l-1]
		//修改index为默认首页
		path = path + "index"
		router, ok := me.absRouters[path]
		if ok && (router.bindName == "" || bindName == router.bindName) {
			return router, ""
		}
	}
	tmpUrl = path
	for i := 0; i < len(me.routers); i++ {
		index := strings.Index(tmpUrl, me.routers[i].path)
		//Dbg.Dbg("Get Router %s:%s, index=%d\n", tmpUrl, me.routers[i].path, index)
		if index == 0 && (bindName == me.routers[i].bindName || me.routers[i].bindName == "") {
			secondUrl = tmpUrl[len(me.routers[i].path):]
			return me.routers[i], secondUrl
		}
	}
	for i := 0; i < len(me.routers); i++ {
		index := strings.Index(tmpUrl, me.routers[i].path)
		//Dbg.Dbg("Get Router %s:%s, index=%d\n", tmpUrl, me.routers[i].path, index)
		if index == 0 && (me.routers[i].bindName == "") {
			secondUrl = tmpUrl[len(me.routers[i].path):]
			return me.routers[i], secondUrl
		}
	}
	return me.routerError, ""
}

func (me *ECRouterMgr) SortRouter() {
	me.mutex.Lock()
	defer me.mutex.Unlock()
	for m := 0; m < len(me.routers); m++ {
		for n := m; n < len(me.routers); n++ {
			Uri1 := me.routers[m].path
			Uri2 := me.routers[n].path
			if len(Uri1) < len(Uri2) {
				tmpRouter := me.routers[m]
				me.routers[m] = me.routers[n]
				me.routers[n] = tmpRouter
			}
		}
	}

	for m := 0; m < len(me.cookieRouter); m++ {
		for n := m; n < len(me.cookieRouter); n++ {
			Uri1 := me.cookieRouter[m].uri
			Uri2 := me.cookieRouter[n].uri
			if len(Uri1) < len(Uri2) {
				tmpRouter := me.cookieRouter[m]
				me.cookieRouter[m] = me.cookieRouter[n]
				me.cookieRouter[n] = tmpRouter
			}
		}
	}
}

func (me *ECRouterMgr) doRPCServer(ins *ECInstance, router *ECRouter) error {
	handler, ok := router.handler.(ECRPCHandler)
	if ok {
		_ = ins.httpInData.httpCtx.Response.SetHttpHeader("Content-Type", "application/json")
		_, err := handler.Call(ins, nil, ins.jsonObj)
		if err != nil {
			dbg.Dbg("%s,Data: %+v\n", err.Error(), ins.jsonObj)
			//b, _ := json.Marshal(ins.jsonObj)
			//fmt.Println("doRPCServer Error:", string(b))
			return err
		}
	} else {
		return errors.New("invalid ECRPCHandler")
	}
	return nil
}

func (me *ECRouterMgr) doCallFunc(ins *ECInstance, router *ECRouter) error {
	if me.filterAction != nil {
		if me.filterAction(ins, router) {
			dbg.Dbg("Filter action %s\n", router.path)
			return nil
		}
	}
	err := me.reflectCall(router.handler, ins)
	if err != nil {
		dbg.Dbg("path: %s, error: %s\n", router.path, err.Error())
		return err
	}
	return nil
}

func (me *ECRouterMgr) doRouter(ins *ECInstance, router *ECRouter) error {
	if me.preRouter != nil {
		if me.preRouter(ins, router) {
			dbg.Dbg("PreRouter Filter action %s\n", router.path)
			return nil
		}
	}
	tmpHandlerValue := reflect.ValueOf(router.handler)
	tmpType := tmpHandlerValue.Type()
	if router.name == "fs" {
		return me.doFileServer(ins, router)
	} else if tmpType.Kind() == reflect.Func {
		return me.doCallFunc(ins, router)
	} else if router.name == "fs" {
		return me.doFileServer(ins, router)
	} else {
		return me.doRPCServer(ins, router)
	}
	return nil
}

func (me *ECRouterMgr) reflectCall(fun interface{}, ins *ECInstance) error {
	if fun == nil || ins == nil {
		return errors.New("null")
	}
	tmpFun := reflect.ValueOf(fun)
	if tmpFun.Type().Kind() != reflect.Func {
		dbg.Dbg("is not function\n")
		return errors.New("is not function")
	}
	tmpType := tmpFun.Type()
	if tmpType.NumIn() == 0 {
		dbg.Dbg("too few argument\n")
		return errors.New("too few argument")
	}
	arg1Type := tmpType.In(0)
	if arg1Type != reflect.ValueOf(ins).Type() && tmpType.In(0).Kind() != reflect.Interface {
		dbg.Dbg("first argument invalid, %+v <> %+v\n", arg1Type, reflect.ValueOf(ins).Type())
		return errors.New("first argument invalid")
	}
	inArg := make([]reflect.Value, 0, 3)
	inArg = append(inArg, reflect.ValueOf(ins))
	var newValue reflect.Value
	if tmpType.NumIn() > 1 {
		arg2Type := tmpType.In(1)
		isStruct := true
		if arg2Type.Kind() == reflect.Ptr {
			if arg2Type.Elem().Kind() != reflect.Struct {
				isStruct = false
			}
			newValue = reflect.New(arg2Type.Elem())
		} else {
			if arg2Type.Kind() != reflect.Struct {
				isStruct = false
			}
			newValue = reflect.New(arg2Type)
		}
		if newValue.CanInterface() && isStruct {
			mapValue := make(map[string]interface{})
			v, ok := ins.httpInData.httpEnv["_request"]
			if ok {
				mapValue, ok = v.(map[string]interface{})
				if ok {
					_ = xreflect.ReflectFillStruct(newValue.Interface(), mapValue)
				}
			}
		}
		if arg2Type.Kind() == reflect.Ptr {
			inArg = append(inArg, newValue)
		} else {
			inArg = append(inArg, newValue.Elem())
		}
	}

	if tmpType.NumIn() > 2 {
		arg3Type := tmpType.In(2)
		if arg3Type.Kind() == reflect.Ptr {
			newValue = reflect.New(arg3Type.Elem())
		} else {
			newValue = reflect.New(arg3Type)
		}
		if newValue.CanInterface() {
			if ins.jsonObj != nil {
				var mapValue map[string]interface{}
				v, ok := ins.jsonObj["params"]
				if ok {
					mapValue, ok = v.(map[string]interface{})
				} else {
					ok = true
					mapValue = ins.jsonObj
				}
				if ok {
					xreflect.ReflectFillStruct(newValue.Interface(), mapValue)
				}
				sliceValue, ok := v.([]interface{})
				arg3Type2 := arg3Type
				if arg3Type.Kind() == reflect.Ptr {
					arg3Type2 = arg3Type.Elem()
				}
				if ok {
					newMapValue := make(map[string]interface{})
					for k, v := range sliceValue {
						if k < arg3Type2.NumField() {
							fieldName := arg3Type2.Field(k).Name
							newMapValue[fieldName] = v
						}
					}
					xreflect.ReflectFillStruct(newValue.Interface(), newMapValue)
				}
			}
		}
		if arg3Type.Kind() == reflect.Ptr {
			inArg = append(inArg, newValue)
		} else {
			inArg = append(inArg, newValue.Elem())
		}
	}

	defer sys.RecoverFunc("", 0)
	tmpFun.Call(inArg)
	return nil
}
