//go:build !GOHTTP_CLIENT
// +build !GOHTTP_CLIENT

package gohttpclient

import (
	"crypto/tls"
	"errors"
	"gitee.com/xlm516/http/common"
	"os"
)

type HttpClientEx struct {
	FOnSend func(httpClient *HttpClientEx, data []byte) ([]byte, error)
	FOnData func(httpClient *HttpClientEx, data []byte) ([]byte, error)
}

func NewHttpClientEx(url string, keepAlive bool, opt ...*common.HTTPClientOption) *HttpClientEx {
	return new(HttpClientEx)
}

func (me *HttpClientEx) SetClient(client interface{}) {

}
func (me *HttpClientEx) EnableProcess(v bool) {
}
func (me *HttpClientEx) SetMaxDownloadSize(size int) {
}

// second
func (me *HttpClientEx) SetTotalTimeout(v int) {
}

// second
func (me *HttpClientEx) SetTimeout(timeout int) {
}
func (me *HttpClientEx) SetBasicAuth(user, pwd string) {
}
func (me *HttpClientEx) initHttpClient() {
}

func (me *HttpClientEx) SetProxy(proxyUrl string) error {
	return errors.New("not support")
}
func (me *HttpClientEx) SetBindIP(ip string) error {
	return errors.New("not support")
}

func (me *HttpClientEx) GetUrl() string {
	return ""
}

func (me *HttpClientEx) SetKeepAlive(v bool) {
}

func (me *HttpClientEx) Close() {
}

func (me *HttpClientEx) InsecureSkipVerify() {
}

func (me *HttpClientEx) SetCert(ca, cert, key string, authServer bool) error {
	return nil
}

func (me *HttpClientEx) SetTlsConfig(tlsConfig *tls.Config) {
}

func (me *HttpClientEx) SetHttpHeader(name, value string) {
}

func (me *HttpClientEx) SetUrl(url string) {
}

func (me *HttpClientEx) SetUserAgent(agent string) {
}

func (me *HttpClientEx) SetConnTimeout(timeout int) {
}

func (me *HttpClientEx) Get(uri string) ([]byte, error) {
	return nil, errors.New("not support")
}

func (me *HttpClientEx) Post(uri string, data []byte) ([]byte, error) {
	return nil, errors.New("not support")
}

func (me *HttpClientEx) Put(uri string, data []byte) ([]byte, error) {
	return nil, errors.New("not support")
}

func (me *HttpClientEx) Delete(uri string, data []byte) ([]byte, error) {
	return nil, errors.New("not support")
}

func (me *HttpClientEx) Download(uri string, fh *os.File) (int64, error) {
	return 0, errors.New("not support")
}

func (me *HttpClientEx) GetContentType() string {
	return ""
}

func (me *HttpClientEx) GetHeader(name string) string {
	return ""
}

func (me *HttpClientEx) GetHeaders() common.Header {
	return nil
}

func (me *HttpClientEx) GetStatusCode() int {
	return 0
}

func (me *HttpClientEx) request(uri string, data []byte, fh *os.File) ([]byte, error) {
	return nil, errors.New("not support")
}

func (me *HttpClientEx) GetBody() []byte {
	return nil
}

func (me *HttpClientEx) SetContentJson() {

}

func (me *HttpClientEx) SetContentUrlEncode() {

}

type QueryCookie struct {
	Name  string
	Value string
	Path  string
}

func (me *HttpClientEx) GetCookie(name string) []*QueryCookie {
	return nil
}
