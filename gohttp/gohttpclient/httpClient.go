//go:build GOHTTP_CLIENT
// +build GOHTTP_CLIENT

package gohttpclient

import (
	"bufio"
	"bytes"
	"container/list"
	"context"
	"crypto/tls"
	"crypto/x509"
	"encoding/json"
	"errors"
	"fmt"
	"gitee.com/xlm516/http/common"
	"gitee.com/xlm516/xtool/cast"
	"gitee.com/xlm516/xtool/dbg"
	xlist "gitee.com/xlm516/xtool/list"
	xstring "gitee.com/xlm516/xtool/string"
	"gitee.com/xlm516/xtool/sys"
	xtime "gitee.com/xlm516/xtool/time"
	"golang.org/x/net/proxy"
	"io"
	"io/ioutil"
	"net"
	"net/http"
	"net/http/cookiejar"
	"net/url"
	"os"
	"strings"
	"sync"
	"time"
)

type HttpCookie struct {
	name    string
	path    string
	checked bool
	cookie  *http.Cookie
}
type HttpCookies struct {
	cookie     map[string]*HttpCookie
	updateTime int64
}

var httpClientMutex *sync.Mutex = new(sync.Mutex)
var httpCookies map[string]*HttpCookies = make(map[string]*HttpCookies)

func getCookie(host string, bCreate bool) *HttpCookies {
	httpClientMutex.Lock()
	c, _ := httpCookies[host]
	if c == nil && bCreate {
		c = &HttpCookies{}
		c.cookie = make(map[string]*HttpCookie)
		c.updateTime = xtime.UnixTime()
		httpCookies[host] = c
	}
	httpClientMutex.Unlock()
	return c
}

// timeout: second
func JRPCRequest(host string, reqData string, timeout int) (map[string]interface{}, error) {
	client := &http.Client{}
	if timeout > 0 {
		client.Timeout = time.Duration(timeout) * time.Second
	}
	req, err := http.NewRequest("POST", host, strings.NewReader(reqData))
	if err != nil {
		return nil, err
	}
	tmpHost := host
	index := strings.Index(host, "://")
	if index > 0 {
		tmpHost = host[index+3:]
	}
	index = strings.IndexByte(tmpHost, '/')
	if index > 0 {
		tmpHost = tmpHost[:index]
	}
	req.Header.Set("Content-Type", "application/json")
	cookie := getCookie(host, false)
	if cookie != nil {
		for _, v := range cookie.cookie {
			req.AddCookie(v.cookie)
		}
	}
	resp, err := client.Do(req)
	if err == nil {
		defer resp.Body.Close()
	} else {
		return nil, err
	}
	c := resp.Cookies()
	for _, cookie := range c {
		cookies := getCookie(host, true)
		if cookies == nil {
			fmt.Println("Create Http Cookie Failed")
			break
		}
		key := cookie.Name + cookie.Path
		existFlag := false
		for k, existCookie := range cookies.cookie {
			if k == key {
				existCookie.checked = true
				existCookie.cookie = cookie
				existFlag = true
				break
			}
		}
		if !existFlag {
			httpCookie := &HttpCookie{}
			httpCookie.cookie = cookie
			httpCookie.name = cookie.Name
			httpCookie.path = cookie.Path
			httpCookie.checked = true
			cookies.cookie[cookie.Name+cookie.Path] = httpCookie
			//fmt.Println("Add Cookie:", cookie.Name+cookie.Path, cookies.cookie)
		} else {
		}

		for k, existCookie := range cookies.cookie {
			if existCookie.checked == false {
				delete(cookies.cookie, k)
			}
		}
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	var ret map[string]interface{}
	err = json.Unmarshal(body, &ret)
	return ret, err
}

// timeout: second
func HttpRequest(host string, action, contextType, reqData string, timeout int) (string, error) {
	client := &http.Client{}
	if timeout > 0 {
		client.Timeout = time.Duration(timeout) * time.Second
	}
	req, err := http.NewRequest(action, host, strings.NewReader(reqData))
	if err != nil {
		return "", err
	}
	tmpHost := host
	index := strings.Index(host, "://")
	if index > 0 {
		tmpHost = host[index+3:]
	}
	index = strings.IndexByte(tmpHost, '/')
	if index > 0 {
		tmpHost = tmpHost[:index]
	}
	if contextType != "" {
		req.Header.Set("Content-Type", contextType)
	}
	cookie := getCookie(host, false)
	if cookie != nil {
		for _, v := range cookie.cookie {
			req.AddCookie(v.cookie)
		}
	}
	resp, err := client.Do(req)
	if err == nil {
		defer resp.Body.Close()
	} else {
		return "", err
	}
	c := resp.Cookies()
	for _, cookie := range c {
		cookies := getCookie(host, true)
		if cookies == nil {
			fmt.Println("Create Http Cookie Failed")
			break
		}
		key := cookie.Name + cookie.Path
		existFlag := false
		for k, existCookie := range cookies.cookie {
			if k == key {
				existCookie.checked = true
				existCookie.cookie = cookie
				existFlag = true
				break
			}
		}
		if !existFlag {
			httpCookie := &HttpCookie{}
			httpCookie.cookie = cookie
			httpCookie.name = cookie.Name
			httpCookie.path = cookie.Path
			httpCookie.checked = true
			cookies.cookie[cookie.Name+cookie.Path] = httpCookie
			//fmt.Println("Add Cookie:", cookie.Name+cookie.Path, cookies.cookie)
		} else {
		}

		for k, existCookie := range cookies.cookie {
			if existCookie.checked == false {
				delete(cookies.cookie, k)
			}
		}
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	return string(body), nil
}

// ////////////////////////////////////////////////////////////////////////////////////////
type HttpClientEx struct {
	conn       net.Conn
	httpClient *http.Client
	httpCookie HttpCookies
	headers    map[string]string
	statusCode int
	url        string
	uri        string
	action     string
	httpCA     string
	httpCert   string
	httpKey    string
	tlsMode    bool
	authServer bool
	respHeader common.Header
	bodyData   []byte

	maxDownloadSize int
	timeout         int // 多久没有接收到数据就超时，返回错误
	totalTimeout    int //second 总的接收超时

	enableBasicAuth bool
	authUser        string
	authPwd         string

	processInfo bool
	opt         common.HTTPClientOption

	totalBytes cast.Int64
	recvBytes  cast.Int64

	FOnSend func(httpClient *HttpClientEx, data []byte) ([]byte, error)
	FOnData func(httpClient *HttpClientEx, data []byte) ([]byte, error)
}

func NewHttpClientEx(url string, keepAlive bool, opt ...*common.HTTPClientOption) *HttpClientEx {
	index := strings.Index(url, "://")
	if index < 0 && url != "" {
		url = "http://" + url
	}
	client := &HttpClientEx{}
	client.url = url
	client.totalTimeout = 0
	client.timeout = 30
	client.maxDownloadSize = 10 * 1024 * 1024
	client.opt.Keepalive = keepAlive
	if len(opt) > 0 && opt[0] != nil {
		client.opt = *opt[0]
	}
	client.initHttpClient()
	client.headers = make(map[string]string)
	client.httpCookie.cookie = make(map[string]*HttpCookie)
	client.SetHttpHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36")
	return client
}

func (me *HttpClientEx) SetClient(client interface{}) {
	tmpClient, ok := client.(*http.Client)
	if ok {
		me.httpClient = tmpClient
	}
}
func (me *HttpClientEx) EnableProcess(v bool) {
	me.processInfo = v
}
func (me *HttpClientEx) SetMaxDownloadSize(size int) {
	me.maxDownloadSize = size
}

// return total, receive
func (me *HttpClientEx) GetProcess() (int64, int64) {
	return me.totalBytes.Get(), me.recvBytes.Get()
}

// second
func (me *HttpClientEx) SetTotalTimeout(v int) {
	me.totalTimeout = v
}

// second
func (me *HttpClientEx) SetTimeout(timeout int) {
	me.timeout = timeout
}
func (me *HttpClientEx) SetBasicAuth(user, pwd string) {
	me.enableBasicAuth = true
	me.authUser = user
	me.authPwd = pwd
}
func (me *HttpClientEx) newHTTPClient() (*http.Client, error) {
	tmpProxy := strings.ToLower(me.opt.Proxy)
	proxyType := ""
	host := me.opt.Proxy
	index := strings.Index(tmpProxy, "://")
	if index >= 0 {
		proxyType = tmpProxy[:index]
		host = tmpProxy[index+3:]
	}
	if proxyType == "" && me.opt.Proxy != "" {
		proxyType = "http"
		me.opt.Proxy = "http://" + me.opt.Proxy
	}
	var client *http.Client

	defer func() {
		if client == nil {
			return
		}
		tp, ok := client.Transport.(*http.Transport)
		if ok {
			if me.opt.Cert != "" {
				crt, err := tls.LoadX509KeyPair(me.opt.Cert, me.opt.CertKey)
				if err != nil {
					dbg.Dbg("Error: %s\n", err.Error())
					return
				}
				if tp.TLSClientConfig == nil {
					tp.TLSClientConfig = new(tls.Config)
				}
				if tp.TLSClientConfig != nil {
					tp.TLSClientConfig.Certificates = []tls.Certificate{crt}
				}
			}
		}
	}()

	switch proxyType {
	case "http", "https":
		u := url.URL{}
		urlProxy, err := u.Parse(me.opt.Proxy)
		if err != nil {
			fmt.Println("Parse Url Failed: ", err)
			return nil, err
		}
		client = &http.Client{
			Timeout: time.Duration(me.timeout) * time.Second,
			Transport: &http.Transport{
				TLSClientConfig: &tls.Config{InsecureSkipVerify: me.opt.InsecureSkipVerify},
				Proxy:           http.ProxyURL(urlProxy),
				DialContext: func(ctx context.Context, netw, addr string) (net.Conn, error) {
					//本地地址  ipaddr是本地外网IP
					lAddr, err := net.ResolveTCPAddr(netw, me.opt.OutIP+":0")
					if err != nil {
						return nil, err
					}
					//被请求的地址
					rAddr, err := net.ResolveTCPAddr(netw, addr)
					if err != nil {
						return nil, err
					}
					conn, err := net.DialTCP(netw, lAddr, rAddr)
					if err != nil {
						return nil, err
					}
					me.conn = conn
					/*
						deadline := time.Now().Add(10 * time.Second)
						_ = conn.SetDeadline(deadline)
					*/
					return conn, nil
				},
			},
		}
		return client, nil
	case "sock5", "sock4", "socks5", "socks4":
		index := strings.IndexByte(host, '@')
		userInfo := ""
		if index > 0 {
			userInfo = host[:index]
			host = host[index+1:]
		}
		index = strings.IndexByte(userInfo, ':')
		user := ""
		pwd := ""
		if index > 0 {
			user = userInfo[:index]
			pwd = userInfo[index+1:]
		}
		var auth *proxy.Auth
		if user != "" {
			auth = new(proxy.Auth)
			auth.User = user
			auth.Password = pwd
		}
		dialer, err := proxy.SOCKS5("tcp", host, auth, proxy.Direct)
		if err != nil {
			_, _ = fmt.Fprintln(os.Stderr, "can't connect to the proxy:", err)
			return nil, err
		}
		trans := new(http.Transport)
		trans.Dial = dialer.Dial
		client = &http.Client{
			Transport: trans,
		}
		return client, nil
	default:

	}

	client = &http.Client{
		Timeout: time.Duration(me.timeout) * time.Second,
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: me.opt.InsecureSkipVerify},
			DialContext: func(ctx context.Context, netw, addr string) (net.Conn, error) {
				//本地地址  ipaddr是本地外网IP
				lAddr, err := net.ResolveTCPAddr(netw, me.opt.OutIP+":0")
				if err != nil {
					return nil, err
				}
				//被请求的地址
				rAddr, err := net.ResolveTCPAddr(netw, addr)
				if err != nil {
					return nil, err
				}
				conn, err := net.DialTCP(netw, lAddr, rAddr)
				if err != nil {
					return nil, err
				}
				/*
					deadline := time.Now().Add(10 * time.Second)
					_ = conn.SetDeadline(deadline)
				*/
				return conn, nil
			},
		},
	}
	return client, nil
}

func (me *HttpClientEx) initHttpClient() {
	var err error
	me.httpClient, err = me.newHTTPClient()
	if err != nil {
		dbg.Dbg("Error: %s\n", err.Error())
	}
	me.SetKeepAlive(me.opt.Keepalive)
	tmpStr := strings.ToLower(me.url)
	index := strings.Index(tmpStr, "https://")
	if index == 0 {
		me.tlsMode = true
	}
}
func (me *HttpClientEx) SetBindIP(ip string) error {
	me.opt.OutIP = ip
	client, err := me.newHTTPClient()
	if err != nil {
		return err
	}
	me.httpClient = client
	return nil
}

func (me *HttpClientEx) SetProxy(proxyUrl string) error {
	me.opt.Proxy = proxyUrl
	client, err := me.newHTTPClient()
	if err != nil {
		return err
	}
	me.httpClient = client
	return nil
}

func (me *HttpClientEx) SetUserAgent(agent string) {
	me.SetHttpHeader("UserAgent", agent)
}

func (me *HttpClientEx) GetUrl() string {
	return me.url
}

func (me *HttpClientEx) SetTransport(transport *http.Transport) {
	me.httpClient.Transport = transport
}

func (me *HttpClientEx) GetTransport() *http.Transport {
	v := me.httpClient.Transport
	if v == nil {
		transport := new(http.Transport)
		me.httpClient.Transport = transport
		return transport
	} else {
		tr, _ := v.(*http.Transport)
		return tr
	}
}

func (me *HttpClientEx) SetKeepAlive(v bool) {
	var transport *http.Transport
	transport = me.GetTransport()
	if transport == nil {
		transport = new(http.Transport)
		me.httpClient.Transport = transport
	}
	transport.DisableKeepAlives = !v
}

func (me *HttpClientEx) Close() {
	me.SetKeepAlive(false)
	if sys.IsNil(me.conn) == false {
		_ = me.conn.Close()
	}
}

func (me *HttpClientEx) InsecureSkipVerify() {
	me.authServer = false
	tr := me.GetTransport()
	if tr == nil {
		tr = new(http.Transport)
		me.httpClient.Transport = tr
	}
	tlsConfig := &tls.Config{
		InsecureSkipVerify: true,
	}
	tr.TLSClientConfig = tlsConfig
}

func (me *HttpClientEx) SetCert(ca, cert, key string, authServer bool) error {
	me.httpCA = ca
	me.httpCert = cert
	me.httpKey = key
	me.authServer = authServer
	tr := me.GetTransport()
	if tr == nil {
		tr = new(http.Transport)
		me.httpClient.Transport = tr
	}

	pool := x509.NewCertPool()
	caCertPath := me.httpCA
	if me.httpCA != "" {
		caCertPath = sys.FormatDir(me.httpCA)
	}
	caCrt, err := ioutil.ReadFile(caCertPath)
	if err != nil && me.httpCA != "" {
		return err
	}
	pool.AppendCertsFromPEM(caCrt)
	certFile := me.httpCert
	if certFile != "" {
		certFile = sys.FormatDir(me.httpCert)
	}
	keyFile := me.httpKey
	if keyFile != "" {
		keyFile = sys.FormatDir(me.httpKey)
	}
	cliCrt, err := tls.LoadX509KeyPair(certFile, keyFile)
	if err != nil && certFile != "" {
		return err
	}
	tlsConfig := &tls.Config{
		RootCAs:      pool,
		Certificates: []tls.Certificate{cliCrt},
	}
	if authServer == false {
		tlsConfig.InsecureSkipVerify = true
	}
	tr.TLSClientConfig = tlsConfig
	return nil
}

func (me *HttpClientEx) SetTlsConfig(tlsConfig *tls.Config) {
	if tlsConfig != nil {
		tr := me.GetTransport()
		if tr != nil {
			tr.TLSClientConfig = tlsConfig
		}
	}
}

func (me *HttpClientEx) SetHttpHeader(name, value string) {
	me.headers[name] = value
}

func (me *HttpClientEx) SetHeaderFormUrl(encode string) {
	if encode == "" {
		encode = "utf8"
	}
	me.headers["Content-Type"] = "application/x-www-form-urlencoded;charset=" + encode
}

func (me *HttpClientEx) SetHeaderJson() {
	me.headers["Content-Type"] = "application/json"
}

func (me *HttpClientEx) SetUrl(url string) {
	index := strings.Index(url, "://")
	if index < 0 {
		url = "http://" + url
	}
	me.url = url
}

func (me *HttpClientEx) SetConnTimeout(timeout int) {
	me.httpClient.Transport = &http.Transport{
		Dial: func(netw, addr string) (net.Conn, error) {
			deadline := time.Now().Add(time.Duration(timeout) * time.Second)
			c, err := net.DialTimeout(netw, addr, time.Second*20)
			if err != nil {
				return nil, err
			}
			c.SetReadDeadline(deadline)
			return c, nil
		},
	}
}

func (me *HttpClientEx) Get(uri string) ([]byte, error) {
	me.action = "GET"
	v, err := me.request(uri, nil, nil)
	return v, err
}

func (me *HttpClientEx) Post(uri string, data []byte) ([]byte, error) {
	me.action = "POST"
	v, err := me.request(uri, data, nil)
	return v, err
}

func (me *HttpClientEx) Put(uri string, data []byte) ([]byte, error) {
	me.action = "PUT"
	v, err := me.request(uri, data, nil)
	return v, err
}

func (me *HttpClientEx) Option(uri string, data []byte) ([]byte, error) {
	me.action = "OPTIONS"
	v, err := me.request(uri, data, nil)
	return v, err
}

func (me *HttpClientEx) Delete(uri string, data []byte) ([]byte, error) {
	me.action = "DELETE"
	v, err := me.request(uri, data, nil)
	return v, err
}

func (me *HttpClientEx) Download(uri string, fh *os.File) (int64, error) {
	me.action = "GET"
	v, err := me.request(uri, nil, fh)
	if v != nil {
		return cast.ToInt64Value(string(v)), err
	} else {
		return 0, err
	}
}

func (me *HttpClientEx) GetContentType() string {
	if me.respHeader != nil {
		tmpStr, _ := me.respHeader["Content-Type"]
		if tmpStr != nil && len(tmpStr) > 0 {
			return tmpStr[0]
		}
	}
	return ""
}

func (me *HttpClientEx) GetHeader(name string) string {
	if me.respHeader != nil {
		tmpStr, _ := me.respHeader[name]
		if tmpStr != nil && len(tmpStr) > 0 {
			return tmpStr[0]
		}
	}
	return ""
}

func (me *HttpClientEx) GetHeaders() common.Header {
	return me.respHeader
}

func (me *HttpClientEx) GetStatusCode() int {
	return me.statusCode
}

type BodyReader struct {
	io.ReadCloser
	*bufio.Reader
}

func (me *BodyReader) ReadLine() (string, error) {
	line, _, err := me.Reader.ReadLine()
	if err != nil {
		return "", err
	}
	return string(line), nil
}

func (me *HttpClientEx) Request(uri string, method string, data []byte) (*BodyReader, error) {
	me.action = strings.ToUpper(method)
	var err error
	if me.url == "" && uri == "" {
		return nil, errors.New("url is empty")
	}
	if data != nil && len(data) > 0 && me.FOnSend != nil {
		data, err = me.FOnSend(me, data)
		if err != nil {
			fmt.Println("FOnSend Error: ", err)
			return nil, err
		}
	}
	me.uri = uri
	if data == nil {
		data = make([]byte, 0)
	}
	if me.timeout > 0 {
		me.httpClient.Timeout = time.Duration(me.timeout) * time.Second
	}
	host := me.url
	if me.uri != "" {
		host = xstring.ConnPath(host, me.uri)
	}
	index := strings.Index(host, "://")
	if index < 0 {
		host = "http://" + host
	}
	//dbg.Dbg("request Url: %s, action: %s\n", host, me.action)
	req, err := http.NewRequest(me.action, host, bytes.NewReader(data))
	if err != nil {
		return nil, err
	}
	if me.enableBasicAuth {
		req.SetBasicAuth(me.authUser, me.authPwd)
	}
	tmpHost := host
	index = strings.Index(host, "://")
	if index > 0 {
		tmpHost = host[index+3:]
	}
	index = strings.IndexByte(tmpHost, '/')
	if index > 0 {
		tmpHost = tmpHost[:index]
	}
	for k, v := range me.headers {
		req.Header.Set(k, v)
	}
	cookie := &me.httpCookie
	if cookie != nil {
		for _, v := range cookie.cookie {
			req.AddCookie(v.cookie)
		}
	}
	resp, err := me.httpClient.Do(req)
	if err == nil {
		me.respHeader = common.Header(resp.Header)
	} else {
		if resp != nil && resp.Body != nil {
			resp.Body.Close()
		}
		me.respHeader = nil
		dbg.Dbg("Error: %s\n", err.Error())
		return nil, err
	}
	me.statusCode = resp.StatusCode
	if resp.StatusCode >= 400 {
		//return nil, errors.New(fmt.Sprintf("http response: %d", resp.StatusCode))
	}
	c := resp.Cookies()
	for _, cookie := range c {
		cookies := &me.httpCookie
		if cookies == nil {
			fmt.Println("Create Http Cookie Failed")
			break
		}
		key := cookie.Name + cookie.Path
		existFlag := false
		for k, existCookie := range cookies.cookie {
			if k == key {
				existCookie.checked = true
				existCookie.cookie = cookie
				existFlag = true
				break
			}
		}
		if !existFlag {
			httpCookie := &HttpCookie{}
			httpCookie.cookie = cookie
			httpCookie.name = cookie.Name
			httpCookie.path = cookie.Path
			httpCookie.checked = true
			if sys.IsNil(me.httpClient.Jar) {
				cookies.cookie[cookie.Name+cookie.Path] = httpCookie
			}
			//fmt.Println("Add Cookie:", cookie.Name+cookie.Path, cookies.cookie)
		} else {
		}

		for k, existCookie := range cookies.cookie {
			if existCookie.checked == false {
				delete(cookies.cookie, k)
			}
		}
	}
	bodyReader := &BodyReader{ReadCloser: resp.Body}
	bodyReader.Reader = bufio.NewReader(resp.Body)
	return bodyReader, nil
}

func (me *HttpClientEx) request(uri string, data []byte, fh *os.File) ([]byte, error) {
	var err error
	if me.url == "" && uri == "" {
		return nil, errors.New("url is empty")
	}
	if data != nil && len(data) > 0 && me.FOnSend != nil {
		data, err = me.FOnSend(me, data)
		if err != nil {
			fmt.Println("FOnSend Error: ", err)
			return nil, err
		}
	}
	me.uri = uri
	if data == nil {
		data = make([]byte, 0)
	}
	if me.timeout > 0 {
		me.httpClient.Timeout = time.Duration(me.timeout) * time.Second
	}
	host := me.url
	if me.uri != "" {
		host = xstring.ConnPath(host, me.uri)
	}
	index := strings.Index(host, "://")
	if index < 0 {
		host = "http://" + host
	}
	//dbg.Dbg("request Url: %s\n", host)
	req, err := http.NewRequest(me.action, host, bytes.NewReader(data))
	if err != nil {
		return nil, err
	}
	if me.enableBasicAuth {
		req.SetBasicAuth(me.authUser, me.authPwd)
	}
	tmpHost := host
	index = strings.Index(host, "://")
	if index > 0 {
		tmpHost = host[index+3:]
	}
	index = strings.IndexByte(tmpHost, '/')
	if index > 0 {
		tmpHost = tmpHost[:index]
	}
	for k, v := range me.headers {
		req.Header.Set(k, v)
	}
	cookie := &me.httpCookie
	if cookie != nil {
		for _, v := range cookie.cookie {
			req.AddCookie(v.cookie)
		}
	}
	fileSize := int64(0)
	if fh != nil {
		st, err := fh.Stat()
		tmpSize := int64(0)
		if err != nil {
			return nil, err
		}
		tmpSize = st.Size()
		fileSize = tmpSize
		_, _ = fh.Seek(tmpSize, 0)
		if tmpSize > 0 {
			req.Header.Set("Range", "bytes="+fmt.Sprintf("%d", tmpSize)+"-")
		}
	}
	lastRecvTime := xtime.UnixTime()
	reqTime := xtime.UnixTime()
	if lastRecvTime < 0 {

	}
	if me.totalTimeout > 0 {
		ctx, cancel := context.WithCancel(context.Background()) // 获取一个上下文
		req = req.WithContext(ctx)
		ch := make(chan int, 1)
		defer func() {
			ch <- 1
			close(ch)
		}()
		go func() {
			for {
				select {
				case <-ch:
					//fmt.Println("http request normal exit")
					return
				case <-time.After(time.Second * 1):
					if xtime.CompTime(xtime.UnixTime(), reqTime) > int64(me.totalTimeout) {
						cancel()
						fmt.Printf("http client timeout with %ds\n", me.totalTimeout)
						return
					}
				}
			}
		}()
	}
	resp, err := me.httpClient.Do(req)
	if err == nil {
		me.respHeader = common.Header(resp.Header)
		defer resp.Body.Close()
	} else {
		if resp != nil && resp.Body != nil {
			resp.Body.Close()
		}
		me.respHeader = nil
		dbg.Dbg("Error: %s\n", err.Error())
		return nil, err
	}
	me.statusCode = resp.StatusCode
	if resp.StatusCode >= 400 {
		//return nil, errors.New(fmt.Sprintf("http response: %d", resp.StatusCode))
	}
	c := resp.Cookies()
	for _, cookie := range c {
		cookies := &me.httpCookie
		if cookies == nil {
			fmt.Println("Create Http Cookie Failed")
			break
		}
		key := cookie.Name + cookie.Path
		existFlag := false
		for k, existCookie := range cookies.cookie {
			if k == key {
				existCookie.checked = true
				existCookie.cookie = cookie
				existFlag = true
				break
			}
		}
		if !existFlag {
			httpCookie := &HttpCookie{}
			httpCookie.cookie = cookie
			httpCookie.name = cookie.Name
			httpCookie.path = cookie.Path
			httpCookie.checked = true
			if sys.IsNil(me.httpClient.Jar) {
				cookies.cookie[cookie.Name+cookie.Path] = httpCookie
			}
			//fmt.Println("Add Cookie:", cookie.Name+cookie.Path, cookies.cookie)
		} else {
		}

		for k, existCookie := range cookies.cookie {
			if existCookie.checked == false {
				delete(cookies.cookie, k)
			}
		}
	}
	me.totalBytes.Set(resp.ContentLength + fileSize)
	if fh != nil {
		/*
			_, err := io.Copy(fh, resp.Body)
			if err != nil {
				return nil, err
			}
		*/
		buffer := make([]byte, 8096)
		total := resp.ContentLength + fileSize
		per2 := int64(-1)
		for {
			lastRecvTime = xtime.UnixTime()
			n, err := resp.Body.Read(buffer)
			if err != nil {
				dbg.Dbg("Error: %s\n", err.Error())
			}
			if n > 0 {
				_, err := fh.Write(buffer[:n])
				if err != nil {
					dbg.Dbg("Error: %s\n", err.Error())
					return nil, err
				}
				fileSize += int64(n)
				me.recvBytes.Set(fileSize)
				if me.processInfo {
					if resp.ContentLength > 0 {
						per := fileSize * 100 / total
						if per != per2 {
							per2 = per
							tmpStr := fmt.Sprintf("Download: %d(%d%%)/%d", fileSize, per, total)
							fmt.Printf("\r%s", tmpStr)
						}
					} else {
						tmpStr := fmt.Sprintf("Download: %d", fileSize)
						fmt.Printf("\r%s", tmpStr)
					}
				}
			} else {
				if fileSize < total {
					return nil, err
				}
				break
			}
			if err == io.EOF {
				if fileSize < total {
					return nil, err
				}
				break
			}
			if err != nil {
				dbg.Dbg("Error: %+v\n", err)
				return nil, err
			}
		}
		if me.processInfo {
			fmt.Println("\nDownload OK")
		}
		st, err := fh.Stat()
		tmpSize := int64(0)
		if err == nil {
			tmpSize = st.Size()
		}
		tmpStr := fmt.Sprintf("%d", tmpSize)
		return []byte(tmpStr), nil
	}

	if resp.ContentLength > int64(me.maxDownloadSize) {
		return nil, errors.New("content too large")
	}
	me.bodyData = nil
	var body []byte
	httpBuf := new(bytes.Buffer)
	startTime := time.Now()
	per2 := int64(-1)
	for {
		buf := make([]byte, 4096)
		n, err := resp.Body.Read(buf)
		if n > 0 {
			lastRecvTime = xtime.UnixTime()
			httpBuf.Write(buf[:n])
			fileSize += int64(n)
			if me.processInfo {
				if resp.ContentLength > 0 {
					per := fileSize * 100 / resp.ContentLength
					if per != per2 {
						per2 = per
						tmpStr := fmt.Sprintf("Download: %d(%d%%)/%d", fileSize, per, resp.ContentLength)
						fmt.Printf("\r%s", tmpStr)
					}
				} else {
					tmpStr := fmt.Sprintf("Download: %d", fileSize)
					fmt.Printf("\r%s", tmpStr)
				}
			}
		}
		if err != nil {
			if me.processInfo {
				if resp.ContentLength > 0 && resp.ContentLength != int64(httpBuf.Len()) {
					fmt.Printf("Err: %s, %d:%d\n", err.Error(), resp.ContentLength, httpBuf.Len())
				}
				fmt.Println("\nCost:", time.Now().Sub(startTime))
			}
			if err == io.EOF {
				break
			} else {
				me.bodyData = httpBuf.Bytes()
				return nil, err
			}
		}
	}
	body = httpBuf.Bytes()
	/*
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return nil, err
		}
	*/
	me.bodyData = body
	if me.FOnData != nil && body != nil && len(body) > 0 {
		body, err = me.FOnData(me, body)
		if err != nil {
			fmt.Println("FOnData Error:", body, err)
			return nil, err
		}
	}
	return body, nil
}

func (me *HttpClientEx) GetBody() []byte {
	return me.bodyData
}

// /////////////////////////http client pool ///////////////////////////////
type HttpClientPool struct {
	httpQueue *xlist.Queue
	reqQueue  *xlist.Queue

	waitTimeout int
	allHttp     list.List
	max         int
	mutex       sync.Mutex
}

func NewHttpClientPool(max int) *HttpClientPool {
	resp := new(HttpClientPool)
	resp.httpQueue = xlist.NewQueue(20000)
	resp.reqQueue = xlist.NewQueue(5000)
	resp.max = max
	resp.waitTimeout = 10
	return resp
}

func (me *HttpClientPool) SetWaitTimeout(v int) {
	me.waitTimeout = v
}

func (me *HttpClientPool) GetHttp(url string, keepAlive bool, isNew *bool) (*HttpClientEx, error) {
	v, err := me.httpQueue.GetNoWait()
	if err == nil {
		h, ok := v.(*HttpClientEx)
		if ok && h != nil {
			return h, nil
		}
	}
	var retHttp *HttpClientEx
	me.mutex.Lock()
	if me.allHttp.Len() < me.max {
		retHttp = NewHttpClientEx(url, keepAlive)
		me.allHttp.PushBack(retHttp)
		if isNew != nil {
			*isNew = true
		}
	}
	me.mutex.Unlock()
	if retHttp != nil {
		return retHttp, nil
	}
	ch := make(chan *HttpClientEx, 1)
	_ = me.reqQueue.PutNoWait(ch)
	select {
	case h := <-ch:
		retHttp = h
	case <-time.After(time.Duration(me.waitTimeout) * time.Second):
		return nil, errors.New("wait free http timeout")
	}
	if retHttp != nil {
		retHttp.SetUrl(url)
		retHttp.SetKeepAlive(keepAlive)
		return retHttp, nil
	}
	return nil, errors.New("wait free http timeout")
}

func (me *HttpClientPool) Free(h *HttpClientEx) {
	v, err := me.reqQueue.GetNoWait()
	if err == nil {
		ch, ok := v.(chan *HttpClientEx)
		if ok && ch != nil {
			ch <- h
			close(ch)
			return
		}
	}
	_ = me.httpQueue.PutNoWait(h)
}

func NewCookieJar() *cookiejar.Jar {
	resp, _ := cookiejar.New(nil)
	return resp
}

func NewTransPort(proxy string, InsecureSkipVerify bool) *http.Transport {
	u := url.URL{}
	urlProxy, err := u.Parse(proxy)
	if err != nil {
		dbg.Dbg("Error: %s\n", err.Error())
		return nil
	}
	transPort := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: InsecureSkipVerify},
		Proxy:           http.ProxyURL(urlProxy),
	}
	return transPort
}

func (me *HttpClientEx) GetCookieJar() http.CookieJar {
	return me.httpClient.Jar
}

func (me *HttpClientEx) SetCookieJar(jar http.CookieJar) {
	me.httpClient.Jar = jar
}

func (me *HttpClientEx) SetContentJson() {
	me.SetHttpHeader("Content-Type", "application/json")
}

func (me *HttpClientEx) SetContentUrlEncode() {
	me.SetHttpHeader("Content-Type", "application/x-www-form-urlencoded")
}

func (me *HttpClientEx) AddCookie(name string, value string, path string) {
	if path == "" {
		path = "/"
	}
	c := new(HttpCookie)
	c.name = name
	c.cookie = new(http.Cookie)
	c.cookie.Path = "/"
	c.cookie.Name = name
	c.cookie.Value = value
	me.httpCookie.cookie[name] = c
}

type QueryCookie struct {
	Name  string
	Value string
	Path  string
}

func (me *HttpClientEx) GetCookie(name string) []*QueryCookie {
	c := me.respHeader["Set-Cookie"]
	resp := make([]*QueryCookie, 0)
	for _, v := range c {
		item := strings.Split(v, ";")
		for _, names := range item {
			index := strings.IndexByte(names, '=')
			if index <= 0 {
				continue
			}
			tmpName := strings.Trim(names[:index], " \r\n\t")
			tmpValue := strings.Trim(names[index+1:], " \r\n\t")
			newCookie := new(QueryCookie)
			switch strings.ToLower(tmpName) {
			case "expires":
			case "path":
				newCookie.Path = tmpValue
			case "max-age":
			default:
				if newCookie.Name == "" {
					newCookie.Name = tmpName
					newCookie.Value = tmpValue
				}
			}
			if newCookie.Name != "" {
				if name == "" || name == newCookie.Name {
					resp = append(resp, newCookie)
				}
			}
		}
	}
	return resp
}
