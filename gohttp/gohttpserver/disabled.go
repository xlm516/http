//go:build !GOHTTP_SERVER
// +build !GOHTTP_SERVER

package gohttpserver

import (
	"crypto/tls"
	"errors"
	"gitee.com/xlm516/http/common"
	"net"
	"net/http"
)

const (
	SESSION_COOKIE_NAME = "_SESSION_"
)

type GoHttpServer struct {
	ConnStatusChanged common.ConnStatusCallback
	DupFd             uintptr
	IsError           bool
}

func NewGoHttpServer() *GoHttpServer {
	resp := new(GoHttpServer)
	resp.init()
	return resp
}

func NewGoHttpFileHandler(rootDir string) interface{} {
	return nil
}

func GoHttpFileHandler(ctx *common.ECHttpContext, handle interface{}) {
}

func (me *GoHttpServer) init() {
	me.DupFd = 0xFFFFFFF
}
func (me *GoHttpServer) UpdateLastUrl(addr string, url string) {
}

func (me *GoHttpServer) GetListener() net.Listener {
	return nil
}

func (me *GoHttpServer) CloseListen() {
}

func (me *GoHttpServer) CloseAllClient() {
}

func (me *GoHttpServer) Lock() {
}

func (me *GoHttpServer) Unlock() {
}

func (me *GoHttpServer) SetFd(fd uintptr) uintptr {
	return me.DupFd
}

func (me *GoHttpServer) SetHandler(h common.ECHttpHandler) {
}

func (me *GoHttpServer) GetConnectList() map[string]*common.ConnInfo {
	return nil
}

func (me *GoHttpServer) GetConnInfo(remoteAddr string) *common.ConnInfo {
	return nil
}

func (me *GoHttpServer) SetTcpTimeout(s int) {
}

func (me *GoHttpServer) SetBindHost(host string) {
}

func (me *GoHttpServer) HandleFunc(webSite *common.WebSite) {
}

func (me *GoHttpServer) ClientStatus(c net.Conn, s http.ConnState) {
}

func (me *GoHttpServer) CheckConnection() {
}
func (me *GoHttpServer) CheckCookieExpire() {
}
func (me *GoHttpServer) SetCookie(name string, value interface{}, cookieName string, sessionName string,
	path string, timeout int) *common.ECHttpCookie {
	return nil
}
func (me *GoHttpServer) SetCookieTimeout(sessionName string, t int, url string) bool {
	return false
}
func (me *GoHttpServer) GetCookieAll(sessionName string, url string) map[string]interface{} {
	return nil
}

func (me *GoHttpServer) GetCookie(name string, sessionName string,
	url string) (*common.SessionVar, *common.ECHttpCookie) {
	return nil, nil
}
func (me *GoHttpServer) DelCookie(sessionName string, url string) {
}

func (me *GoHttpServer) Listen() (bool, error) {
	return false, errors.New("not support")
}
func (me *GoHttpServer) Start() (bool, error) {
	return false, errors.New("not support")
}

func (me *GoHttpServer) getNetTCPListener(addr string) (*net.TCPListener, error) {
	return nil, nil
}

func (me *GoHttpServer) ListenTLS(certFile, keyFile string, tlsConfig *tls.Config) (bool, error) {
	return false, nil
}
func (me *GoHttpServer) DumpCookie(limit int) []*common.Session {
	return nil
}
