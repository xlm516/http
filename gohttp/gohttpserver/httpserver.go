//go:build GOHTTP_SERVER
// +build GOHTTP_SERVER

package gohttpserver

import (
	"container/list"
	"crypto/tls"
	"errors"
	"fmt"
	"gitee.com/xlm516/http/common"
	"gitee.com/xlm516/xtool/encoding"
	xsync "gitee.com/xlm516/xtool/sync"
	"gitee.com/xlm516/xtool/sys"
	xtime "gitee.com/xlm516/xtool/time"
	"net"
	"net/http"
	"os"
	"sort"
	"strings"
	"sync"
	"time"
)

const (
	SESSION_COOKIE_NAME = "_SESSION_"
)

type GoHttpServer struct {
	ConnStatusChanged common.ConnStatusCallback

	bindHost        string
	server          *http.Server
	WebSite         *common.WebSite
	routeList       *list.List
	connList        map[string]*common.ConnInfo
	handler         common.ECHttpHandler
	mutex           *sync.Mutex
	cookieMutex     *sync.Mutex
	rand            *sys.RandEx
	session         map[string]*common.Session
	lastCheckCookie int64
	cookieTimeout   int
	connTimeout     int
	listener        net.Listener
	DupFd           uintptr
	IsError         bool
	bExit           bool
}

func NewGoHttpServer() *GoHttpServer {
	resp := new(GoHttpServer)
	resp.init()
	return resp
}

func NewGoHttpFileHandler(rootDir string) interface{} {
	return http.FileServer(http.FileSystem(http.Dir(rootDir)))
}

func GoHttpFileHandler(ctx *common.ECHttpContext, handle interface{}) {
	fsHandler, ok := handle.(http.Handler)
	if ok {
		w, r, err := ctx.ToGoHttpHandle()
		if err == nil {
			if w != nil && r != nil {
				fsHandler.ServeHTTP(w, r)
			}
		} else {
			fmt.Printf("FastHttpFileHandler: to fasthttp ctx failed\n")
		}
	} else {
		fmt.Printf("GoHttpFileHandler: to go http Request File Handle failed\n")
	}
}

func (me *GoHttpServer) init() {
	me.routeList = list.New()
	me.connList = make(map[string]*common.ConnInfo)
	me.mutex = new(sync.Mutex)
	me.cookieMutex = new(sync.Mutex)
	me.session = make(map[string]*common.Session)
	me.WebSite = &common.WebSite{}
	me.WebSite.Schedule = make(map[string]*common.WebSchedule)
	me.rand = sys.NewRand()
	me.lastCheckCookie = xtime.UnixTime()
	me.cookieTimeout = 5 * 60
	me.connTimeout = 1800
	me.DupFd = 0xFFFFFFF
}
func (me *GoHttpServer) UpdateLastUrl(addr string, url string) {
	me.Lock()
	tmpClient, ok := me.connList[addr]
	if ok {
		tmpClient.LastUrl = url
	}
	me.Unlock()
}
func (me *GoHttpServer) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if me.handler != nil {
		handler := new(common.ECHttpContext)
		handler.HttpType = common.GOHTTP
		handler.Response = common.NewECGoHttpResponse(w, r)
		handler.Request = common.NewECGoHttpRequest(w, r)
		handler.Www = nil
		handler.Http = me
		me.handler(handler)
		return
	}
	tmpUrl := ""
	secondUrl := ""
	if r.URL != nil {
		tmpUrl = r.URL.Path
	} else {
		tmpUrl = r.RequestURI
		index := strings.Index(r.RequestURI, "?")
		if index > 0 {
			tmpUrl = tmpUrl[:index]
		}
	}
	me.Lock()
	tmpClient, ok := me.connList[r.RemoteAddr]
	if ok {
		tmpClient.LastUrl = r.RequestURI
	}
	me.Unlock()

	for i := 0; i < len(me.WebSite.Www); i++ {
		//fmt.Println(tmpUrl, ":", me.WebSite.Www[i].Uri)
		index := strings.Index(tmpUrl, me.WebSite.Www[i].Uri)
		if index == 0 {
			secondUrl = tmpUrl[len(me.WebSite.Www[i].Uri):]
			//fmt.Println("SecondUrl:", secondUrl)
			if me.WebSite.Www[i].Handler != nil {
				me.WebSite.Www[i].SecondUri = secondUrl

				handler := new(common.ECHttpContext)
				handler.Response = common.NewECGoHttpResponse(w, r)
				handler.Request = common.NewECGoHttpRequest(w, r)
				handler.Www = &me.WebSite.Www[i]
				handler.Http = me
				me.WebSite.Www[i].Handler(handler)
				return
			}
		}
	}
}

func (me *GoHttpServer) GetListener() net.Listener {
	return me.listener
}

func (me *GoHttpServer) CloseListen() {
	if me != nil && me.listener != nil {
		me.listener.Close()
	}
}

func (me *GoHttpServer) CloseAllClient() {
	me.Lock()
	defer me.Unlock()
	for k, v := range me.connList {
		if v.Conn != nil {
			v.Conn.Close()
		}
		delete(me.connList, k)
	}
}

func (me *GoHttpServer) Lock() {
	me.mutex.Lock()
}

func (me *GoHttpServer) Unlock() {
	me.mutex.Unlock()
}

func (me *GoHttpServer) SetFd(fd uintptr) uintptr {
	if fd == 0 {
		return me.DupFd
	}
	me.DupFd = fd
	return me.DupFd
}

func (me *GoHttpServer) SetHandler(h common.ECHttpHandler) {
	me.handler = h
}

func (me *GoHttpServer) GetConnectList() map[string]*common.ConnInfo {
	return me.connList
}

func (me *GoHttpServer) GetConnInfo(remoteAddr string) *common.ConnInfo {
	me.Lock()
	defer me.Unlock()
	conn, ok := me.connList[remoteAddr]
	if ok {
		return conn
	} else {
		return nil
	}
}

func (me *GoHttpServer) SetTcpTimeout(s int) {
	me.connTimeout = s
}

func (me *GoHttpServer) SetBindHost(host string) {
	me.bindHost = host
}

func (me *GoHttpServer) HandleFunc(webSite *common.WebSite) {
	me.WebSite = webSite
	if me.WebSite.Schedule == nil {
		me.WebSite.Schedule = make(map[string]*common.WebSchedule)
	}
	if me.WebSite.ScheduleMutex == nil {
		me.WebSite.ScheduleMutex = xsync.NewMutex("ScheduleMutex")
	}
}

func (me *GoHttpServer) ClientStatus(c net.Conn, s http.ConnState) {
	me.Lock()
	defer me.Unlock()
	tmpState := common.StateIdle
	switch s {
	case http.StateNew:
		newConn := &common.ConnInfo{}
		newConn.Conn = c
		newConn.CreateTime = xtime.UnixTime()
		newConn.UpdateTime = newConn.CreateTime
		newConn.State = common.StateNew
		tmpState = newConn.State
		newConn.SessData = make(map[string]string)
		me.connList[c.RemoteAddr().String()] = newConn
		tcpConn, ok := c.(*net.TCPConn)
		if ok {
			_ = tcpConn.SetReadBuffer(32 * 1024)
			_ = tcpConn.SetWriteBuffer(32 * 1024)
		}
		//fmt.Printf("new connection, addr=%s\n", c.RemoteAddr().String())
		break

	case http.StateActive:
		newConn, ok := me.connList[c.RemoteAddr().String()]
		if ok {
			newConn.UpdateTime = xtime.UnixTime()
			newConn.State = common.StateActive
			tmpState = newConn.State
		}
		//fmt.Println("active connection")
		break

	case http.StateClosed:
		//fmt.Println("close connection:", c.RemoteAddr().String())
		delete(me.connList, c.RemoteAddr().String())
		tmpState = common.StateClosed
		break

	case http.StateIdle:
		//fmt.Println("idle connection")
		newConn, ok := me.connList[c.RemoteAddr().String()]
		if ok {
			newConn.UpdateTime = xtime.UnixTime()
			newConn.State = common.StateIdle
			tmpState = newConn.State
		}
		break

	case http.StateHijacked:
		//c.Close()
		delete(me.connList, c.RemoteAddr().String())
		//fmt.Println("Hijacked connection")
		tmpState = common.StateHijacked
		break

	default:
		break
	}

	if me.ConnStatusChanged != nil {
		if me.ConnStatusChanged(c, tmpState) {
			return
		}
	}
}

func (me *GoHttpServer) CheckConnection() {
	for {
		if me.bExit {
			return
		}
		time.Sleep(time.Second * 3)
		nowTime := xtime.UnixTime()
		me.Lock()
		for _, conn := range me.connList {
			if xtime.CompTime(nowTime, conn.UpdateTime) >= int64(me.connTimeout) {
				conn.Conn.Close()
				fmt.Printf("CheckConnection, Timeout close connect %s, timeout=%d\n",
					conn.Conn.RemoteAddr().String(), me.connTimeout)
				delete(me.connList, conn.Conn.RemoteAddr().String())
			}
		}
		me.Unlock()
	}
}
func (me *GoHttpServer) CheckCookieExpire() {
	me.cookieMutex.Lock()
	defer me.cookieMutex.Unlock()
	nowTime := xtime.UnixTime()
	if xtime.CompTime(nowTime, me.lastCheckCookie) < 120 {
		return
	}
	me.lastCheckCookie = nowTime
	//fmt.Println("Check Cookie Expire")
	for k, v := range me.session {
		t := v.Timeout
		if t == 0 {
			t = me.cookieTimeout
		}
		interval := xtime.CompTime(nowTime, v.CreateTime)
		if interval >= int64(t) || (v.Count < 1 && interval > 1) {
			delete(me.session, k)
		}
	}
}
func (me *GoHttpServer) SetCookie(name string, value interface{}, cookieName string, sessionName string,
	path string, timeout int) *common.ECHttpCookie {
	me.CheckCookieExpire()
	if cookieName == "" {
		fmt.Println("cookie name is empty")
	}
	v := &common.SessionVar{}
	v.Name = name
	v.Value = value
	v.CreateTime = xtime.UnixTime()
	me.cookieMutex.Lock()
	defer me.cookieMutex.Unlock()
	if path == "" {
		path = "/"
	}
	key := sessionName + path
	s, ok := me.session[key]
	if ok && sessionName != "" {
		if timeout > 0 {
			s.Timeout = timeout
		}
		s.CreateTime = xtime.UnixTime()
		if s.SessionVar == nil {
			s.SessionVar = make(map[string]*common.SessionVar)
		}
		v2, ok := s.SessionVar[name]
		if ok {
			if value == "" {
				delete(s.SessionVar, name)
			} else {
				v2.Value = value
			}
		} else if name != "" && value != "" {
			s.SessionVar[name] = v
		}
		return nil
	} else {
		if len(me.session) > 100*10000 {
			return nil
		}
		c := &http.Cookie{}
		c.Name = cookieName
		c.Value = encoding.Md516(fmt.Sprintf("%d%d", xtime.UnixTime(), me.rand.Int(8)))
		sessionName = c.Value
		key = sessionName + path
		c.Path = path
		s := &common.Session{}
		if timeout > 0 {
			s.Timeout = timeout
		}
		s.Name = c.Value
		s.Path = path
		s.CreateTime = xtime.UnixTime()
		s.SessionVar = make(map[string]*common.SessionVar)
		s.SessionVar[name] = v
		me.session[key] = s
		ecCookie := new(common.ECHttpCookie)
		ecCookie.Cookie = c
		ecCookie.CookieInfo.Name = c.Name
		ecCookie.CookieInfo.Value = c.Value
		ecCookie.CookieInfo.Path = c.Path
		ecCookie.HttpType = common.GOHTTP
		return ecCookie
	}
}
func (me *GoHttpServer) SetCookieTimeout(sessionName string, t int, url string) bool {
	me.CheckCookieExpire()
	me.cookieMutex.Lock()
	defer me.cookieMutex.Unlock()
	s, ok := me.session[sessionName+url]
	if ok {
		s.Timeout = t
		return true
	} else {
		return false
	}
}
func (me *GoHttpServer) GetCookieAll(sessionName string, url string) map[string]interface{} {
	me.cookieMutex.Lock()
	defer me.cookieMutex.Unlock()
	c, ok := me.session[sessionName+url]
	if ok {
		timeout := me.cookieTimeout
		if c.Timeout > 0 {
			timeout = c.Timeout
		}
		if xtime.CompTime(xtime.UnixTime(), c.CreateTime) >= int64(timeout) {
			delete(me.session, sessionName)
			return nil
		}
		newSession := make(map[string]interface{})
		for k, v := range c.SessionVar {
			newSession[k] = v.Value
		}
		return newSession
	}
	return nil
}

func (me *GoHttpServer) DumpCookie(limit int) []*common.Session {
	me.cookieMutex.Lock()
	defer me.cookieMutex.Unlock()
	resp := make([]*common.Session, 0)
	n := 0
	for _, v := range me.session {
		if limit > 0 && n > limit {
			break
		}
		item := new(common.Session)
		*item = *v
		item.SessionVar = make(map[string]*common.SessionVar)
		for k, v := range v.SessionVar {
			s := new(common.SessionVar)
			*s = *v
			s.Value = fmt.Sprintf("%+v\n", v.Value)
			item.SessionVar[k] = s
		}
		resp = append(resp, item)
		n++
	}
	sort.Sort(common.SessionSlice(resp))
	return resp
}

func (me *GoHttpServer) GetCookie(name string, sessionName string, url string) (*common.SessionVar, *common.ECHttpCookie) {
	me.CheckCookieExpire()
	me.cookieMutex.Lock()
	defer me.cookieMutex.Unlock()
	for {
		s, ok := me.session[sessionName+url]
		if ok {
			timeout := me.cookieTimeout
			if s.Timeout > 0 {
				timeout = s.Timeout
			}
			if xtime.CompTime(xtime.UnixTime(), s.CreateTime) >= int64(timeout) {
				delete(me.session, sessionName)
				return nil, nil
			}

			s.CreateTime = xtime.UnixTime()
			s.Count++
			v, ok := s.SessionVar[name]
			if ok {
				return v, nil
			} else {
				return nil, nil
			}
		} else {
			if len(url) <= 1 {
				break
			}
			index := strings.LastIndexByte(url, '/')
			if index < 0 {
				index = strings.LastIndexByte(url, '\\')
			}
			if index >= 0 {
				url = url[:index]
				if url == "" {
					url = "/"
				}
			}
			if index < 0 {
				break
			}
		}
	}
	return nil, nil
	/*
		c := &http.Cookie{}
		c.Name = SESSION_COOKIE_NAME
		c.Value = fmt.Sprintf("%d%d", UnixTime(), me.rand.Int(8))
		c.Path = "/"
		s := &Session{}
		s.name = c.Value
		s.sessionVar = make(map[string]*SessionVar)
		me.session[c.Value] = s
		return nil, c
	*/
}
func (me *GoHttpServer) DelCookie(sessionName string, url string) {
	me.cookieMutex.Lock()
	defer me.cookieMutex.Unlock()
	_, ok := me.session[sessionName+url]
	if ok {
		delete(me.session, sessionName+url)
	}
}

func (me *GoHttpServer) Listen() (bool, error) {
	me.server = &http.Server{
		Addr:      me.bindHost,
		ConnState: me.ClientStatus,
		//Handler:   http.TimeoutHandler(me, time.Second*60, "Server Timeout@@@"),
		Handler: me,
	}
	go me.CheckConnection()
	//listener, err := net.ListenTCP("tcp", tcpAddr)
	listener, err := me.getNetTCPListener(me.bindHost)
	if listener == nil || err != nil {
		me.bExit = true
		me.IsError = true
		fmt.Printf("GoHttpServer Listen %s Failed, err:%+v\n", me.bindHost, err)
		return false, err
	}
	me.listener = listener
	return true, nil
}
func (me *GoHttpServer) Start() (bool, error) {
	//err := server.ListenAndServe()
	if me.listener == nil {
		fmt.Printf("GoHttpServer: Please Call Listen firstly\n")
		return false, errors.New("Please Call Listen firstly")
	}
	err := me.server.Serve(me.listener)
	if err != nil {
		me.IsError = true
		me.bExit = true
		fmt.Printf("Http Server Exit at [%s] error:, %s\n", me.bindHost, err)
		return false, err
	}
	return true, nil
}

func (me *GoHttpServer) getNetTCPListener(addr string) (*net.TCPListener, error) {
	var ln net.Listener
	var err error

	if me.DupFd != 0xFFFFFFF {
		file := os.NewFile(me.DupFd, "")
		ln, err = net.FileListener(file)
		if err != nil {
			err = fmt.Errorf("net.FileListener error: %v", err)
			fmt.Println(err.Error())
			return nil, err
		} else {
			fmt.Println("Dup FD OK:", me.DupFd)
		}
	} else {
		ln, err = net.Listen("tcp", addr)
		if err != nil {
			err = fmt.Errorf("net.Listen error: %v", err)
			return nil, err
		}
	}

	return ln.(*net.TCPListener), nil
}

func (me *GoHttpServer) ListenTLS(certFile, keyFile string, tlsConfig *tls.Config) (bool, error) {
	me.server = &http.Server{
		Addr:      me.bindHost,
		ConnState: me.ClientStatus,
		//Handler:   http.TimeoutHandler(me, time.Second*60, "Server Timeout@@@"),
		Handler: me,
	}
	go me.CheckConnection()

	if tlsConfig != nil {
		me.server.TLSConfig = tlsConfig
	}

	addr := me.bindHost
	if addr == "" {
		addr = ":https"
	}

	config := &tls.Config{}
	if me.server.TLSConfig != nil {
		*config = *me.server.TLSConfig
	}
	if config.NextProtos == nil {
		config.NextProtos = []string{"http/1.1"}
	}

	var err error
	config.Certificates = make([]tls.Certificate, 1)
	config.Certificates[0], err = tls.LoadX509KeyPair(certFile, keyFile)
	if err != nil {
		return false, err
	}

	ln, err := me.getNetTCPListener(addr)
	if err != nil {
		return false, err
	}

	listener := tls.NewListener(ln, config)
	me.listener = listener
	return true, nil
}
