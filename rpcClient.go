package xhttp

import (
	"encoding/json"
	"errors"
	"fmt"
	"gitee.com/xlm516/http/common"
	"gitee.com/xlm516/http/gohttp/gohttpclient"
	"gitee.com/xlm516/xtool/cast"
	"gitee.com/xlm516/xtool/dbg"
	"gitee.com/xlm516/xtool/encoding"
	xid "gitee.com/xlm516/xtool/id"
	xlist "gitee.com/xlm516/xtool/list"
	xreflect "gitee.com/xlm516/xtool/reflect"
	"gitee.com/xlm516/xtool/sys"
	xtime "gitee.com/xlm516/xtool/time"
	"reflect"
	"strings"
)

type ECRPCError struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
	Nonce   string `json:"nonce"`
}

type ECRPCResponse struct {
	Err    ECRPCError  `json:"error"`
	Result interface{} `json:"result"`
}

type ECRPCClient struct {
	httpClient   *gohttpclient.HttpClientEx
	sockClient   *ECSocketClient
	user         string
	pwd          string
	nonce        string
	isLogin      bool
	id           int64
	rpcResp      ECRPCResponse
	respData     []byte
	returnValues []interface{}
	secMode      int //SECURITY_NEGO_ZIP_3DES
	isNego       bool
	token        string
	privKey      string
	userData     map[string]interface{}
	sessionKey   string
}

func NewECRPCClientEncrpty(url string) *ECRPCClient {
	index := strings.Index(url, "://")
	if index < 0 {
		url = "http://" + url
	}
	c := &ECRPCClient{}
	c.httpClient = gohttpclient.NewHttpClientEx(url, true)
	c.id = int64(sys.RandomUint32())
	c.userData = make(map[string]interface{})

	c.SetKeepAlive(false)
	c.SetSecurityMode(SECURITY_HTTP_AES_KIV)
	return c
}

func NewECRPCClient(url string) *ECRPCClient {
	index := strings.Index(url, "://")
	if index < 0 {
		url = "http://" + url
	}
	c := &ECRPCClient{}
	c.httpClient = gohttpclient.NewHttpClientEx(url, true)
	c.id = int64(sys.RandomUint32())
	c.userData = make(map[string]interface{})
	return c
}

func (me *ECRPCClient) SetProxy(proxy string) {
	if me.httpClient != nil {
		me.httpClient.SetProxy(proxy)
	}
}

func (me *ECRPCClient) SetBasicAuth(user, pwd string) {
	me.httpClient.SetBasicAuth(user, pwd)
}

func (me *ECRPCClient) SetHttpClient(c *gohttpclient.HttpClientEx) {
	me.httpClient = c
	me.SetSecurityMode(me.secMode)
}

func (me *ECRPCClient) SetUrl(url string) {
	if me.httpClient != nil {
		me.httpClient.SetUrl(url)
	}
}

func (me *ECRPCClient) GetUrl() string {
	return me.httpClient.GetUrl()
}
func (me *ECRPCClient) SetKeepAlive(v bool) {
	me.httpClient.SetKeepAlive(v)
}
func (me *ECRPCClient) SetUserString(name, value string) {
	me.userData[name] = value
}

// second
func (me *ECRPCClient) SetTimeout(second int) {
	me.httpClient.SetTimeout(second)
}

func (me *ECRPCClient) SetTotalTimeout(second int) {
	me.httpClient.SetTotalTimeout(second)
}

func (me *ECRPCClient) Close() {
	if me.httpClient != nil {
		me.httpClient.Close()
	}
}

func (me *ECRPCClient) GetUserString(name string) string {
	ret, ok := me.userData[name]
	if ok {
		return fmt.Sprintf("%v", ret)
	} else {
		return ""
	}
}

func (me *ECRPCClient) FOnSend(httpClient *gohttpclient.HttpClientEx, data []byte) ([]byte, error) {
	nonce := xid.NewShortKey()
	me.SetUserString("nonce", nonce)
	b, err := encoding.AesEncodeByKIV(data, nonce, nil, nil, true)
	httpClient.SetHttpHeader("HttpEncode", "true")
	return b, err
}

func (me *ECRPCClient) FOnData(httpClient *gohttpclient.HttpClientEx, data []byte) ([]byte, error) {
	if data == nil || len(data) <= 0 {
		return nil, errors.New("hex data is empty")
	}
	b, kiv, err := encoding.AesDecodeByKIV(data, nil, nil, true)
	if err == nil && kiv != nil {
		nonce := me.GetUserString("nonce")
		if kiv.Nonce != nonce {
			return nil, errors.New("invalid nonce")
		}
	}
	if err != nil {
		if data[0] == '{' {
			return data, nil
		}
	}
	return b, err
}

func (me *ECRPCClient) SetHttpHeader(name, value string) {
	me.httpClient.SetHttpHeader(name, value)
}

func (me *ECRPCClient) SetSecurityMode(v int) {
	if v == SECURITY_HTTP_AES_KIV {
		me.secMode = v
		me.httpClient.FOnSend = me.FOnSend
		me.httpClient.FOnData = me.FOnData
	} else {
		me.secMode = v
		me.httpClient.FOnSend = nil
		me.httpClient.FOnData = nil
	}
}

func (me *ECRPCClient) SetClientSecurityMode(v bool) {
	if v {
		me.httpClient.FOnSend = me.FOnSend
		me.httpClient.FOnData = me.FOnData
	} else {
		me.httpClient.FOnSend = nil
		me.httpClient.FOnData = nil
	}
}

func (me *ECRPCClient) InsecureSkipVerify() {
	me.httpClient.InsecureSkipVerify()
}

func (me *ECRPCClient) SetTokenKey(token, key string) {
	me.token = token
	me.privKey = key
}

func (me *ECRPCClient) SetSessionKey(key string) {
	me.sessionKey = key
}

func (me *ECRPCClient) SetCert(ca, cert, key string, authServer bool) error {
	return me.httpClient.SetCert(ca, cert, key, authServer)
}

func (me *ECRPCClient) SetSockBind(bindHost string) {
	if me.sockClient != nil {
		me.sockClient.SetHost(bindHost)
	} else {
		me.sockClient = NewECSocketClient(bindHost)
	}
}

func (me *ECRPCClient) SetUser(user, pwd string) {
	me.user = user
	me.pwd = pwd
}

func (me *ECRPCClient) CallSocket(output interface{}, method string, args ...interface{}) error {
	if me.sockClient == nil {
		return errors.New("do not init socket client")
	}
	me.sockClient.Connect()
	b, err := me.buildRequest(method, args...)
	if err != nil {
		return err
	}
	me.sockClient.WriteQueue(1, b, len(b))
	return nil
}

func (me *ECRPCClient) buildRequest(method string, args ...interface{}) ([]byte, error) {
	jsonObj := make(map[string]interface{})
	funcArgs := make([]interface{}, len(args))
	if len(args) > 0 {
		for k, v := range args {
			funcArgs[k] = v
		}
	}
	jsonObj["jsonrpc"] = "2.0"
	jsonObj["method"] = method
	if me == nil {
		dbg.Dbg("do not init \n")
		return nil, errors.New("do not init")
	}
	me.id = (me.id + 1) % 10000
	jsonObj["id"] = fmt.Sprintf("%d%04d", xtime.UnixTime(), me.id)
	jsonObj["params"] = funcArgs
	if me.nonce != "" && me.isLogin == false {
		jsonObj["user"] = me.user
		jsonObj["token"] = encoding.Md5(me.nonce + me.pwd)
	}
	if me.secMode == SECURITY_HTTP_AES_KIV {
		if me.sessionKey != "" {
			jsonObj["sessionKey"] = encoding.Md5(me.sessionKey)
		}
	}
	funcData, err := json.Marshal(jsonObj)
	if err != nil {
		return nil, err
	}
	if me.secMode == SECURITY_NEGO_ZIP_3DES && me.isNego {
		me.nonce = xid.NewShortKey()
		data := JsonRPCEncodeData(me.secMode, funcData, me.privKey, me.nonce)
		if data != nil {
			funcData = data
		}
	}
	return funcData, nil
}

func (me *ECRPCClient) processData(retInterface interface{}, data []byte) (int, error) {
	me.rpcResp.Err = ECRPCError{}
	userInput := false
	tmpType := reflect.TypeOf(retInterface)
	if retInterface != nil && (tmpType.Kind() == reflect.Ptr) {
		if tmpType.Kind() == reflect.Ptr {
			me.rpcResp.Result = retInterface
			userInput = true
		}
	} else {
		me.rpcResp.Result = nil
	}
	if me.secMode != SECURITY_NULL && me.isNego {
		b, header := JsonRPCDecodeData(me.secMode, data, me.privKey)
		err := json.Unmarshal(b, &me.rpcResp)
		if err != nil {
			tmpStr := string(b)
			l := len(tmpStr)
			if l > 32 {
				l = 32
			}
			dbg.Dbg("%s\n", err.Error()+":"+tmpStr[:l])
			return 0, err
		}
		if me.rpcResp.Err.Code == 0 && header.Nonce != me.nonce {
			return 0, errors.New("invalid response nonce")
		}
	} else {
		err := json.Unmarshal(data, &me.rpcResp)
		if err != nil {
			me.rpcResp.Result = nil
			err = json.Unmarshal(data, &me.rpcResp)
			isStruct := false
			isSlice := false
			if sys.IsNil(retInterface) == false && tmpType.Elem().Kind() == reflect.Struct {
				isStruct = true
			}
			if sys.IsNil(retInterface) == false && tmpType.Elem().Kind() == reflect.Slice {
				isSlice = true
			}
			tmpArray, ok := me.rpcResp.Result.([]interface{})
			if ok && len(tmpArray) > 0 {
				if isStruct {
					_ = xreflect.ReflectFillStruct(retInterface, tmpArray[0])
				} else if isSlice == false {
					_ = cast.ConvertAssign(retInterface, tmpArray[0])
				}
			}
		}
		if err != nil {
			l := len(data)
			if l > 1024 {
				l = 1024
			}
			dbg.Dbg("Error: %+v, response:%s\n", retInterface, string(data))
			return 0, fmt.Errorf("invalid json: %s", string(data[:l]))
		}
	}
	if userInput == true && tmpType.Kind() == reflect.Ptr && tmpType.Elem().Kind() == reflect.Slice {
		tmpArray, ok := me.rpcResp.Result.([]interface{})
		if ok && len(tmpArray) > 0 {
			tmpArray2, ok := tmpArray[0].([]interface{})
			if ok {
				err := xreflect.ReflectFillSlice(retInterface, tmpArray2)
				if err != nil {
					dbg.Dbg("Error: %s\n", err.Error())
				}
			} else {
				err := xreflect.ReflectFillSlice(retInterface, tmpArray)
				if err != nil {
					dbg.Dbg("Error: %s\n", err.Error())
				}
			}
		}
	}
	rpcResp := &me.rpcResp
	if rpcResp.Err.Nonce != "" {
		me.nonce = rpcResp.Err.Nonce
	}
	if rpcResp.Err.Code != 0 {
		return rpcResp.Err.Code, errors.New(rpcResp.Err.Message)
	}
	if rpcResp.Result == nil {
		return 0, errors.New("No Result: " + string(data))
	}
	if userInput == false {
		var ok bool
		me.returnValues, ok = rpcResp.Result.([]interface{})
		if ok == false {
			me.returnValues = make([]interface{}, 1)
			me.returnValues[0] = rpcResp.Result
		}
	} else {
		me.returnValues = make([]interface{}, 1)
		me.returnValues[0] = rpcResp.Result
	}
	//fmt.Println("RPCData: ", string(retData))
	return 0, nil
}

func (me *ECRPCClient) doNego(force bool) (*NegoResponse, string) {
	req := &NegoRequest{}
	resp := &NegoResponse{}
	errMsg := ""
	if me.secMode == SECURITY_HTTP_AES_KIV {
		return nil, errMsg
	}
	if me.secMode != SECURITY_NULL || force {
		req.Token = me.token
		_, err := me._Call("nego", resp, req)
		if err == nil {
			me.isNego = true
			me.secMode = resp.AuthMode
			dbg.Dbg("do nego ok, mode=%d\n", resp.AuthMode)
			return resp, ""
		} else {
			if ECErrorConnectFailed(err) {
				errMsg = "Connect Server Failed"
			} else if ECErrorTimeout(err) {
				errMsg = "Connect Server Timeout"
			} else {
				errMsg = "RPC Nego Failed: " + err.Error()
			}
			dbg.Dbg("Do Nego Ok Failed, %s\n", err.Error())
			me.isNego = false
			return resp, errMsg
		}
	}
	return nil, errMsg
}

func (me *ECRPCClient) Call(method string, args ...interface{}) error {
	if me.isNego == false {
		resp, errMsg := me.doNego(false)
		if me.secMode == SECURITY_NEGO_ZIP_3DES {
			if resp == nil || me.isNego == false {
				errStr := "rpc nego failed"
				if errMsg != "" {
					errStr = errMsg
				}
				return errors.New(errStr)
			}
		}
	}
	code, err := me._Call(method, nil, args...)
	if code == JRPC_ERR_NOT_LOGIN {
		dbg.Dbg("doLogin, nonce=%s\n", me.nonce)
		code, err = me._Call(method, nil, args...)
	}
	if code == EC_JSON_NOT_NEGO {
		me.isNego = false
		resp, errMsg := me.doNego(true)
		if resp != nil {
			code, err = me._Call(method, nil, args...)
		} else {
			if errMsg != "" {
				return errors.New(errMsg)
			}
		}
	}
	return err
}

func (me *ECRPCClient) Call2(output interface{}, method string, args ...interface{}) error {
	if me.isNego == false {
		resp, errMsg := me.doNego(false)
		if me.secMode == SECURITY_NEGO_ZIP_3DES {
			if resp == nil || me.isNego == false {
				errStr := "rpc nego failed"
				if errMsg != "" {
					errStr = errMsg
				}
				return errors.New(errStr)
			}
		}
	}
	code, err := me._Call(method, output, args...)
	if code == JRPC_ERR_NOT_LOGIN {
		dbg.Dbg("doLogin, nonce=%s\n", me.nonce)
		code, err = me._Call(method, output, args...)
	}
	if code == EC_JSON_NOT_NEGO {
		me.isNego = false
		resp, errMsg := me.doNego(true)
		if resp != nil {
			code, err = me._Call(method, output, args...)
		} else {
			if errMsg != "" {
				return errors.New(errMsg)
			}
		}
	}
	return err
}

func (me *ECRPCClient) BuildRequestData(method string, args ...interface{}) ([]byte, error) {
	funcData, err := me.buildRequest(method, args...)
	if err != nil {
		return nil, err
	}
	return funcData, nil
}

func (me *ECRPCClient) ProcessJson(retInterface interface{}, data []byte) {
	me.processData(retInterface, data)
}

func (me *ECRPCClient) _Call(method string, retInterface interface{}, args ...interface{}) (int, error) {
	funcData, err := me.buildRequest(method, args...)
	if err != nil {
		return 0, nil
	}
	//Dbg.Dbg("PostData: %s\n", funcData)
	me.httpClient.SetHttpHeader("Content-Type", "application/json")
	retData, err := me.httpClient.Post("", funcData)
	me.respData = retData
	if err != nil {
		timeout := IsHttpTimeout(err)
		connFailed := IsHttpConnFailed(err)
		ecErr := NewECError(err.Error(), me.GetErrorCode())
		ecErr.timeout = timeout
		ecErr.connFailed = connFailed
		return 0, ecErr
	}
	if len(retData) == 0 {
		return 0, errors.New("Empty response data")
	}
	//Dbg.Dbg("%s\n", retData)
	n, err := me.processData(retInterface, retData)
	if err != nil {
		return n, NewECError(err.Error(), me.GetErrorCode())
	} else {
		return n, nil
	}
}

func (me *ECRPCClient) GetRespData() []byte {
	if me.respData != nil {
		return me.respData
	} else {
		return make([]byte, 0)
	}
}

func (me *ECRPCClient) checkReturns(index int) error {
	if me.returnValues == nil {
		return errors.New("Invalid Index")
	}
	if index < 0 && index >= len(me.returnValues) {
		return errors.New("Invalid Index")
	}
	return nil
}

func (me *ECRPCClient) GetReturn() []interface{} {
	return me.returnValues
}

func (me *ECRPCClient) GetErrorCode() int {
	return me.rpcResp.Err.Code
}

func (me *ECRPCClient) GetResult() []interface{} {
	return me.returnValues
}

func (me *ECRPCClient) RetAsString(index int) (string, error) {
	err := me.checkReturns(index)
	if err != nil {
		return "", err
	}
	tmpStr, ok := me.returnValues[index].(string)
	if ok == false {
		return "", errors.New("Is not string")
	}
	return tmpStr, nil
}

func (me *ECRPCClient) RetAsInt(index int) (int64, error) {
	err := me.checkReturns(index)
	if err != nil {
		return 0, err
	}
	tmpF, ok := me.returnValues[index].(float64)
	if ok == false {
		tmpF2, ok := me.returnValues[index].(float32)
		if ok == false {
			return 0, errors.New("Is not int")
		}
		tmpF = float64(tmpF2)
	}
	tmpV := int64(tmpF)
	if float64(tmpV) == tmpF {
		return tmpV, nil
	} else {
		return 0, errors.New("Is not int")
	}
}

func (me *ECRPCClient) RetAsFloat(index int) (float64, error) {
	err := me.checkReturns(index)
	if err != nil {
		return 0, err
	}
	tmpF, ok := me.returnValues[index].(float64)
	if ok == false {
		tmpF2, ok := me.returnValues[index].(float32)
		if ok == false {
			return 0, errors.New("Is not Float")
		}
		tmpF = float64(tmpF2)
	}
	return tmpF, nil
}

func (me *ECRPCClient) RetAsBool(index int) (bool, error) {
	err := me.checkReturns(index)
	if err != nil {
		return false, err
	}
	tmpV, ok := me.returnValues[index].(bool)
	if ok == false {
		return false, errors.New("Is not Bool")
	}
	return tmpV, nil
}

func (me *ECRPCClient) RetASStructIndex(s interface{}, index int) error {
	err := me.checkReturns(index)
	if err != nil {
		return err
	}
	v := me.returnValues[index]
	tmpM, ok := v.(map[string]interface{})
	if ok == false {
		return errors.New("do not map")
	}
	err = xreflect.ReflectFillStruct(s, tmpM)
	return err
}

func (me *ECRPCClient) RetAsStruct(s interface{}) error {
	err := me.checkReturns(0)
	if err != nil {
		return err
	}
	fields := xreflect.GetStructField(s)
	if len(fields) <= 0 {
		return errors.New("Struct Not Field")
	}
	tmpM := make(map[string]interface{})
	for k, v := range me.returnValues {
		if k < len(fields) {
			tmpM[fields[k]] = v
		}
	}
	err = xreflect.ReflectFillStruct(s, tmpM)
	return err
}

func (me *ECRPCClient) Call2Struct(retStruct interface{}, method string, args ...interface{}) error {
	err := me.Call(method, args...)
	if err != nil {
		return err
	}
	err = me.RetAsStruct(retStruct)
	return err
}

func (me *ECRPCClient) CallHeader(method string, args ...interface{}) ([]interface{}, *HttpData, error) {
	err := me.Call(method, args...)
	result := me.GetResult()
	httpData := new(HttpData)
	httpData.StatusCode = me.httpClient.GetStatusCode()
	httpData.Data = me.httpClient.GetBody()
	httpData.Headers = me.httpClient.GetHeaders()

	return result, httpData, err
}

// ////////////////////////////////////rpc client memory pool///////////////////////////////////////
type ECRPCClientPool struct {
	freeClient  *xlist.Queue
	url         string
	maxPool     int
	timeout     int
	clientCount cast.Int
	secMode     int //SECURITY_HTTP_AES_KIV
	secToken    string
	secKey      string
	authServer  bool
	httpCa      string
	httpCert    string
	httpKey     string
	httpHeader  map[string]string
	reqTimeout  int

	sessionKey string

	FOnSend func(httpClient *gohttpclient.HttpClientEx, data []byte) ([]byte, error)
	FOnData func(httpClient *gohttpclient.HttpClientEx, data []byte) ([]byte, error)
}

func NewECRPCClientPool(url string, max int) *ECRPCClientPool {
	if max <= 0 || max >= 10000 {
		max = 100
	}
	pool := new(ECRPCClientPool)
	pool.maxPool = max
	pool.url = url
	pool.freeClient = xlist.NewQueue(10000)
	pool.timeout = 300
	pool.clientCount.Set(0)
	pool.httpHeader = make(map[string]string)
	return pool
}

func (me *ECRPCClientPool) SetTimeout(v int) {
	me.reqTimeout = v
}

func (me *ECRPCClientPool) GetUrl() string {
	return me.url
}

func (me *ECRPCClientPool) SetHttpHeader(name, value string) {
	me.httpHeader[name] = value
}

func (me *ECRPCClientPool) SetSecurity(mode int, token, key string) {
	me.secMode = mode
	me.secToken = token
	me.secKey = key
}

func (me *ECRPCClientPool) SetSessionKey(key string) {
	me.sessionKey = key
}

func (me *ECRPCClientPool) SetCert(ca, cert, key string, authServer bool) {
	me.httpCa = ca
	me.httpCert = cert
	me.httpKey = key
	me.authServer = authServer
}

func (me *ECRPCClientPool) newClient() *ECRPCClient {
	client := NewECRPCClient(me.url)
	if me.sessionKey != "" {
		client.SetSessionKey(me.sessionKey)
	}
	if me.secMode > 0 {
		if me.secMode == SECURITY_HTTP_AES_KIV {
			client.SetSecurityMode(me.secMode)
		} else {
			client.SetSecurityMode(me.secMode)
			client.SetTokenKey(me.secToken, me.secKey)
		}
	}
	if me.authServer || me.httpCa != "" || me.httpCert != "" {
		err := client.SetCert(me.httpCa, me.httpCert, me.httpKey, me.authServer)
		if err != nil {
			dbg.Dbg("setCert: %s\n", err.Error())
		}
	}
	for k, v := range me.httpHeader {
		client.SetHttpHeader(k, v)
	}
	if me.FOnData != nil {
		client.httpClient.FOnData = me.FOnData
	}
	if me.FOnSend != nil {
		client.httpClient.FOnSend = me.FOnSend
	}
	if me.reqTimeout > 0 {
		client.SetTimeout(me.reqTimeout)
	}
	me.clientCount.Add(1)
	return client
}

func (me *ECRPCClientPool) getClient() *ECRPCClient {
	if me.freeClient.Size() <= 0 && me.clientCount.Get() < int32(me.maxPool) {
		return me.newClient()
	}
	v, err := me.freeClient.Get(float64(me.timeout))
	if err != nil {
		dbg.Dbg("%s\n", err.Error())
		return nil
	}
	client, ok := v.(*ECRPCClient)
	if ok {
		return client
	} else {
		dbg.Dbg("convert rpc client failed\n")
		return nil
	}
	return nil
}

/*
 */
func (me *ECRPCClientPool) Size() (clientCount int, idleCount int) {
	clientCount = me.freeClient.Size()
	idleCount = int(me.clientCount.Get())
	return
}

type HttpData struct {
	StatusCode int
	Headers    common.Header
	Data       []byte
}

func (me *ECRPCClientPool) Call(method string, args ...interface{}) ([]interface{}, error) {
	rpcClient := me.getClient()
	if rpcClient == nil {
		return nil, errors.New("no free rpc client")
	}
	defer func() {
		err := me.freeClient.PutNoWait(rpcClient)
		if err != nil {
			dbg.Dbg("%s\n", err.Error())
		}
	}()
	err := rpcClient.Call(method, args...)
	result := rpcClient.GetResult()
	return result, err
}

func (me *ECRPCClientPool) CallHeader(method string, args ...interface{}) ([]interface{}, *HttpData, error) {
	rpcClient := me.getClient()
	if rpcClient == nil {
		return nil, nil, errors.New("no free rpc client")
	}
	defer func() {
		err := me.freeClient.PutNoWait(rpcClient)
		if err != nil {
			dbg.Dbg("%s\n", err.Error())
		}
	}()
	err := rpcClient.Call(method, args...)
	result := rpcClient.GetResult()
	httpData := new(HttpData)
	httpData.Data = rpcClient.httpClient.GetBody()
	httpData.Headers = rpcClient.httpClient.GetHeaders()
	return result, httpData, err
}

func (me *ECRPCClientPool) Call2(output interface{}, method string, args ...interface{}) error {
	rpcClient := me.getClient()
	if rpcClient == nil {
		return errors.New("no free rpc client")
	}
	defer me.freeClient.PutNoWait(rpcClient)
	return rpcClient.Call2(output, method, args...)
}

func (me *ECRPCClientPool) Call2Header(output interface{}, method string, args ...interface{}) (*HttpData, error) {
	rpcClient := me.getClient()
	if rpcClient == nil {
		return nil, errors.New("No free rpc client")
	}
	defer me.freeClient.PutNoWait(rpcClient)
	err := rpcClient.Call2(output, method, args...)
	httpData := new(HttpData)
	httpData.Data = rpcClient.httpClient.GetBody()
	httpData.Headers = rpcClient.httpClient.GetHeaders()
	return httpData, err
}
