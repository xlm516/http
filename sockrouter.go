package xhttp

import (
	"encoding/json"
	"fmt"
	"gitee.com/xlm516/xtool/dbg"
)

func (me *ECRouterMgr) SockRouter(sockConn *ECSocketConn, data []byte) {
	tmpUrl := ""
	var jsonObj map[string]interface{}
	err := json.Unmarshal(data, &jsonObj)
	if err != nil {
		dbg.Dbg("%s\n", err.Error())
		return
	}
	v, ok := jsonObj["params"]
	if ok == false {
		dbg.Dbg("do not found params\n")
		return
	}
	param, ok := v.([]interface{})
	if ok == false {
		dbg.Dbg("params is not array\n")
		return
	}
	if len(param) < 1 {
		dbg.Dbg("two few argument\n")
		return
	}
	tmpUrl = fmt.Sprintf("%v", param[0])
	router, secondUrl := me.GetRouter("", tmpUrl)
	if router == nil {
		dbg.Dbg("Get Router Failed for %s, bindName=%s\n", tmpUrl, "")
		tmpStr := fmt.Sprintf("Not found %s\n", tmpUrl)
		sockConn.Write([]byte(tmpStr))
		return
	}
	ins := NewECInstance()
	cookiePath := me.GetCookieRouter(tmpUrl)
	ins.httpInData.cookiePath = cookiePath
	ins.httpInData.reqUri = tmpUrl
	ins.httpInData.httpEnv = make(map[string]interface{})
	ins.httpInData.httpBind = nil
	ins.httpInData.httpCtx = nil
	ins.httpInData.SecondUrl = secondUrl
	ins.httpInData.host = sockConn.GetIP()
	ins.connWriter = sockConn

	ins.httpInData.cookieSessionName = GetSessionName(cookiePath, sockConn.sockServer.GetBindPort())
	err = me.doRouter(ins, router)

	if ins.outBuffer.Len() > 0 {
		sockConn.Write(ins.outBuffer.Bytes())
	} else if err != nil {

	}
}
