package httpclient

import (
	"fmt"
	"gitee.com/xlm516/xtool/dbg"
	xfile "gitee.com/xlm516/xtool/file"
	"gitee.com/xlm516/xtool/sys"
	"http/common"
	"http/gohttp/gohttpclient"
	"strings"
)

// HTTPClient  定义http client结构
type HTTPClient struct {
	*gohttpclient.HttpClientEx
	cookieMgr *CookieManager
}

// NewHTTPClient NewHTTPClient
func NewHTTPClient(opt *common.HTTPClientOption) *HTTPClient {
	resp := new(HTTPClient)
	resp.HttpClientEx = gohttpclient.NewHttpClientEx("", false, opt)
	resp.cookieMgr = NewCookieManager()
	return resp
}
func urlToHost(url string) (string, string) {
	uri := ""
	index := strings.Index(url, "://")
	if index > 0 {
		url = url[index+3:]
	}
	index = strings.IndexByte(url, '/')
	if index > 0 {
		uri = url[index:]
		url = url[:index]
	} else {
		uri = "/"
	}
	return url, uri
}

// SetProxy 设置代理服务器
func (me *HTTPClient) SetProxy(proxy string) {
	me.HttpClientEx.SetProxy(proxy)
}

// SetBindIP 设置绑定IP
func (me *HTTPClient) SetBindIP(ip string) {
	me.HttpClientEx.SetBindIP(ip)
}

// SetCookieDir SetCookieDir
func (me *HTTPClient) SetCookieDir(dir string) {
	dir = sys.FormatDir(dir)
	exist, isDir := xfile.FileExist2(dir)
	if exist && !isDir {
		dir = sys.GetFilePath(dir)
	}
	xfile.Mkdir(dir)
	if me.cookieMgr != nil {
		me.cookieMgr.saveCookieDir = dir
	}
	dbg.Dbg("http Set Cookie Dir: %s\n", dir)
}

// Get http get
func (me *HTTPClient) Get(url string) ([]byte, error) {
	host, uri := urlToHost(url)
	if strings.Index(url, "://") <= 0 {
		url = "http://" + url
	}
	cookie := me.cookieMgr.GetByHostURL(host, uri)
	if cookie != nil {
		tmpStr := ""
		for _, v := range cookie.Cookies {
			tmpStr += fmt.Sprintf("%s=%s;", v.Name, v.Value)
		}
		if tmpStr != "" {
			me.SetHttpHeader("Cookie", tmpStr)
			dbg.Dbg("Add Cookie: %s\n", tmpStr)
		}
	}

	b, err := me.HttpClientEx.Get(url)
	if err == nil {
		headers := me.HttpClientEx.GetHeaders()
		items, ok := headers["Set-Cookie"]
		if ok {
			dbg.Dbg("set-cookie: %s\n", items)
			if me.cookieMgr != nil {
				host, _ := urlToHost(url)
				me.cookieMgr.Update(host, items)
			}
		}
	}
	return b, err
}

// Post http post
func (me *HTTPClient) Post(url string, data []byte) ([]byte, error) {
	host, uri := urlToHost(url)
	if strings.Index(url, "://") <= 0 {
		url = "http://" + url
	}
	cookie := me.cookieMgr.GetByHostURL(host, uri)
	if cookie != nil {
		tmpStr := ""
		for _, v := range cookie.Cookies {
			tmpStr += fmt.Sprintf("%s=%s;", v.Name, v.Value)
		}
		if tmpStr != "" {
			me.SetHttpHeader("Cookie", tmpStr)
			dbg.Dbg("Add Cookie: %s\n", tmpStr)
		}
	}
	b, err := me.HttpClientEx.Post(url, data)
	if err == nil {
		headers := me.HttpClientEx.GetHeaders()
		items, ok := headers["Set-Cookie"]
		if ok {
			dbg.Dbg("set-cookie: %s\n", items)
			if me.cookieMgr != nil {
				host, _ := urlToHost(url)
				me.cookieMgr.Update(host, items)
			}
		}
	}
	return b, err
}
