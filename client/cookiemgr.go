package httpclient

import (
	"bytes"
	"fmt"
	"gitee.com/xlm516/xtool/constvar"
	"gitee.com/xlm516/xtool/dbg"
	xfile "gitee.com/xlm516/xtool/file"
	xstring "gitee.com/xlm516/xtool/string"
	"net/http"
	"sort"
	"strings"
	"sync"
)

// CookieHostURL  定义Cookie 绑定主机 + URL
type CookieHostURL struct {
	Host      string
	Path      string
	Cookies   []*http.Cookie
	CookieMap map[string]*http.Cookie
}

// CookieHost  CookieHost
type CookieHost struct {
	HostURL      map[string]*CookieHostURL //map by path
	HostURLSlice []*CookieHostURL
}

// CookieManager  CookieManager
type CookieManager struct {
	cookieHost    map[string]*CookieHost //map by host
	saveCookieDir string
	mutex         sync.Mutex
}

type cookieSlice []*CookieHostURL

func (s cookieSlice) Len() int      { return len(s) }
func (s cookieSlice) Swap(i, j int) { s[i], s[j] = s[j], s[i] }
func (s cookieSlice) Less(i, j int) bool {
	return s[i].Path > s[j].Path
}

// NewCookieManager NewCookieManager
func NewCookieManager() *CookieManager {
	resp := new(CookieManager)
	resp.cookieHost = make(map[string]*CookieHost)
	resp.saveCookieDir = constvar.ApplicationPath + "/cookie"
	return resp
}

// LoadCookieFile LoadCookieFile
func (me *CookieManager) LoadCookieFile(host string) {
	tmpFile := xstring.ConnPath(me.saveCookieDir, fmt.Sprintf("%s.txt", host))
	b, _ := xfile.ReadFile(tmpFile)
	if b != nil {
		items := strings.Split(string(b), "\n")
		me.Update(host, items)
	}
}

// GetByHostURL GetByHostURL
func (me *CookieManager) GetByHostURL(host string, uri string) *CookieHostURL {
	me.mutex.Lock()
	defer me.mutex.Unlock()
	ret, ok := me.cookieHost[host]
	if ok {
		for _, v := range ret.HostURLSlice {
			index := strings.Index(uri, v.Path)
			if index >= 0 {
				return v
			}
		}
	} else if me.saveCookieDir != "" {
		me.LoadCookieFile(host)
		ret, ok := me.cookieHost[host]
		if ok {
			for _, v := range ret.HostURLSlice {
				index := strings.Index(uri, v.Path)
				if index >= 0 {
					return v
				}
			}
		}
	}
	return nil
}

// Update Update
func (me *CookieManager) Update(host string, cookieItem []string) {
	tmpStr := ""
	for _, v := range cookieItem {
		tmpStr += v + "\n"
	}
	retCookie := ParseCookies(cookieItem)
	if len(retCookie) > 0 {
		newHost, ok := me.cookieHost[host]
		if ok == false {
			newHost = new(CookieHost)
			newHost.HostURL = make(map[string]*CookieHostURL)
			newHost.HostURLSlice = make([]*CookieHostURL, 0)
			me.cookieHost[host] = newHost
		}
		for _, v := range retCookie {
			newHostURL, ok := newHost.HostURL[v.Path]
			if ok == false {
				newHostURL = new(CookieHostURL)
				newHostURL.CookieMap = make(map[string]*http.Cookie)
				newHostURL.Cookies = make([]*http.Cookie, 0)
				newHostURL.Host = host
				newHostURL.Path = v.Path
				newHost.HostURL[v.Path] = newHostURL
				newHost.HostURLSlice = append(newHost.HostURLSlice, newHostURL)
			}
			c, ok := newHostURL.CookieMap[v.Name]
			if ok {
				*c = *v
			} else {
				newHostURL.Cookies = append(newHostURL.Cookies, v)
				newHostURL.CookieMap[v.Name] = v
			}
		}
		sort.Sort(cookieSlice(newHost.HostURLSlice))

		var tmpBuf bytes.Buffer
		for _, path := range newHost.HostURLSlice {
			for _, c := range path.CookieMap {
				tmpBuf.WriteString(c.String() + "\n")
			}
		}
		tmpFile := xstring.ConnPath(me.saveCookieDir, fmt.Sprintf("%s.txt", host))
		err := xfile.WriteFile(tmpFile, tmpBuf.Bytes(), 0666)
		if err != nil {
			dbg.Dbg("%s\n", err.Error())
		}
	}
}
