package xhttp

import (
	"bytes"
	"crypto/sha256"
	"encoding/hex"
	"errors"
	"fmt"
	"gitee.com/xlm516/xtool/dbg"
	"gitee.com/xlm516/xtool/encoding"
	"io"
	"reflect"
	"sort"
	"strings"
)

type StringValue []string

func (s StringValue) Len() int           { return len(s) }
func (s StringValue) Swap(i, j int)      { s[i], s[j] = s[j], s[i] }
func (s StringValue) Less(i, j int) bool { return s[i] < s[j] }

func getInterfaceContent(value reflect.Value, name string, writer io.Writer, level int, isHide bool, skip map[string]string,
	mode int) error {
	if value.IsValid() == false {
		return nil
	}
	tmpType := value.Type()
	if tmpType.Kind() == reflect.Ptr {
		tmpType = tmpType.Elem()
		value = value.Elem()
	}
	if tmpType.Kind() == reflect.Ptr {
		return errors.New("not support pointer **")
	}
	if tmpType.Kind() == reflect.Interface {
		value = reflect.ValueOf(value.Interface())
		return getInterfaceContent(value, name, writer, level, isHide, skip, mode)
	}
	switch tmpType.Kind() {
	case reflect.Struct:
		var fieldName StringValue
		var fieldIndex map[string]int

		fieldIndex = make(map[string]int)
		for i := 0; i < tmpType.NumField(); i++ {
			fieldName = append(fieldName, tmpType.Field(i).Name)
			fieldIndex[tmpType.Field(i).Name] = i
		}
		sort.Sort(fieldName)
		for _, v := range fieldName {
			index, ok := fieldIndex[v]
			if ok == false {
				dbg.Dbg("field sort failed\n")
				return errors.New("field sort failed")
			}
			tmpStr := strings.ToLower(v)
			if tmpStr == "sign" || tmpStr == "key" {
				continue
			}
			if skip != nil {
				_, ok := skip[v]
				if ok {
					continue
				}
			}
			field := value.Field(index)
			fName := tmpType.Field(index).Name
			isHide := false
			if fName[0] >= 'a' && fName[0] <= 'z' {
				isHide = true
			}
			tmpName := fName
			if name != "" {
				tmpName = fmt.Sprintf("%s_%s", name, fName)
			}
			err := getInterfaceContent(field, tmpName, writer, level, isHide, skip, mode)
			if err != nil {
				return err
			}
		}

	case reflect.Slice:
		for i := 0; i < value.Len(); i++ {
			tmpName := fmt.Sprintf("%s_%d", name, i)
			err := getInterfaceContent(value.Index(i), tmpName, writer, level, isHide, skip, mode)
			if err != nil {
				return err
			}
		}
	case reflect.Float32, reflect.Float64:
		tmpStr := ""
		if value.CanInterface() == false {
			return errors.New("cannot interface")
		}
		f, ok := value.Interface().(float64)
		if ok {
			tmpStr = fmt.Sprintf("%s=%f", name, f)
		} else {
			f, ok := value.Interface().(float32)
			if ok {
				tmpStr = fmt.Sprintf("%s=%f", name, f)
			}
		}
		tmpLen := len(tmpStr)
		if tmpLen > 0 {
			index := -1
			for i := tmpLen - 1; i > 0; i-- {
				if tmpStr[i] != '0' {
					index = i
					break
				}
			}
			if index > 0 {
				tmpStr = tmpStr[:index]
			}
		}
		writer.Write([]byte(tmpStr + "&"))
	case reflect.Func:
	case reflect.Map:
		if value.CanInterface() == false {
			break
		}
		mapValue, ok := value.Interface().(map[string]interface{})
		if ok == false {
			dbg.Dbg("To Map failed\n")
			break
		}
		var fieldName StringValue
		for k, _ := range mapValue {
			fieldName = append(fieldName, k)
		}
		sort.Sort(fieldName)

		for _, v := range fieldName {
			tmpStr := strings.ToLower(v)
			if tmpStr == "sign" || tmpStr == "key" {
				continue
			}
			field, ok := mapValue[v]
			if ok == false {
				dbg.Dbg("Get Map Field Field by %s\n", v)
			}
			if skip != nil {
				_, ok := skip[v]
				if ok {
					continue
				}
			}
			fName := v
			tmpName := fName
			if name != "" {
				tmpName = fmt.Sprintf("%s_%s", name, fName)
			}
			err := getInterfaceContent(reflect.ValueOf(field), tmpName, writer, level, isHide, skip, mode)
			if err != nil {
				return err
			}
		}
	default:
		//Dbg.Dbg("Type=%s\n", tmpType.Kind())
		tmpValue := fmt.Sprintf("%v", value)
		if tmpValue != "" {
			tmpStr := fmt.Sprintf("%s=%v", name, value)
			writer.Write([]byte(tmpStr + "&"))
		}
	}

	return nil
}

func GetInterfaceContent(inf interface{}, writer io.Writer, skip map[string]string, mode int) error {
	if inf == nil {
		return errors.New("NULL Value")
	}
	return getInterfaceContent(reflect.ValueOf(inf), "", writer, 0, false, skip, mode)
}

func CreateSignMd5(inf interface{}, key string, skip map[string]string, mode int) string {
	buf := new(bytes.Buffer)
	GetInterfaceContent(inf, buf, skip, mode)
	tmpStr := buf.String() + "key=" + key
	dbg.Dbg("sign strings: %s\n", tmpStr)
	return strings.ToUpper(encoding.Md5(tmpStr))
}

func CreateSignSha256(inf interface{}, key string, skip map[string]string, mode int) string {
	hash := sha256.New()
	buf := new(bytes.Buffer)
	GetInterfaceContent(inf, buf, skip, mode)
	tmpStr := buf.String() + "key=" + key
	hash.Write([]byte(tmpStr))
	b := hash.Sum(nil)
	return hex.EncodeToString(b)
}
