package xhttp

import (
	"bytes"
	"encoding/binary"
	"encoding/json"
	"fmt"
	"gitee.com/xlm516/xtool/dbg"
	"gitee.com/xlm516/xtool/encoding"
	"gitee.com/xlm516/xtool/sys"
)

var random *sys.RandEx

func init() {
	random = sys.NewRand()
}

type JsonRPCCryptoHeader struct {
	Index int32
	Nonce string
}

var JsonEncodeMagic []byte = []byte{0x01, 0xFF, 0xAA, 0xEE}

func JsonRPCEncodeCryptoHeader(index int32) []byte {
	outBuf := new(bytes.Buffer)
	binary.Write(outBuf, binary.BigEndian, int32(index))
	binary.Write(outBuf, binary.BigEndian, int32(random.Int(100000)))
	binary.Write(outBuf, binary.BigEndian, int32(random.Int(100000)))
	return outBuf.Bytes()
}

func JsonRPCEncodeHeader(data []byte) *JsonRPCCryptoHeader {
	header := new(JsonRPCCryptoHeader)
	if data == nil || len(data) < 12 {
		return header
	}
	l := len(data)
	headerData := data[l-12:]
	rd := bytes.NewReader(headerData)
	var index int32
	err := binary.Read(rd, binary.BigEndian, &index)
	if err != nil {
		return header
	}
	header.Index = index
	return header
}

func JsonRPCEncodeData(mode int, data []byte, key, nonce string) []byte {
	if mode == SECURITY_NEGO_ZIP_3DES {
		dataHeader := new(JsonRPCCryptoHeader)
		zipData := encoding.Zip(data)
		var b []byte
		var err error
		if key != "" {
			tmpKey := encoding.SHA256(key)
			index := random.Int(100000)
			n := index % 30
			tmpKey = tmpKey[n : n+24]
			dataHeader.Nonce = nonce
			dataHeader.Index = int32(index)
			tmpHeader, _ := json.Marshal(dataHeader)
			for _, v := range tmpHeader {
				zipData = append(zipData, v)
			}
			zipData = append(zipData, uint8(len(tmpHeader)))
			b, err = encoding.TripleDesEncrypt(zipData, []byte(tmpKey))
			header := JsonRPCEncodeCryptoHeader(int32(index))
			if err == nil {
				for _, v := range header {
					b = append(b, v)
				}
			} else {
				dbg.Dbg("%s\n", err.Error())
			}
		} else {
			index := random.Int(100000)
			tmpKey := encoding.SHA256(fmt.Sprintf("%d", index))
			n := index % 30
			tmpKey = tmpKey[n : n+24]

			dataHeader.Nonce = nonce
			dataHeader.Index = int32(0)
			tmpHeader, _ := json.Marshal(dataHeader)
			for _, v := range tmpHeader {
				zipData = append(zipData, v)
			}
			zipData = append(zipData, uint8(len(tmpHeader)))
			b, err = encoding.TripleDesEncrypt(zipData, []byte(tmpKey))

			header := JsonRPCEncodeCryptoHeader(int32(index))
			if err == nil {
				for _, v := range header {
					b = append(b, v)
				}
			} else {
				dbg.Dbg("%s\n", err.Error())
			}
		}
		if err != nil {
			return make([]byte, 0)
		} else {
			for _, v := range JsonEncodeMagic {
				b = append(b, v)
			}
			return b
		}
	}
	return data
}

func JsonRPCDecodeData(mode int, data []byte, key string) ([]byte, *JsonRPCCryptoHeader) {
	encodeHeader := new(JsonRPCCryptoHeader)
	if data == nil || len(data) < 12 {
		return make([]byte, 0), encodeHeader
	}
	if mode == SECURITY_NEGO_ZIP_3DES {
		dataLen := len(data)
		if dataLen < 4 {
			return data, encodeHeader
		}
		orgData := data
		magic := data[dataLen-4:]
		data = data[:dataLen-4]
		if bytes.Compare(magic, JsonEncodeMagic) != 0 {
			return orgData, encodeHeader
		}
		var b []byte
		var err error
		if key != "" {
			header := JsonRPCEncodeHeader(data)
			tmpKey := encoding.SHA256(key)
			n := header.Index % 30
			tmpKey = tmpKey[n : n+24]
			l := len(data)
			data = data[:l-12]
			b, err = encoding.TripleDesDecrypt(data, []byte(tmpKey))
		} else {
			header := JsonRPCEncodeHeader(data)
			index := header.Index
			if index < 0 {
				index = 0
			}
			tmpKey := encoding.SHA256(fmt.Sprintf("%d", index))
			n := index % 30
			tmpKey = tmpKey[n : n+24]

			l := len(data)
			data = data[:l-12]
			b, err = encoding.TripleDesDecrypt(data, []byte(tmpKey))
			if err != nil {
				return make([]byte, 0), encodeHeader
			}
		}
		if err != nil {
			dbg.Dbg("%s\n", err.Error())
		}
		tmpLen := len(b)
		if tmpLen > 0 {
			headerLen := b[tmpLen-1]
			headerData := b[tmpLen-int(headerLen)-1 : tmpLen-1]
			b = b[:tmpLen-int(headerLen)-1]
			json.Unmarshal(headerData, encodeHeader)
		}
		var buffer bytes.Buffer
		buffer.Write(b)
		unzipData, err := encoding.UnzipByte(&buffer)
		if err != nil {
			dbg.Dbg("unzip failed: %s\n", err.Error())
			return make([]byte, 0), encodeHeader
		}
		return unzipData, encodeHeader
	}
	return data, encodeHeader
}
