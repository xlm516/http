package xhttp

import (
	"bytes"
	"encoding/binary"
	"encoding/json"
	"errors"
	"fmt"
	"gitee.com/xlm516/xtool/dbg"
	"gitee.com/xlm516/xtool/encoding"
	xid "gitee.com/xlm516/xtool/id"
	"gitee.com/xlm516/xtool/klv"
	xnet "gitee.com/xlm516/xtool/net"
	"strings"
	"sync"
	"time"
)

const (
	CLIENT_ID_LEN = 32
)
const (
	MSG_TYPE_REQUEST      = 1
	MSG_TYPE_RESPONSE     = 2
	MSG_TYPE_KEEPALIVE    = 3
	MSG_TYPE_KLV_REQUEST  = 4
	MSG_TYPE_KLV_RESPONSE = 5
)

const (
	KLV_HTTP_CODE         = 1
	KLV_HTTP_URL          = 2
	KLV_HTTP_HEADER       = 3
	KLV_HTTP_CONTENT_TYPE = 4
	KLV_HTTP_FORM         = 5
	KLV_HTTP_DATA         = 6
	KLV_HTTP_METHOD       = 7
)

type ECSockMsgHeader struct {
	MsgLen   int32
	MsgType  int32
	ClientId []byte //32 byte
}

type ECSockRPCConn struct {
	*xnet.MySrvClient
	clientId    string
	header      *ECSockMsgHeader
	sockServer  *ECSockRPCServer
	msgType     int
	reqChMutex  sync.Mutex
	clientMutex sync.Mutex
	reqCh       chan []byte
	createTime  int64
	updateTime  int64
	encode      bool
}

type ECSockConnInfo struct {
	ClientId   string
	Host       string
	CreateTime string
	UpdateTime string
}

type ECSockRPCServer struct {
	*xnet.MyServer
	rpcServer    *ECRPCServer
	OnKlvRequest func(conn *ECSockRPCConn, klvData *klv.KLV) *klv.KLV
	bindHost     string
	encode       bool
	bExit        bool
}

func NewECSockRPCServer(bindHost string) *ECSockRPCServer {
	srv := new(ECSockRPCServer)
	srv.bindHost = bindHost
	srv.init()
	return srv
}

func (me *ECSockRPCConn) GetRequestChannel() chan []byte {
	me.reqChMutex.Lock()
	defer me.reqChMutex.Unlock()
	return me.reqCh
}
func (me *ECSockRPCConn) SetRequestChannel(c chan []byte) {
	me.reqChMutex.Lock()
	defer me.reqChMutex.Unlock()
	me.reqCh = c
}
func (me *ECSockRPCConn) Write(b []byte) (int, error) {
	if me.encode {
		nonce := xid.NewShortKey()
		b2, err := encoding.AesEncodeByKIV(b, nonce, nil, nil, false)
		if err != nil {
			dbg.Dbg("%s\n", err.Error())
			return 0, err
		}
		b = b2
	}

	outBuf := new(bytes.Buffer)
	dataLen := len(b)
	_ = binary.Write(outBuf, binary.BigEndian, int32(dataLen))
	_ = binary.Write(outBuf, binary.BigEndian, int32(me.msgType))
	idBuf := make([]byte, CLIENT_ID_LEN)
	if len(me.clientId) > CLIENT_ID_LEN {
		me.clientId = me.clientId[:CLIENT_ID_LEN]
	}
	copy(idBuf, []byte(me.clientId))
	outBuf.Write(idBuf)
	outBuf.Write(b)

	data := outBuf.Bytes()
	v := me.Send(data, false)
	if v {
		return len(data), nil
	} else {
		return 0, errors.New("writer failed")
	}
}

// /////////////////////////////////////////////////////////////////
func (me *ECSockRPCServer) init() {
	me.rpcServer = NewECRPCServer()
}
func (me *ECSockRPCServer) SetEncode(v bool) {
	me.encode = v
}
func (me *ECSockRPCServer) ParseHeader(b []byte) (int, interface{}) {
	var msgLen int32
	var msgType int32
	msgHeader := &ECSockMsgHeader{}
	rd := bytes.NewReader(b)
	err := binary.Read(rd, binary.BigEndian, &msgLen)
	if err != nil {
		fmt.Println(err)
		return 0, msgHeader
	}

	err = binary.Read(rd, binary.BigEndian, &msgType)
	if err != nil {
		fmt.Println(err)
		return 0, msgHeader
	}

	msgHeader.MsgLen = msgLen
	msgHeader.MsgType = msgType
	msgHeader.ClientId = make([]byte, len(b)-8)
	copy(msgHeader.ClientId, b[8:])
	dataLen := msgLen

	return int(dataLen), msgHeader
}

// ////////////////////////////////////imply interface////////////////////////
func (me *ECSockRPCServer) OnAccept(client *xnet.MySrvClient) bool {
	c := &ECSockRPCConn{}
	c.MySrvClient = client
	c.sockServer = me
	c.encode = me.encode
	client.SetUserData(c)
	if client.GetConn() != nil {
		client.GetConn().SetHeaderLen(8 + CLIENT_ID_LEN)
		client.GetConn().SetIf(me)
		client.IsLogin.Set(true)
	}
	return true
}
func (me *ECSockRPCServer) OnClose(client *xnet.MySrvClient) {
	clientConn := me.GetECSocketConn(client)
	if clientConn != nil {
		reqCh := clientConn.GetRequestChannel()
		if reqCh != nil {
			close(reqCh)
		}
	}
	client.SetUserData(nil)
}

func (me *ECSockRPCServer) OnRequest(client *xnet.MySrvClient, data []byte) bool {
	conn := client.GetConn()
	if conn == nil {
		return false
	}
	h := conn.GetHeaderMsg()
	if h == nil {
		return false
	}
	msgHeader, ok := h.(*ECSockMsgHeader)
	if ok == false {
		dbg.Dbg("get msgHeader Failed\n")
		return false
	}
	clientConn := me.GetECSocketConn(client)
	if clientConn == nil {
		dbg.Dbg("client connection is null\n")
		return false
	}
	if me.encode {
		b, _, err := encoding.AesDecodeByKIV(data, nil, nil, false)
		if err != nil {
			dbg.Dbg("%s\n", err.Error())
			return false
		}
		data = b
	}
	clientConn.header = msgHeader
	clientConn.clientId = strings.Trim(string(msgHeader.ClientId), "\000")
	ins := NewECInstance()
	//ins.httpInData.httpEnv = make(map[string]interface{})
	jsonObj := make(map[string]interface{})
	if msgHeader.MsgType != MSG_TYPE_KLV_RESPONSE && msgHeader.MsgType != MSG_TYPE_KLV_REQUEST {
		err := json.Unmarshal(data, &jsonObj)
		if err != nil {
			dbg.Dbg("%s\n", err.Error())
			dbg.Dbg("%s, msgType=%d\n", string(data), msgHeader.MsgType)
			return false
		}
	}
	ins.SetConnWriter(clientConn)
	if msgHeader.MsgType == MSG_TYPE_REQUEST {
		clientConn.msgType = MSG_TYPE_RESPONSE
		_, err := me.rpcServer.Call(ins, nil, jsonObj)
		if err != nil {
			dbg.Dbg("%s,Data: %+v\n", err.Error(), jsonObj)
			return false
		}
		buf := ins.GetOutBuffer()
		if len(buf) > 0 {
			_, _ = clientConn.Write(buf)
		}
	} else if msgHeader.MsgType == MSG_TYPE_RESPONSE || msgHeader.MsgType == MSG_TYPE_KLV_RESPONSE {
		d := make([]byte, len(data))
		copy(d, data)
		reqCh := clientConn.GetRequestChannel()
		if reqCh != nil {
			reqCh <- d
			close(reqCh)
			clientConn.SetRequestChannel(nil)
		}
	} else if msgHeader.MsgType == MSG_TYPE_KLV_REQUEST {
		if me.OnKlvRequest != nil {
			clientConn.msgType = MSG_TYPE_KLV_RESPONSE
			kv := klv.NewKLV()
			err := kv.Parse(data)
			if err != nil {
				dbg.Dbg("%s\n", err.Error())
			} else {
				tmpKlv := me.OnKlvRequest(clientConn, kv)
				if tmpKlv != nil {
					buf := new(bytes.Buffer)
					tmpKlv.Export(buf)
					_, _ = clientConn.Write(buf.Bytes())
				}
			}
		}
	} else if msgHeader.MsgType == MSG_TYPE_KEEPALIVE {
		clientConn.msgType = MSG_TYPE_KEEPALIVE
		data := make(map[string]interface{})
		data["Data"] = "hello"
		b, _ := json.Marshal(data)
		_, _ = clientConn.Write(b)
	}
	return true
}

func (me *ECSockRPCServer) Start() (bool, error) {
	me.MyServer = xnet.NewMyServer(me.bindHost)
	if me.MyServer != nil {
		me.ServerName = "ECSocketServer"
		me.MyServer.SetInterface(me)
		return me.MyServer.Start()
	}

	return false, nil
}

func (me *ECSockRPCServer) GetECSocketConn(client *xnet.MySrvClient) *ECSockRPCConn {
	d := client.GetUserData()
	if d == nil {
		return nil
	}
	conn, ok := d.(*ECSockRPCConn)
	if ok {
		return conn
	} else {
		return nil
	}
}

func (me *ECSockRPCServer) GetECSocketConnInfo() []*ECSockConnInfo {
	resp := make([]*ECSockConnInfo, 0)
	clients := me.GetClient()

	clients.Range(func(key int64, client *xnet.MySrvClient) bool {
		d := client.GetUserData()
		if d == nil {
			return true
		}
		conn, ok := d.(*ECSockRPCConn)
		if ok {
			item := new(ECSockConnInfo)
			item.ClientId = conn.clientId
			item.Host = client.GetRemoteAddressString()
			resp = append(resp, item)
		}
		return true
	})
	return resp
}

func (me *ECSockRPCServer) GetECSocketConnByClientId(clientId string) *ECSockRPCConn {
	clients := me.GetClient()
	var retConn *ECSockRPCConn
	clients.Range(func(key int64, client *xnet.MySrvClient) bool {
		d := client.GetUserData()
		if d == nil {
			return true
		}
		conn, ok := d.(*ECSockRPCConn)
		if ok {
			if conn.clientId == clientId && conn.reqCh == nil {
				retConn = conn
				return false
			}
		}
		return true
	})
	if retConn != nil {
		return retConn
	}
	return nil
}

func (me *ECSockRPCServer) AddFunc(name string, fun interface{}, level int) error {
	return me.rpcServer.AddFunc(name, fun, level)
}

func (me *ECSockRPCServer) Call(clientId string, method string, args ...interface{}) ([]interface{}, error) {
	return me._Call(clientId, nil, method, args...)
}
func (me *ECSockRPCServer) Call2(clientId string, output interface{}, method string, args ...interface{}) error {
	_, err := me._Call(clientId, output, method, args...)
	return err
}

func (me *ECSockRPCServer) _Call(clientId string, output interface{}, method string, args ...interface{}) ([]interface{}, error) {
	client := me.GetECSocketConnByClientId(clientId)
	if client == nil {
		return nil, errors.New("not found Client")
	}
	client.clientMutex.Lock()
	defer client.clientMutex.Unlock()
	reqCh := client.GetRequestChannel()
	if reqCh != nil {
		return nil, errors.New("requesting now")
	}
	rpcClient := NewECRPCClient("")
	reqData, err := rpcClient.BuildRequestData(method, args...)
	if err != nil {
		return nil, err
	}
	reqCh = make(chan []byte, 1)
	client.SetRequestChannel(reqCh)
	client.msgType = MSG_TYPE_REQUEST
	_, err = client.Write(reqData)
	if err != nil {
		close(reqCh)
		client.SetRequestChannel(reqCh)
		return nil, err
	}

	var retData []byte
	select {
	case retData = <-reqCh:
		break
	case <-time.After(time.Second * time.Duration(30)):
		dbg.Dbg("Do Request fatal error\n")
		client.ForceClose()
		return nil, errors.New("request timeout")
	}
	if retData != nil {
		rpcClient.ProcessJson(output, retData)
		return rpcClient.GetResult(), nil
	} else {
		return nil, errors.New("invalid response data")
	}
}

func (me *ECSockRPCServer) CallKLV(clientId string, klvData *klv.KLV, t int) (*klv.KLV, error) {
	client := me.GetECSocketConnByClientId(clientId)
	if client == nil {
		return nil, errors.New("not found Client")
	}
	client.clientMutex.Lock()
	defer client.clientMutex.Unlock()
	reqCh := client.GetRequestChannel()
	if reqCh != nil {
		return nil, errors.New("requesting now")
	}
	buf := new(bytes.Buffer)
	klvData.Export(buf)
	reqCh = make(chan []byte, 1)
	client.SetRequestChannel(reqCh)
	client.msgType = MSG_TYPE_KLV_REQUEST
	_, err := client.Write(buf.Bytes())
	if err != nil {
		close(reqCh)
		client.SetRequestChannel(nil)
		return nil, err
	}
	if t <= 0 {
		t = 10
	}
	var retData []byte
	select {
	case retData = <-reqCh:
		break
	case <-time.After(time.Second * time.Duration(t)):
		dbg.Dbg("do request fatal error\n")
		client.ForceClose()
		return nil, errors.New("request timeout")
	}
	if retData != nil {
		tmpKlv := klv.NewKLV()
		err := tmpKlv.Parse(retData)
		if err != nil {
			dbg.Dbg("%s\n", err.Error())
			return nil, err
		}
		return tmpKlv, nil
	} else {
		return nil, errors.New("invalid response data")
	}
}
