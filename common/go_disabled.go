//go:build !GOHTTP_SERVER
// +build !GOHTTP_SERVER

package common

import (
	"errors"
)

var GoHttpCtxNull error = errors.New("GoHttp Ctx null")

func (me *ECHttpCookie) GetGoName() string {
	return ""
}

func (me *ECHttpCookie) GetGoPath() string {
	return ""
}

func (me *ECHttpCookie) GetGoValue() string {
	return ""
}

// ///////////////////////////////////////////////////////////////////////////////////////////
type ECGoHttpRequest struct {
}

func (me *ECGoHttpRequest) CloseBody() error {
	return nil
}

func (me *ECGoHttpRequest) ParseForm() error {
	return nil
}

func (me *ECGoHttpRequest) RemoteAddress() string {
	return ""
}

func (me *ECGoHttpRequest) RemoteIPAddress() string {
	return ""
}

func (me *ECGoHttpRequest) RequestURI() string {
	return ""
}

func (me *ECGoHttpRequest) UrlPath() string {
	return ""
}

func (me *ECGoHttpRequest) Close() error {
	return nil
}
func (me *ECGoHttpRequest) GetCookie(name string) *ECCookieInfo {
	return nil
}

func (me *ECGoHttpRequest) GetHttpHeaderAll() []*ECHttpHeader {
	resp := make([]*ECHttpHeader, 0)
	return resp
}

func (me *ECGoHttpRequest) GetHttpHeader(name string) []string {
	return nil
}

func (me *ECGoHttpRequest) Host() string {
	return ""
}

func (me *ECGoHttpRequest) Method() string {
	return ""
}

func (me *ECGoHttpRequest) UserAgent() string {
	return ""
}

func (me *ECGoHttpRequest) GetHttpHeaderOne(name string, isSet *bool) string {
	return ""
}

func (me *ECGoHttpRequest) ReadBody(maxSize int64) ([]byte, error) {
	return nil, errors.New("not support")
}
func (me *ECGoHttpRequest) ParseMultipartForm(maxSize int64) error {
	return nil
}
func (me *ECGoHttpRequest) SetForm(name, value string) {
}
func (me *ECGoHttpRequest) Form(name string) (string, bool) {
	return "", false
}
func (me *ECGoHttpRequest) GetForms() (map[string][]string, error) {
	resp := make(map[string][]string)
	return resp, nil
}

func (me *ECGoHttpRequest) ModifyUrlPath(path string) {
}

func (me *ECGoHttpRequest) GetFormFileByName(name string) (*ECFormFileInfo, error) {
	return nil, errors.New("not support")
}

func (me *ECGoHttpRequest) GetFormFiles() ([]*ECFormFileInfo, error) {
	resp := make([]*ECFormFileInfo, 0)
	return resp, nil
}
func (me *ECGoHttpRequest) GetFormFileContext(name string) (*ECFormFileInfo, []byte, error) {
	return nil, nil, errors.New("not support")
}

// //////////////////////////////////////////////////////////////////////////////////////////
type ECGoHttpResponse struct {
}

func (me *ECGoHttpResponse) WriteString(str string) error {
	return nil
}

func (me *ECGoHttpResponse) Write(data []byte) error {
	return nil
}

func (me *ECGoHttpResponse) AddHttpHeader(name, value string) error {
	return nil
}

func (me *ECGoHttpResponse) SetHttpHeader(name, value string) error {
	return nil
}

func (me *ECGoHttpResponse) DelHttpHeader(name string) error {
	return nil
}

func (me *ECGoHttpResponse) GetHttpHeader(name string) string {
	return ""
}

func (me *ECGoHttpResponse) SetHttpCode(code int) error {
	return nil
}
func (me *ECGoHttpResponse) Redirect(url string, code int) error {
	return nil
}
func (me *ECGoHttpResponse) SetCookie(cookie *ECCookieInfo) error {
	return nil
}
