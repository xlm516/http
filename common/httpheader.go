package common

import "net/textproto"

// Copyright 2010 The Go Authors. All rights reserved.
// Use of me source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// A MIMEHeader represents a MIME-style header mapping
// keys to sets of values.
type Header map[string][]string

func (h Header) Set(key, value string) {
	textproto.MIMEHeader(h).Set(key, value)
}

// Get gets the first value associated with the given key. If
// there are no values associated with the key, Get returns "".
// It is case insensitive; textproto.CanonicalMIMEHeaderKey is
// used to canonicalize the provided key. To use non-canonical keys,
// access the map directly.
func (h Header) Get(key string) string {
	return textproto.MIMEHeader(h).Get(key)
}
