//go:build !FASTHTTP_SERVER
// +build !FASTHTTP_SERVER

package common

import (
	"errors"
)

var FastHttpCtxNull error = errors.New("FastHttp Ctx null")

func (me *ECHttpCookie) GetFastName() string {
	return ""
}

func (me *ECHttpCookie) GetFastPath() string {
	return ""
}

func (me *ECHttpCookie) GetFastValue() string {
	return ""
}

// /////////////////////////////////////////////////////////////////////////////////
type ECFastHttpRequest struct {
}

func (me *ECFastHttpRequest) CloseBody() error {
	return nil
}
func (me *ECFastHttpRequest) ParseForm() error {
	return errors.New("not support")
}
func (me *ECFastHttpRequest) RemoteAddress() string {
	return ""
}

func (me *ECFastHttpRequest) RemoteIPAddress() string {
	return ""
}

func (me *ECFastHttpRequest) RequestURI() string {
	return ""
}

func (me *ECFastHttpRequest) UrlPath() string {
	return ""
}

func (me *ECFastHttpRequest) Close() error {
	return nil
}
func (me *ECFastHttpRequest) GetCookie(name string) *ECCookieInfo {
	return nil
}

func (me *ECFastHttpRequest) GetHttpHeaderAll() []*ECHttpHeader {
	resp := make([]*ECHttpHeader, 0)
	return resp
}

func (me *ECFastHttpRequest) GetHttpHeader(name string) []string {
	resp := make([]string, 0)
	return resp
}

func (me *ECFastHttpRequest) Host() string {
	return ""
}

func (me *ECFastHttpRequest) Method() string {
	return ""
}

func (me *ECFastHttpRequest) UserAgent() string {
	return ""
}

func (me *ECFastHttpRequest) GetHttpHeaderOne(name string, isSet *bool) string {
	return ""
}

func (me *ECFastHttpRequest) ReadBody(maxSize int64) ([]byte, error) {
	return nil, errors.New("not support")
}
func (me *ECFastHttpRequest) ParseMultipartForm(maxSize int64) error {
	return errors.New("not support")
}
func (me *ECFastHttpRequest) SetForm(name, value string) {
}
func (me *ECFastHttpRequest) Form(name string) (string, bool) {
	return "", false
}
func (me *ECFastHttpRequest) GetForms() (map[string][]string, error) {
	resp := make(map[string][]string)
	return resp, nil
}

func (me *ECFastHttpRequest) ModifyUrlPath(path string) {
}

func (me *ECFastHttpRequest) GetFormFileByName(name string) (*ECFormFileInfo, error) {
	return nil, errors.New("not support")
}

func (me *ECFastHttpRequest) GetFormFiles() ([]*ECFormFileInfo, error) {
	return nil, errors.New("not support")
}
func (me *ECFastHttpRequest) GetFormFileContext(name string) (*ECFormFileInfo, []byte, error) {
	return nil, nil, errors.New("not support")
}

// /////////////////////////////////////////////////////////////////////////
type ECFastHttpResponse struct {
}

func (me *ECFastHttpResponse) WriteString(str string) error {
	return nil
}

func (me *ECFastHttpResponse) Write(data []byte) error {
	return nil
}

func (me *ECFastHttpResponse) AddHttpHeader(name, value string) error {
	return nil
}

func (me *ECFastHttpResponse) SetHttpHeader(name, value string) error {
	return nil
}

func (me *ECFastHttpResponse) DelHttpHeader(name string) error {
	return nil
}

func (me *ECFastHttpResponse) GetHttpHeader(name string) string {
	return ""
}

func (me *ECFastHttpResponse) SetHttpCode(code int) error {
	return nil
}
func (me *ECFastHttpResponse) Redirect(url string, code int) error {
	return nil
}
func (me *ECFastHttpResponse) SetCookie(cookie *ECCookieInfo) error {
	return nil
}
