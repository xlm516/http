//go:build GOHTTP_SERVER
// +build GOHTTP_SERVER

package common

import (
	"bytes"
	"encoding/json"
	"errors"
	"net/http"
	"strings"
)

var GoHttpCtxNull error = errors.New("GoHttp Ctx null")

func (me *ECHttpContext) ToGoHttpHandle() (http.ResponseWriter, *http.Request, error) {
	v, ok := me.Request.(*ECGoHttpRequest)
	if ok {
		return v.w, v.r, nil
	} else {
		return nil, nil, errors.New("is not gohttp ctx")
	}
}

func (me *ECHttpCookie) GetGoName() string {
	v, ok := me.Cookie.(*http.Cookie)
	if ok {
		return v.Name
	} else {
		return ""
	}
}

func (me *ECHttpCookie) GetGoPath() string {
	v, ok := me.Cookie.(*http.Cookie)
	if ok {
		return v.Path
	} else {
		return ""
	}
}

func (me *ECHttpCookie) GetGoValue() string {
	v, ok := me.Cookie.(*http.Cookie)
	if ok {
		return v.Value
	} else {
		return ""
	}
}

// ///////////////////////////////////////////////////////////////////////////////////////////
type ECGoHttpRequest struct {
	w http.ResponseWriter
	r *http.Request
}

func NewECGoHttpRequest(w http.ResponseWriter, r *http.Request) *ECGoHttpRequest {
	resp := new(ECGoHttpRequest)
	resp.w = w
	resp.r = r
	return resp
}

func (me *ECGoHttpRequest) CloseBody() error {
	if me.r == nil {
		return GoHttpCtxNull
	}
	me.r.Body.Close()
	return nil
}

func (me *ECGoHttpRequest) ParseForm() error {
	if me.r == nil {
		return GoHttpCtxNull
	}
	me.r.ParseForm()
	return nil
}

func (me *ECGoHttpRequest) RemoteAddress() string {
	if me.r == nil {
		return ""
	}
	return me.r.RemoteAddr
}

func (me *ECGoHttpRequest) RemoteIPAddress() string {
	if me.r == nil {
		return ""
	}
	addr := me.r.RemoteAddr
	index := strings.IndexByte(addr, ':')
	if index > 0 {
		return addr[:index]
	} else {
		return addr
	}
}

func (me *ECGoHttpRequest) RequestURI() string {
	if me.r == nil {
		return ""
	}
	return me.r.RequestURI
}

func (me *ECGoHttpRequest) UrlPath() string {
	if me.r == nil {
		return ""
	}
	return me.r.URL.Path
}

func (me *ECGoHttpRequest) Close() error {
	if me.r == nil {
		return GoHttpCtxNull
	}
	me.r.Close = true
	return nil
}

func (me *ECGoHttpRequest) GetCookie(name string) *ECCookieInfo {
	resp := new(ECCookieInfo)
	if me.r == nil {
		return nil
	}
	v, err := me.r.Cookie(name)
	if err != nil {
		return nil
	}
	resp.Name = v.Name
	resp.Value = v.Value
	resp.Path = v.Path
	return resp
}

func (me *ECGoHttpRequest) GetCookieSlice(name string) []*ECCookieInfo {
	resp := make([]*ECCookieInfo, 0)
	if me.r == nil {
		return nil
	}
	name = strings.ToLower(name)
	for _, v := range me.r.Cookies() {
		if strings.ToLower(v.Name) == name {
			item := new(ECCookieInfo)
			item.Name = v.Name
			item.Value = v.Value
			item.Path = v.Path
			resp = append(resp, item)
		}
	}
	return resp
}

func (me *ECGoHttpRequest) GetHttpHeaderAll() []*ECHttpHeader {
	resp := make([]*ECHttpHeader, 0)
	if me.r == nil {
		return nil
	}
	for k, v := range me.r.Header {
		item := new(ECHttpHeader)
		item.Name = k
		item.Value = make([]string, 0)
		item.Value = append(item.Value, v...)
		resp = append(resp, item)
	}
	return resp
}

func (me *ECGoHttpRequest) GetHttpHeader(name string) []string {
	resp := make([]string, 0)
	if me.r == nil {
		return nil
	}
	v, ok := me.r.Header[name]
	if ok {
		resp = append(resp, v...)
		return resp
	} else {
		return nil
	}
}

func (me *ECGoHttpRequest) Host() string {
	if me.r == nil {
		return ""
	}
	return me.r.Host
}

func (me *ECGoHttpRequest) Method() string {
	if me.r == nil {
		return ""
	}
	return me.r.Method
}

func (me *ECGoHttpRequest) UserAgent() string {
	if me.r == nil {
		return ""
	}
	return me.r.UserAgent()
}

func (me *ECGoHttpRequest) GetHttpHeaderOne(name string, isSet *bool) string {
	v := me.GetHttpHeader(name)
	if v != nil && len(v) > 0 {
		if isSet != nil {
			*isSet = true
		}
		return v[0]
	} else {
		if isSet != nil {
			*isSet = false
		}
		return ""
	}
}

func (me *ECGoHttpRequest) ReadBody(maxSize int64) ([]byte, error) {
	if me.r == nil {
		return nil, GoHttpCtxNull
	}
	r := me.r.Body
	if r == nil {
		return nil, errors.New("body is null")
	}
	buffer := new(bytes.Buffer)
	tmpBuf := make([]byte, 4096)
	defer r.Close()
	for {
		n, err := r.Read(tmpBuf)
		if n > 0 {
			buffer.Write(tmpBuf[:n])
		}
		if err != nil {
			break
		}
	}
	return buffer.Bytes(), nil
}
func (me *ECGoHttpRequest) ParseMultipartForm(maxSize int64) error {
	if me.r == nil {
		return GoHttpCtxNull
	}
	err := me.r.ParseMultipartForm(maxSize)
	return err
}
func (me *ECGoHttpRequest) SetForm(name, value string) {
	if me.r == nil {
		return
	}
	me.r.Form.Add(name, value)
}
func (me *ECGoHttpRequest) Form(name string) (string, bool) {
	if me.r == nil {
		return "", false
	}
	_ = me.r.FormValue(name)
	v, ok := me.r.Form[name]
	if ok == false {
		return "", false
	}
	if len(v) == 0 {
		return "", ok
	}
	if len(v) == 1 {
		return v[0], ok
	} else {
		b, _ := json.Marshal(v)
		return "```" + string(b), ok
	}
}
func (me *ECGoHttpRequest) GetForms() (map[string][]string, error) {
	resp := make(map[string][]string)
	if me.r == nil {
		return resp, GoHttpCtxNull
	}
	for k, v := range me.r.Form {
		if len(v) > 0 {
			resp[k] = v
		}
	}
	return resp, nil
}

func (me *ECGoHttpRequest) ModifyUrlPath(path string) {
	if me.r == nil {
		return
	}
	me.r.URL.Path = path
}

func (me *ECGoHttpRequest) GetFormFileByName(name string) (*ECFormFileInfo, error) {
	if me.r == nil {
		return nil, GoHttpCtxNull
	}
	if name == "" {
		files, err := me.GetFormFiles()
		if err != nil {
			return nil, err
		}
		if len(files) > 0 {
			return files[0], nil
		} else {
			return nil, errors.New("not form file")
		}
	}
	_, formFile, err := me.r.FormFile(name)
	if err != nil {
		return nil, err
	}
	resp := new(ECFormFileInfo)
	resp.Name = name
	resp.Filename = formFile.Filename
	resp.Size = formFile.Size
	return resp, nil
}

func (me *ECGoHttpRequest) GetFormFiles() ([]*ECFormFileInfo, error) {
	if me.r == nil {
		return nil, GoHttpCtxNull
	}
	if me.r.MultipartForm == nil {
		return nil, errors.New("no form file")
	}
	resp := make([]*ECFormFileInfo, 0)
	for k, v := range me.r.MultipartForm.File {
		for _, f := range v {
			item := new(ECFormFileInfo)
			item.Name = k
			item.Filename = f.Filename
			item.Size = f.Size
			item.Header = f.Header
			resp = append(resp, item)
		}
	}
	return resp, nil
}
func (me *ECGoHttpRequest) GetFormFileContext(name string) (*ECFormFileInfo, []byte, error) {
	fileInfo, err := me.GetFormFileByName(name)
	if err != nil {
		return fileInfo, nil, err
	}
	if name == "" {
		name = fileInfo.Name
	}
	mutiData, _, err := me.r.FormFile(name)
	if err != nil {
		return fileInfo, nil, err
	}
	data := new(bytes.Buffer)
	tmpBuf := make([]byte, 4096)
	for {
		n, err := mutiData.Read(tmpBuf)
		if n > 0 {
			data.Write(tmpBuf)
		}
		if err != nil {
			break
		}
	}
	return fileInfo, data.Bytes(), err
}

// //////////////////////////////////////////////////////////////////////////////////////////
type ECGoHttpResponse struct {
	w http.ResponseWriter
	r *http.Request
}

func NewECGoHttpResponse(w http.ResponseWriter, r *http.Request) *ECGoHttpResponse {
	resp := new(ECGoHttpResponse)
	resp.w = w
	resp.r = r
	return resp
}

func (me *ECGoHttpResponse) WriteString(str string) error {
	if me.w == nil {
		return GoHttpCtxNull
	}
	_, err := me.w.Write([]byte(str))
	return err
}

func (me *ECGoHttpResponse) Write(data []byte) error {
	if me.w == nil {
		return GoHttpCtxNull
	}
	_, err := me.w.Write(data)
	return err
}

func (me *ECGoHttpResponse) AddHttpHeader(name, value string) error {
	if me.w == nil {
		return GoHttpCtxNull
	}
	me.w.Header().Add(name, value)
	return nil
}

func (me *ECGoHttpResponse) SetHttpHeader(name, value string) error {
	if me.w == nil {
		return GoHttpCtxNull
	}
	me.w.Header().Del(name)
	me.w.Header().Set(name, value)
	return nil
}

func (me *ECGoHttpResponse) DelHttpHeader(name string) error {
	if me.w == nil {
		return GoHttpCtxNull
	}
	me.w.Header().Del(name)
	return nil
}

func (me *ECGoHttpResponse) GetHttpHeader(name string) string {
	if me.w == nil {
		return ""
	}
	return me.w.Header().Get(name)
}

func (me *ECGoHttpResponse) SetHttpCode(code int) error {
	if me.w == nil {
		return GoHttpCtxNull
	}
	me.w.WriteHeader(code)
	return nil
}
func (me *ECGoHttpResponse) Redirect(url string, code int) error {
	if me.w == nil {
		return GoHttpCtxNull
	}
	http.Redirect(me.w, me.r, url, code)
	return nil
}
func (me *ECGoHttpResponse) SetCookie(cookie *ECCookieInfo) error {
	if me.w == nil {
		return GoHttpCtxNull
	}
	c := new(http.Cookie)
	c.Name = cookie.Name
	c.Path = cookie.Path
	c.Value = cookie.Value
	http.SetCookie(me.w, c)
	return nil
}
