//go:build FASTHTTP_SERVER
// +build FASTHTTP_SERVER

package common

import (
	"bytes"
	"errors"
	"github.com/valyala/fasthttp"
	"strings"
)

var FastHttpCtxNull error = errors.New("FastHttp Ctx null")

func (me *ECHttpContext) ToFastHttpHandle() (*fasthttp.RequestCtx, error) {
	v, ok := me.Request.(*ECFastHttpRequest)
	if ok {
		return v.ctx, nil
	} else {
		return nil, errors.New("is not fastcgi ctx")
	}
}

func (me *ECHttpCookie) GetFastName() string {
	v, ok := me.Cookie.(*fasthttp.Cookie)
	if ok {
		return string(v.Key())
	} else {
		return ""
	}
}

func (me *ECHttpCookie) GetFastPath() string {
	v, ok := me.Cookie.(*fasthttp.Cookie)
	if ok {
		return string(v.Path())
	} else {
		return ""
	}
}

func (me *ECHttpCookie) GetFastValue() string {
	v, ok := me.Cookie.(*fasthttp.Cookie)
	if ok {
		return string(v.Value())
	} else {
		return ""
	}
}

// /////////////////////////////////////////////////////////////////////////////////
type ECFastHttpRequest struct {
	ctx      *fasthttp.RequestCtx
	userForm map[string]string
}

func NewECFastHttpRequest(ctx *fasthttp.RequestCtx) *ECFastHttpRequest {
	resp := new(ECFastHttpRequest)
	resp.userForm = make(map[string]string)
	resp.ctx = ctx
	return resp
}
func (me *ECFastHttpRequest) CloseBody() error {
	if me.ctx == nil {
		return FastHttpCtxNull
	}
	me.ctx.ResetBody()
	return nil
}
func (me *ECFastHttpRequest) ParseForm() error {
	if me.ctx == nil {
		return FastHttpCtxNull
	}
	return nil
}
func (me *ECFastHttpRequest) RemoteAddress() string {
	if me.ctx == nil {
		return ""
	}
	return me.ctx.RemoteAddr().String()
}

func (me *ECFastHttpRequest) RemoteIPAddress() string {
	if me.ctx == nil {
		return ""
	}
	addr := me.ctx.RemoteAddr().String()
	index := strings.IndexByte(addr, ':')
	if index > 0 {
		return addr[:index]
	} else {
		return addr
	}
}

func (me *ECFastHttpRequest) RequestURI() string {
	if me.ctx == nil {
		return ""
	}
	return string(me.ctx.RequestURI())
}

func (me *ECFastHttpRequest) UrlPath() string {
	if me.ctx == nil {
		return ""
	}
	return string(me.ctx.URI().Path())
}

func (me *ECFastHttpRequest) Close() error {
	if me.ctx == nil {
		return FastHttpCtxNull
	}
	me.ctx.Request.SetConnectionClose()
	return nil
}
func (me *ECFastHttpRequest) GetCookie(name string) *ECCookieInfo {
	resp := new(ECCookieInfo)
	if me.ctx == nil {
		return nil
	}
	v := me.ctx.Request.Header.Cookie(name)
	if v == nil {
		return nil
	}
	resp.Name = name
	resp.Value = string(v)
	return resp
}

func (me *ECFastHttpRequest) GetHttpHeaderAll() []*ECHttpHeader {
	resp := make([]*ECHttpHeader, 0)
	if me.ctx == nil {
		return nil
	}
	headerMap := make(map[string]*ECHttpHeader)
	me.ctx.Request.Header.VisitAll(func(key, value []byte) {
		name := strings.ToLower(string(key))
		v, ok := headerMap[name]
		if ok {
			v.Value = append(v.Value, string(value))
		} else {
			v := new(ECHttpHeader)
			v.Name = string(key)
			v.Value = make([]string, 0)
			v.Value = append(v.Value, string(value))
			headerMap[name] = v
			resp = append(resp, v)
		}
	})
	return resp
}

func (me *ECFastHttpRequest) GetHttpHeader(name string) []string {
	resp := make([]string, 0)
	if me.ctx == nil {
		return nil
	}
	v := me.ctx.Request.Header.Peek(name)
	if v != nil {
		resp = append(resp, string(v))
		return resp
	} else {
		return nil
	}
}

func (me *ECFastHttpRequest) Host() string {
	if me.ctx == nil {
		return ""
	}
	return string(me.ctx.Host())
}

func (me *ECFastHttpRequest) Method() string {
	if me.ctx == nil {
		return ""
	}
	return string(me.ctx.Method())
}

func (me *ECFastHttpRequest) UserAgent() string {
	if me.ctx == nil {
		return ""
	}
	return string(me.ctx.UserAgent())
}

func (me *ECFastHttpRequest) GetHttpHeaderOne(name string, isSet *bool) string {
	v := me.GetHttpHeader(name)
	if v != nil && len(v) > 0 {
		if isSet != nil {
			*isSet = true
		}
		return v[0]
	} else {
		if isSet != nil {
			*isSet = false
		}
		return ""
	}
}

func (me *ECFastHttpRequest) ReadBody(maxSize int64) ([]byte, error) {
	if me.ctx == nil {
		return nil, FastHttpCtxNull
	}
	return me.ctx.Request.Body(), nil
}
func (me *ECFastHttpRequest) ParseMultipartForm(maxSize int64) error {
	if me.ctx == nil {
		return FastHttpCtxNull
	}
	_, err := me.ctx.MultipartForm()
	return err
}
func (me *ECFastHttpRequest) SetForm(name, value string) {
	if me.ctx == nil {
		return
	}
	me.userForm[name] = value
}
func (me *ECFastHttpRequest) Form(name string) (string, bool) {
	if me.ctx == nil {
		return "", false
	}
	v := me.ctx.FormValue(name)
	if v != nil {
		return string(v), true
	} else {
		return "", false
	}
}
func (me *ECFastHttpRequest) GetForms() (map[string][]string, error) {
	resp := make(map[string][]string)
	if me.ctx == nil {
		return resp, FastHttpCtxNull
	}
	mutiForm, _ := me.ctx.MultipartForm()
	if mutiForm != nil {
		for k, v := range mutiForm.Value {
			name := strings.ToLower(k)
			existArg, ok := resp[name]
			if ok {
				existArg = append(existArg, v...)
				resp[name] = existArg
			} else {
				existArg = make([]string, 0)
				existArg = append(existArg, v...)
				resp[name] = existArg
			}
		}
	}
	for k, v := range me.userForm {
		name := strings.ToLower(k)
		existArg, ok := resp[name]
		if ok {
			existArg = append(existArg, v)
			resp[name] = existArg
		} else {
			existArg = make([]string, 0)
			existArg = append(existArg, v)
			resp[name] = existArg
		}
	}
	if me.ctx.IsPost() {
		me.ctx.Request.PostArgs().VisitAll(func(key []byte, value []byte) {
			name := string(key)
			v, ok := resp[name]
			if ok {
				v = append(v, string(value))
				resp[name] = v
			} else {
				v = make([]string, 0)
				v = append(v, string(value))
				resp[name] = v
			}
		})
		me.ctx.QueryArgs().VisitAll(func(key []byte, value []byte) {
			name := string(key)
			v, ok := resp[name]
			if ok {
				v = append(v, string(value))
				resp[name] = v
			} else {
				v = make([]string, 0)
				v = append(v, string(value))
				resp[name] = v
			}
		})
	} else {
		me.ctx.QueryArgs().VisitAll(func(key []byte, value []byte) {
			name := string(key)
			v, ok := resp[name]
			if ok {
				v = append(v, string(value))
				resp[name] = v
			} else {
				v = make([]string, 0)
				v = append(v, string(value))
				resp[name] = v
			}
		})
	}
	return resp, nil
}

func (me *ECFastHttpRequest) ModifyUrlPath(path string) {
	if me.ctx == nil {
		return
	}
	me.ctx.Request.URI().SetPath(path)
}

func (me *ECFastHttpRequest) GetFormFileByName(name string) (*ECFormFileInfo, error) {
	if me.ctx == nil {
		return nil, FastHttpCtxNull
	}
	if name == "" {
		files, err := me.GetFormFiles()
		if err != nil {
			return nil, err
		}
		if len(files) > 0 {
			return files[0], nil
		} else {
			return nil, errors.New("not form file")
		}
	}
	formFile, err := me.ctx.FormFile(name)
	if err != nil {
		return nil, err
	}
	resp := new(ECFormFileInfo)
	resp.Filename = formFile.Filename
	resp.Size = formFile.Size
	return resp, nil
}

func (me *ECFastHttpRequest) GetFormFiles() ([]*ECFormFileInfo, error) {
	if me.ctx == nil {
		return nil, FastHttpCtxNull
	}
	mutiForm, err := me.ctx.MultipartForm()
	if err != nil {
		return nil, err
	}
	resp := make([]*ECFormFileInfo, 0)
	for k, v := range mutiForm.File {
		for _, f := range v {
			item := new(ECFormFileInfo)
			item.Name = k
			item.Filename = f.Filename
			item.Size = f.Size
			item.Header = f.Header
			resp = append(resp, item)
		}
	}
	return resp, nil
}
func (me *ECFastHttpRequest) GetFormFileContext(name string) (*ECFormFileInfo, []byte, error) {
	fileInfo, err := me.GetFormFileByName(name)
	if err != nil {
		return fileInfo, nil, err
	}
	if name == "" {
		name = fileInfo.Name
	}
	mutiData, err := me.ctx.FormFile(name)
	if err != nil {
		return fileInfo, nil, err
	}
	fh, err := mutiData.Open()
	if err != nil {
		return fileInfo, nil, err
	}
	defer fh.Close()
	data := new(bytes.Buffer)
	tmpBuf := make([]byte, 4096)
	for {
		n, err := fh.Read(tmpBuf)
		if n > 0 {
			data.Write(tmpBuf[:n])
		}
		if err != nil {
			break
		}
	}
	return fileInfo, data.Bytes(), err
}

// /////////////////////////////////////////////////////////////////////////
type ECFastHttpResponse struct {
	ctx *fasthttp.RequestCtx
}

func NewECFastHttpResponse(ctx *fasthttp.RequestCtx) *ECFastHttpResponse {
	resp := new(ECFastHttpResponse)
	resp.ctx = ctx
	return resp
}

func (me *ECFastHttpResponse) WriteString(str string) error {
	if me.ctx == nil {
		return FastHttpCtxNull
	}
	_, err := me.ctx.WriteString(str)
	return err
}

func (me *ECFastHttpResponse) Write(data []byte) error {
	if me.ctx == nil {
		return FastHttpCtxNull
	}
	_, err := me.ctx.Write(data)
	return err
}

func (me *ECFastHttpResponse) AddHttpHeader(name, value string) error {
	if me.ctx == nil {
		return FastHttpCtxNull
	}
	me.ctx.Response.Header.Add(name, value)
	return nil
}

func (me *ECFastHttpResponse) SetHttpHeader(name, value string) error {
	if me.ctx == nil {
		return FastHttpCtxNull
	}
	me.ctx.Response.Header.Del(name)
	me.ctx.Response.Header.Set(name, value)
	return nil
}

func (me *ECFastHttpResponse) DelHttpHeader(name string) error {
	if me.ctx == nil {
		return FastHttpCtxNull
	}
	me.ctx.Response.Header.Del(name)
	return nil
}

func (me *ECFastHttpResponse) GetHttpHeader(name string) string {
	if me.ctx == nil {
		return ""
	}
	v := me.ctx.Response.Header.Peek(name)
	if v != nil {
		return string(v)
	}
	return ""
}

func (me *ECFastHttpResponse) SetHttpCode(code int) error {
	if me.ctx == nil {
		return FastHttpCtxNull
	}
	me.ctx.Response.SetStatusCode(code)
	return nil
}
func (me *ECFastHttpResponse) Redirect(url string, code int) error {
	if me.ctx == nil {
		return FastHttpCtxNull
	}
	me.ctx.Redirect(url, code)
	return nil
}
func (me *ECFastHttpResponse) SetCookie(cookie *ECCookieInfo) error {
	if me.ctx == nil {
		return FastHttpCtxNull
	}
	c := new(fasthttp.Cookie)
	c.SetKey(cookie.Name)
	c.SetValue(cookie.Value)
	c.SetPath(cookie.Path)
	me.ctx.Response.Header.SetCookie(c)
	return nil
}
