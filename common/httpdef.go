package common

import (
	"crypto/tls"
	xsync "gitee.com/xlm516/xtool/sync"
	"net"
	"net/textproto"
)

const (
	SESSION_COOKIE_NAME = "_SESSION_"
)

type HttpType int

const (
	GOHTTP HttpType = iota
	FASTHTTP
)

type ECHttpRequest interface {
	CloseBody() error
	RemoteAddress() string
	RemoteIPAddress() string
	UrlPath() string
	RequestURI() string
	Close() error
	GetCookie(name string) *ECCookieInfo
	GetCookieSlice(name string) []*ECCookieInfo
	GetHttpHeader(name string) []string
	GetHttpHeaderOne(name string, isSet *bool) string
	GetHttpHeaderAll() []*ECHttpHeader
	Method() string
	UserAgent() string
	Host() string
	ReadBody(maxSize int64) ([]byte, error)
	ParseMultipartForm(maxSize int64) error
	GetForms() (map[string][]string, error)
	SetForm(name, value string)
	Form(name string) (string, bool)
	ModifyUrlPath(path string)
	ParseForm() error
	GetFormFileByName(name string) (*ECFormFileInfo, error)
	GetFormFiles() ([]*ECFormFileInfo, error)
	GetFormFileContext(name string) (*ECFormFileInfo, []byte, error)
}

type ECHttpResponse interface {
	WriteString(str string) error
	Write(data []byte) error
	SetHttpHeader(name, value string) error
	AddHttpHeader(name, value string) error
	DelHttpHeader(name string) error
	GetHttpHeader(name string) string
	SetHttpCode(code int) error
	Redirect(url string, code int) error
	SetCookie(cookie *ECCookieInfo) error
}

type ECHttpContext struct {
	Request  ECHttpRequest
	Response ECHttpResponse

	Http     interface{}
	Www      *WwwManager
	HttpType HttpType
}

type ECHttpCookie struct {
	HttpType   HttpType
	Cookie     interface{}
	CookieInfo ECCookieInfo
}

func (me *ECHttpCookie) GetName() string {
	if me.HttpType == FASTHTTP {
		return me.GetFastName()
	} else {
		return me.GetGoName()
	}
}
func (me *ECHttpCookie) GetValue() string {
	if me.HttpType == FASTHTTP {
		return me.GetFastValue()
	} else {
		return me.GetGoValue()
	}
}
func (me *ECHttpCookie) GetPath() string {
	if me.HttpType == FASTHTTP {
		return me.GetFastPath()
	} else {
		return me.GetGoPath()
	}
}

type ECCookieInfo struct {
	Name  string
	Value string
	Path  string
}

type ECHttpHandler func(handler *ECHttpContext)
type RouteCallback func(handler *ECHttpContext, ext string)
type WwwManager struct {
	Uri         string            `json:"uri"`
	RootDir     string            `json:"rootDir"`
	ViewDir     string            `json:"viewDir"`
	ModelDir    string            `json:"modelDir"`
	Page404     string            `json:"page404"`
	PageErr     string            `json:"pageErr"`
	MemFile     int32             `json:"memFile"`
	UploadMax   int32             `json:"uploadMax"`
	HandlerType string            `json:"handlerType"`
	Header      map[string]string `json:"header"`
	ShowDir     bool              `json:"showDir"` /*如果是handler file 时候显示目录文件信息*/
	CgiBin      string            `json:"cgiBin"`
	CgiPort     int               `json:"cgiPort"`
	CgiTimeout  int               `json:"cgiTimeout"`
	CgiSrc      string            `json:"cgiSrc"`
	CgiCompile  string            `json:"cgiCompile"`
	//////////////////////////////////////////////////////////
	Handler   ECHttpHandler `json:"-"`
	SecondUri string        `json:"-"` /*包含mvc信息*/
	CgiId     int           `json:"-"` /*CGIMgr*/
	//CgiProg         *Shell       `json:"-"` /*CGI Progs*/
	CompilerHandler interface{} `json:"-"` /*CGI Progs*/
}
type WebSchedule struct {
	Types     int
	LastCheck int64
	Interval  int64
	Name      string
	LuaFile   string
	Value     []string
}
type WebSite struct {
	Bind          string                  `json:"bind"`
	Https         int                     `json:"https"`
	HttpCert      string                  `json:"httpCert"`
	HttpKey       string                  `json:"httpKey"`
	Timeout       int                     `json:"timeout"`
	OnLoad        string                  `json:"onload"`
	Debug         int                     `json:"debug"`
	Www           []WwwManager            `json:"www"`
	Http          interface{}             `json:"-"`
	ScheduleMutex *xsync.MutexEx          `json:"-"`
	Schedule      map[string]*WebSchedule `json:"-"`
	OnLoadPath    string                  `json:"-"`
}
type ConnStatusCallback func(c net.Conn, s ConnState) bool
type HttpRoute struct {
	url string
	fun RouteCallback
}

type ConnState int

const (
	StateNew ConnState = iota
	StateActive
	StateIdle
	StateHijacked
	StateClosed
)

type ConnInfo struct {
	CreateTime int64
	UpdateTime int64
	State      ConnState
	Conn       net.Conn
	SessData   map[string]string
	Userdata   interface{}
	LastUrl    string
}

type SessionVar struct {
	Name       string
	Value      interface{}
	CreateTime int64
}

type Session struct {
	Name       string
	Path       string
	CreateTime int64
	Timeout    int
	Count      int
	SessionVar map[string]*SessionVar
}

type SessionSlice []*Session

func (s SessionSlice) Len() int      { return len(s) }
func (s SessionSlice) Swap(i, j int) { s[i], s[j] = s[j], s[i] }
func (s SessionSlice) Less(i, j int) bool {
	if s[i].CreateTime > s[j].CreateTime {
		return true
	}
	return false
}

type ECFormFileInfo struct {
	Name     string
	Filename string
	Size     int64
	Header   textproto.MIMEHeader
}

type ECHttpHeader struct {
	Name  string
	Value []string
}

type HTTPClientOption struct {
	InsecureSkipVerify bool
	Keepalive          bool
	Proxy              string
	UserAgent          string
	OutIP              string
	CA                 string
	Cert               string
	CertKey            string
}

type ECHttpServer interface {
	GetListener() net.Listener
	SetFd(fd uintptr) uintptr
	CloseListen()
	CloseAllClient()
	Lock()
	Unlock()
	GetConnectList() map[string]*ConnInfo
	GetConnInfo(remoteAddr string) *ConnInfo
	SetTcpTimeout(s int)
	SetBindHost(host string)
	SetHandler(h ECHttpHandler)
	Start() (bool, error)
	Listen() (bool, error)
	ListenTLS(certFile, keyFile string, tlsConfig *tls.Config) (bool, error)
	SetCookie(name string, value interface{}, cookieName string,
		sessionName string, path string, timeout int) *ECHttpCookie
	SetCookieTimeout(sessionName string, t int, url string) bool
	GetCookieAll(sessionName string, url string) map[string]interface{}
	GetCookie(name string, sessionName string, url string) (*SessionVar, *ECHttpCookie)
	DelCookie(sessionName string, url string)
	UpdateLastUrl(remoteAddr, url string)
	DumpCookie(limit int) []*Session
}
