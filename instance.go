package xhttp

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"gitee.com/xlm516/http/common"
	"gitee.com/xlm516/xtool/cast"
	"gitee.com/xlm516/xtool/constvar"
	"gitee.com/xlm516/xtool/dbg"
	xfile "gitee.com/xlm516/xtool/file"
	xid "gitee.com/xlm516/xtool/id"
	xstring "gitee.com/xlm516/xtool/string"
	"io"
	"os"
	"strings"
)

type ECJsonRPCData struct {
	rpcId       uint64
	rpcVer      string
	errorCode   int
	errorString string
}

type ECHttpOut struct {
	httpHeader map[string]string
	httpCookie map[string]string
	cookiePath string
}

type ECHttpIn struct {
	SecondUrl         string
	reqUri            string
	cookiePath        string
	cookieSessionName string
	httpBind          *ECHttpBinds
	httpCtx           *common.ECHttpContext
	httpEx            common.ECHttpServer
	httpEnv           map[string]interface{}
	sessionName       string
	sessionNames      []string
	host              string
}

type ECInstance struct {
	Router  *ECRouter
	VmDB    interface{}
	Http    *ECHttp
	data    []byte
	jsonObj map[string]interface{}
	//////////////////////////
	connWriter  io.Writer
	outBuffer   bytes.Buffer
	rpcData     ECJsonRPCData
	rpcArgument []interface{}
	httpOutData ECHttpOut
	httpInData  ECHttpIn
	httpCode    int

	RedirectFile     bool
	RedirectFilePath string
	RedirectFileName string
}

func NewECInstance() *ECInstance {
	ins := new(ECInstance)
	ins.init()
	return ins
}

func (me *ECInstance) init() {
	me.httpOutData.httpHeader = make(map[string]string)
	me.httpOutData.httpCookie = make(map[string]string)
	me.httpInData.httpEnv = make(map[string]interface{})
}
func (me *ECInstance) GetHttpCtx() *common.ECHttpContext {
	return me.httpInData.httpCtx
}
func (me *ECInstance) GetRequestData() []byte {
	return me.data
}

func (me *ECInstance) GetRouterData() interface{} {
	return me.Router.userData
}

func (me *ECInstance) GetRPCArgument() []interface{} {
	if me.rpcArgument == nil {
		return make([]interface{}, 0)
	}
	return me.rpcArgument
}

func (me *ECInstance) SetConnWriter(w io.Writer) {
	me.connWriter = w
}

func (me *ECInstance) Write(b []byte) {
	me.outBuffer.Write(b)
}

func (me *ECInstance) WriteObject(o interface{}) {
	b, _ := json.Marshal(o)
	me.outBuffer.Write(b)
}

func (me *ECInstance) WriteString(str string) {
	me.outBuffer.WriteString(str)
}
func (me *ECInstance) GetOutBuffer() []byte {
	return me.outBuffer.Bytes()
}
func (me *ECInstance) WriteConn(b []byte) {
	if me.httpInData.httpCtx != nil {
		me.httpInData.httpCtx.Response.Write(b)
	} else if me.connWriter != nil {
		me.connWriter.Write(b)
	}
}

func (me *ECInstance) JsonRPCError(code int, errStr string) {
	me.rpcData.errorCode = code
	me.rpcData.errorString = errStr
}

func (me *ECInstance) HttpHeader(name string) string {
	return me.httpInData.httpCtx.Request.GetHttpHeaderOne(name, nil)
}

func (me *ECInstance) HttpMethod() string {
	method := me.httpInData.httpCtx.Request.Method()
	return strings.ToUpper(method)
}

func (me *ECInstance) AddHttpHeader(name, value string) {
	me.httpOutData.httpHeader[name] = value
}
func (me *ECInstance) GetFormFileInfo(formName string) (string, int64) {
	formInfo, err := me.httpInData.httpCtx.Request.GetFormFileByName(formName)
	if err == nil {
		return formInfo.Filename, formInfo.Size
	} else {
		return "", 0
	}
}
func (me *ECInstance) GetAllFormFileInfo() []*common.ECFormFileInfo {
	v, _ := me.httpInData.httpCtx.Request.GetFormFiles()
	return v
}
func (me *ECInstance) GetFormFile(formName string) ([]byte, *common.ECFormFileInfo, error) {
	info, d, err := me.httpInData.httpCtx.Request.GetFormFileContext(formName)
	if info != nil {
		d = d[:info.Size]
	}
	return d, info, err
}
func (me *ECInstance) SaveFormFile(formName string, filename string) error {
	header, d, err := me.httpInData.httpCtx.Request.GetFormFileContext(formName)
	if err == nil && d != nil && len(d) > 0 {
		if filename == "" {
			filename = header.Filename
		}
		tmpPath := xstring.GetFilePath(filename)
		xfile.Mkdir(tmpPath)
		fh, err := os.OpenFile(filename, os.O_WRONLY|os.O_CREATE, 0666)
		if err != nil {
			return err
		}
		defer fh.Close()
		fh.Write(d)
		return nil
	}
	return err
}

func (me *ECInstance) Form(name string) (string, bool) {
	v, _ := me.httpInData.httpEnv["_request"]
	if v == nil {
		return "", false
	}
	req, ok := v.(map[string]interface{})
	if ok == false {
		return "", false
	}
	v, ok = req[name]
	if ok {
		return fmt.Sprintf("%v", v), true
	} else {
		return "", false
	}
}

func (me *ECInstance) FormAll() map[string]interface{} {
	v, _ := me.httpInData.httpEnv["_request"]
	if v == nil {
		return make(map[string]interface{})
	}
	req, ok := v.(map[string]interface{})
	if ok == false {
		return make(map[string]interface{})
	}
	return req
}

func (me *ECInstance) GetHttpEnv() map[string]interface{} {
	return me.httpInData.httpEnv
}

func (me *ECInstance) SetSession(name string, value interface{}, path string) error {
	if me.httpInData.httpBind == nil {
		return errors.New("do not init session")
	}
	ecHttp := me.httpInData.httpBind.ecHttp
	if ecHttp == nil || ecHttp.session == nil {
		return errors.New("do not init session")
	}
	if ecHttp.session.CheckCookie(me.httpInData.host, me.httpInData.sessionName,
		me.httpInData.cookiePath) == false {
		me.httpInData.sessionName = ""
	}
	if me.httpInData.sessionName == "" {
		me.httpInData.sessionName = xid.NewKey28()
		me.httpOutData.cookiePath = me.httpInData.cookiePath
		me.httpOutData.httpCookie[me.httpInData.cookieSessionName] = me.httpInData.sessionName
	}
	if path == "" {
		path = me.httpInData.cookiePath
	} else {
		me.httpOutData.cookiePath = path
	}
	err := ecHttp.session.SetCookie(me.httpInData.host, path, me.httpInData.sessionName,
		name, value, 0)
	return err
}

func (me *ECInstance) GetSession(name string) interface{} {
	if me.httpInData.httpBind == nil {
		return nil
	}
	ecHttp := me.httpInData.httpBind.ecHttp
	if ecHttp == nil || ecHttp.session == nil {
		return nil
	}
	return ecHttp.session.GetCookie(me.httpInData.host, me.httpInData.sessionName, name)
}

func (me *ECInstance) DeleteSession(host string, session string) error {
	if me.httpInData.httpBind == nil {
		return nil
	}
	ecHttp := me.httpInData.httpBind.ecHttp
	if ecHttp == nil || ecHttp.session == nil {
		return nil
	}
	return ecHttp.session.Delete(host, session)
}

func (me *ECInstance) GetSessionAll(name string) []interface{} {
	if me.httpInData.httpBind == nil {
		return nil
	}
	ecHttp := me.httpInData.httpBind.ecHttp
	if ecHttp == nil || ecHttp.session == nil {
		return nil
	}
	return ecHttp.session.GetAll(name)
}

func (me *ECInstance) GetSessionName() string {
	if me.httpInData.httpBind == nil {
		return ""
	}
	return me.httpInData.sessionName
}

func (me *ECInstance) SetSessionTimeout(v int) {
	if me.httpInData.httpBind == nil {
		return
	}
	ecHttp := me.httpInData.httpBind.ecHttp
	if ecHttp == nil || ecHttp.session == nil {
		return
	}
	ecHttp.session.SetCookieTimeout(me.httpInData.host, me.httpInData.sessionName, v)
}

func (me *ECInstance) GetSessionInt64(name string) int64 {
	v := me.GetSession(name)
	f, ok := v.(float64)
	if ok {
		return int64(f)
	}

	f2, ok := v.(float32)
	if ok {
		return int64(f2)
	}

	retV, _ := cast.ToInt64(fmt.Sprintf("%v", v))
	return retV
}

func (me *ECInstance) GetSessionFloat64(name string) float64 {
	v := me.GetSession(name)
	f, ok := v.(float64)
	if ok {
		return f
	}

	f2, ok := v.(float32)
	if ok {
		return float64(f2)
	}

	return 0
}

func (me *ECInstance) GetSessionString(name string) string {
	v := me.GetSession(name)
	f, ok := v.(float64)
	if ok {
		return cast.FormatFloat(f)
	}

	f2, ok := v.(float32)
	if ok {
		return cast.FormatFloat32(f2)
	}

	return fmt.Sprintf("%v", v)
}

func (me *ECInstance) SetCookieTimeout(timeout int) {
	if me.httpInData.httpBind == nil {
		return
	}
	ecHttp := me.httpInData.httpBind.ecHttp
	if ecHttp == nil || ecHttp.session == nil {
		return
	}
	ecHttp.session.SetCookieTimeout(me.httpInData.host, me.httpInData.sessionName, timeout)
}

func (me *ECInstance) CheckSession(session string) bool {
	if me.httpInData.httpBind == nil {
		return false
	}
	ecHttp := me.httpInData.httpBind.ecHttp
	if ecHttp == nil || ecHttp.session == nil {
		return false
	}
	return ecHttp.session.CheckCookie(me.httpInData.host, session, me.httpInData.cookiePath)
}

func (me *ECInstance) GetConnUserData() interface{} {
	if me.httpInData.httpBind == nil {
		return nil
	}
	connInfo := me.httpInData.httpEx.GetConnInfo(me.httpInData.httpCtx.Request.RemoteAddress())
	if connInfo == nil {
		return nil
	}
	return connInfo.Userdata
}

func (me *ECInstance) SetConnUserData(v interface{}) {
	if me.httpInData.httpBind == nil {
		return
	}
	connInfo := me.httpInData.httpEx.GetConnInfo(me.httpInData.httpCtx.Request.RemoteAddress())
	if connInfo == nil {
		return
	}
	connInfo.Userdata = v
}

func (me *ECInstance) GetConnData(name string) string {
	if me.httpInData.httpBind == nil {
		return ""
	}

	connInfo := me.httpInData.httpEx.GetConnInfo(me.httpInData.httpCtx.Request.RemoteAddress())
	if connInfo == nil {
		return ""
	}
	v, ok := connInfo.SessData[name]
	if ok {
		return v
	} else {
		return ""
	}
}

func (me *ECInstance) SetHttpCode(code int) {
	me.httpCode = code
	//_ = me.httpInData.httpCtx.Response.SetHttpCode(code)
}

func (me *ECInstance) SetHttpHeader(name string, value string) {
	_ = me.httpInData.httpCtx.Response.SetHttpHeader(name, value)
}

func (me *ECInstance) SetConnData(name, value string) {
	if me.httpInData.httpBind == nil {
		return
	}
	connInfo := me.httpInData.httpEx.GetConnInfo(me.httpInData.httpCtx.Request.RemoteAddress())
	if connInfo == nil {
		return
	}
	connInfo.SessData[name] = value
}

func (me *ECInstance) GetHttpEx() common.ECHttpServer {
	return me.httpInData.httpEx
}

func (me *ECInstance) RemoteAddr() string {
	return me.httpInData.httpCtx.Request.RemoteAddress()
}

func (me *ECInstance) RemoteIP() string {
	str := me.httpInData.httpCtx.Request.RemoteAddress()
	index := strings.IndexByte(str, ':')
	if index > 0 {
		str = str[:index]
	}
	return str
}

func (me *ECInstance) RequestURI() string {
	return me.httpInData.httpCtx.Request.RequestURI()
}

// 处理模板文件
func (me *ECInstance) HandlerHttp(ins *ECInstance, uri string, arg interface{}) {
	me.outBuffer.Reset()
	if arg != nil {
		tp := NewECTemplate()
		err := tp.RunFiles(constvar.ApplicationPath + "/" + uri + ".htm")
		if err != nil {
			dbg.Dbg(err.Error())
		} else {
			me.WriteConn(tp.GetBuffer())
		}
	} else {
		if me.httpInData.httpBind != nil && me.httpInData.httpBind.ecHttp != nil {
			ecHttp := me.httpInData.httpBind.ecHttp
			ins.httpInData.httpCtx.Request.ModifyUrlPath(uri)
			ecHttp.GetRouter().doFileServer(ins, ecHttp.globalRoute)
		}
	}
}

func (me *ECInstance) ToJson(v interface{}) []byte {
	b, err := json.Marshal(v)
	if err != nil {
		dbg.Dbg("%s\n", err.Error())
		return make([]byte, 0)
	} else {
		return b
	}
}

func (me *ECInstance) AddHeaderAttachment(filename string) {
	me.AddHttpHeader("Pragma", "No-cache")
	me.AddHttpHeader("Cache-Control", "No-cache")
	me.AddHttpHeader("Content-Disposition", fmt.Sprintf("attachment; filename=%s", filename))
}
