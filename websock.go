package xhttp

import (
	"errors"
	"gitee.com/xlm516/xtool/dbg"
	xlist "gitee.com/xlm516/xtool/list"
	"strings"
)

type WsMode int

type WsConnIf interface {
	ReadMessage() (messageType int, p []byte, err error)
	WriteMessage(messageType int, data []byte) error
}

type WsSendBuf struct {
	data     []byte
	dataType int
}

type WsClientInfo struct {
	Data       interface{}
	remoteAddr string
	conn       WsConnIf
	sendBuf    *xlist.Queue //*WsSendBuf
}

var goWsEchoHandle func(ins *ECInstance)
var fastWsEchoHandle func(ins *ECInstance)

var goWsHandle func(ins *ECInstance)
var fastWsHandle func(ins *ECInstance)

func (me *ECHttp) AddWebSocketEcho(path string, handler WsHandlerCall, rpcServer *ECRPCServer) {
	var router *ECRouter
	if me.config.FastHttp {
		if fastWsEchoHandle == nil {
			dbg.Dbg("AddWebSocketEcho: not implement fast http\n")
			return
		}
		router = me.GetRouter().AddRouter(path, fastWsEchoHandle, true, "ws")
	} else {
		if goWsEchoHandle == nil {
			dbg.Dbg("AddWebSocketEcho: not implement go http\n")
			return
		}
		router = me.GetRouter().AddRouter(path, goWsEchoHandle, true, "ws")
	}
	if router == nil {
		return
	}
	router.wsCallback = handler
	router.wsRPCServer = rpcServer
	router.wsClientMap = make(map[string]*WsClientInfo)
}

func (me *ECHttp) AddWebSocket(path string, handler WsHandlerCall, event WsEventCall) {
	var router *ECRouter
	if me.config.FastHttp {
		if fastWsHandle == nil {
			dbg.Dbg("AddWebSocketEcho: not implement fast http\n")
			return
		}
		router = me.GetRouter().AddRouter(path, fastWsHandle, true, "ws")
	} else {
		if goWsHandle == nil {
			dbg.Dbg("AddWebSocketEcho: not implement go http\n")
			return
		}
		router = me.GetRouter().AddRouter(path, goWsHandle, true, "ws")
	}
	if router == nil {
		return
	}
	router.wsCallback = handler
	router.wsEvent = event
	router.wsClientMap = make(map[string]*WsClientInfo)
}

// NotifyWsClient address为空通知所有客户端
func (me *ECHttp) NotifyWsClient(path, address string, dataType int, data []byte) error {
	name := ""
	index := strings.IndexByte(path, '@')
	if index > 0 {
		name = path[index+1:]
		path = path[:index]
	}
	r, _ := me.GetRouter().GetRouter(name, path)
	if r.path == "" {
		return errors.New("not find")
	}
	if r.wsClientMap == nil {
		return errors.New("maybe echo mode")
	}
	r.wsMutex.Lock()
	defer r.wsMutex.Unlock()
	if address != "" {
		v, ok := r.wsClientMap[address]
		if ok {
			b := new(WsSendBuf)
			b.dataType = dataType
			b.data = make([]byte, len(data))
			copy(b.data, data)
			_ = v.sendBuf.PutNoWait(b)
		}
	} else {
		for _, v := range r.wsClientMap {
			b := new(WsSendBuf)
			b.dataType = dataType
			b.data = make([]byte, len(data))
			copy(b.data, data)
			_ = v.sendBuf.PutNoWait(b)
		}
	}
	return nil
}
