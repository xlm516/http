package xhttp

import (
	"errors"
	"fmt"
	"gitee.com/xlm516/xtool/dbg"
	xlist "gitee.com/xlm516/xtool/list"
	xtime "gitee.com/xlm516/xtool/time"
	"strings"
	"sync"
	"time"
)

type ECSessionItem struct {
	Ip             string
	Host           string
	Uri            string
	Session        string
	UpdateTime     int64
	Timeout        int
	Values         map[string]interface{} /* map by value name */
	SyncCount      int
	lastNotify     int64
	timeoutChanged bool
}

type ECSessionHost struct {
	lastUpdate  int64
	host        string
	sessionHost map[string]*ECSessionItem /* map by session name */
}

func (me *ECSessionHost) GetSession() map[string]*ECSessionItem {
	return me.sessionHost
}

type ECSession struct {
	session        map[string]*ECSessionHost /* map by ip */
	mutex          *sync.Mutex
	lastCheck      int64
	memUrl         string
	memDBRead      *ECMemDBClient
	memDBWrite     *ECMemDBClient
	writeQueue     *xlist.Queue
	pendingSession *ECSessionItem
	changedSession map[string]*ECSessionItem
	enableMemDb    bool
}

const (
	EC_DEFAULT_TIMEOUT = 15 * 60
)

func GetSessionName(uri string, bindPort int) string {
	if uri == "" {
		uri = "/"
	}
	str := ""
	for _, v := range uri {
		str += fmt.Sprintf("Session_%X_%d", v, bindPort)
	}
	return str
}

func (me *ECSession) init() {
	me.mutex = new(sync.Mutex)
	me.session = make(map[string]*ECSessionHost)
	if me.memUrl == "" {
		me.memUrl = "127.0.0.1:7788/rpc"
	}
	if me.enableMemDb {
		me.memDBRead = NewECMemDBClient(me.memUrl)
		me.memDBWrite = NewECMemDBClient(me.memUrl)
	}
	me.writeQueue = xlist.NewQueue(20000)
	me.changedSession = make(map[string]*ECSessionItem)
	go me.writeLoop()
}

func (me *ECSession) SetEnableMemDB(v bool) {
	me.enableMemDb = v
	if me.enableMemDb {
		me.memDBRead = NewECMemDBClient(me.memUrl)
		me.memDBWrite = NewECMemDBClient(me.memUrl)
	} else {
		me.memDBRead = nil
		me.memDBWrite = nil
	}
}

func (me *ECSession) Reset() {
	me.mutex.Lock()
	defer me.mutex.Unlock()
	me.session = make(map[string]*ECSessionHost)
	me.changedSession = make(map[string]*ECSessionItem)
}

func (me *ECSession) notifyChangeSession() {
	me.mutex.Lock()
	defer me.mutex.Unlock()
	nowTime := xtime.UnixTime()
	if me.enableMemDb {
		return
	}
	for k, v := range me.changedSession {
		if xtime.CompTime(nowTime, v.UpdateTime) >= 60 || xtime.CompTime(nowTime, v.lastNotify) >= 5*60 || v.SyncCount <= 0 {
			dbg.Dbg("Set Cookie\n")
			err := me.writeQueue.PutNoWait(v)
			if err != nil {
				dbg.Dbg("%s\n", err.Error())
			} else {
				v.lastNotify = nowTime
				v.SyncCount++
				delete(me.changedSession, k)
			}
		}
		if v.timeoutChanged && xtime.CompTime(nowTime, v.UpdateTime) >= 2 {
			err := me.writeQueue.PutNoWait(v)
			if err == nil {
				v.lastNotify = nowTime
				v.SyncCount++
				v.timeoutChanged = false
				delete(me.changedSession, k)
			}
		}
	}
}

func (me *ECSession) writeLoop() {
	if me.memDBWrite == nil {
		return
	}
	for {
		me.notifyChangeSession()
		for {
			if me.pendingSession != nil && me.memDBWrite != nil {
				_, err := me.memDBWrite.SetCookie(me.pendingSession)
				if err != nil {
					time.Sleep(time.Second * 15)
					break
				} else {
					me.pendingSession = nil
				}
			}
			v, err := me.writeQueue.GetNoWait()
			if err != nil {
				break
			}
			s, ok := v.(*ECSessionItem)
			if ok {
				_, err = me.memDBWrite.SetCookie(s)
				if err != nil {
					me.pendingSession = s
					time.Sleep(time.Second * 15)
					break
				}
			}
		}
		time.Sleep(time.Millisecond * 50)
	}
}

func (me *ECSession) ParseHost(host string) (ip string, port string) {
	index := strings.LastIndexByte(host, ':')
	if index >= 0 {
		ip = host[:index]
		port = host[index+1:]
	}
	if len(ip) > 0 {
		l := len(ip)
		if ip[0] == '[' && l > 2 {
			ip = ip[1 : l-1]
		}
	}
	return
}

func (me *ECSession) Delete(host, session string) error {
	me.mutex.Lock()
	defer me.mutex.Unlock()
	ip, _ := me.ParseHost(host)
	if ip != "" {
		sessionHost, ok := me.session[ip]
		if ok == false {
			return errors.New("not find host")
		}
		if sessionHost.sessionHost != nil {
			delete(sessionHost.sessionHost, session)
		}
	} else {
		for _, v := range me.session {
			if v.sessionHost != nil {
				delete(v.sessionHost, session)
			}
		}
	}
	return nil
}

func (me *ECSession) Set(host, uri string, session, name string, value interface{}, timeout int) error {
	me.mutex.Lock()
	defer me.mutex.Unlock()
	ip, _ := me.ParseHost(host)
	if timeout <= 0 {
		timeout = EC_DEFAULT_TIMEOUT
	}
	sessionHost, ok := me.session[ip]
	if ok == false {
		sessionHost = new(ECSessionHost)
		sessionHost.host = ip
		sessionHost.sessionHost = make(map[string]*ECSessionItem)
		me.session[ip] = sessionHost
	}
	if sessionHost == nil {
		return errors.New("Get Cookie Memeory Failed")
	}
	sessionHost.lastUpdate = xtime.UnixTime()
	if len(sessionHost.sessionHost) > 2500 {
		dbg.Dbg("session host reach max: %d, %+v\n", len(sessionHost.sessionHost), sessionHost.host)
		sessionHost.sessionHost = make(map[string]*ECSessionItem)
		return errors.New("Cookie too many")
	}
	s, ok := sessionHost.sessionHost[session]
	if ok {
		s.Values[name] = value
		s.UpdateTime = xtime.UnixTime()
		if me.enableMemDb {
			me.changedSession[s.Session] = s
		}
	} else {
		s = new(ECSessionItem)
		s.Session = session
		s.Host = host
		s.Uri = uri
		s.Ip = ip
		s.UpdateTime = xtime.UnixTime()
		s.Timeout = timeout
		s.Values = make(map[string]interface{})
		s.Values[name] = value
		sessionHost.sessionHost[session] = s
		if me.enableMemDb {
			me.changedSession[s.Session] = s
		}
	}
	return nil
}

func (me *ECSession) SetSession(host, session string, c *ECSessionItem) {
	if session == "" || c == nil {
		return
	}
	if c.Host == "" || c.Values == nil {
		return
	}
	ip, _ := me.ParseHost(host)
	me.mutex.Lock()
	defer me.mutex.Unlock()
	sessionHost, ok := me.session[ip]
	if ok == false {
		sessionHost = new(ECSessionHost)
		sessionHost.host = host
		sessionHost.lastUpdate = xtime.UnixTime()
		sessionHost.sessionHost = make(map[string]*ECSessionItem)
		me.session[ip] = sessionHost
	}
	sessionHost.sessionHost[session] = c
}

func (me *ECSession) GetSession(host, session string) *ECSessionItem {
	ip, _ := me.ParseHost(host)
	me.mutex.Lock()
	defer me.mutex.Unlock()

	sessionHost, ok := me.session[ip]
	if ok == false {
		return nil
	}
	s, ok := sessionHost.sessionHost[session]
	if ok {
		return s
	} else {
		return nil
	}
}

func (me *ECSession) GetAll(name string) []interface{} {
	me.mutex.Lock()
	defer me.mutex.Unlock()
	nowTime := xtime.UnixTime()
	resp := make([]interface{}, 0, 100)
	for _, v := range me.session {
		for _, v2 := range v.sessionHost {
			if xtime.CompTime(nowTime, v2.UpdateTime) > int64(v2.Timeout) {
				continue
			}
			d, ok := v2.Values[name]
			if ok {
				m := make(map[string]interface{})
				m["value"] = d
				m["session"] = v2.Session
				m["host"] = v2.Host
				resp = append(resp, m)
			}
		}
	}
	return resp
}

func (me *ECSession) Get(host, session, name string) interface{} {
	ip, _ := me.ParseHost(host)
	me.deleteTimeoutSession()
	me.mutex.Lock()
	defer me.mutex.Unlock()

	sessionHost, ok := me.session[ip]
	if ok == false {
		return nil
	}
	sessionHost.lastUpdate = xtime.UnixTime()
	s, ok := sessionHost.sessionHost[session]
	if ok {
		nowTime := xtime.UnixTime()
		if xtime.CompTime(nowTime, s.UpdateTime) > int64(s.Timeout) {
			delete(sessionHost.sessionHost, session)
			return nil
		}
		s.UpdateTime = nowTime
		if me.enableMemDb {
			me.changedSession[s.Session] = s
		}
		v, ok := s.Values[name]
		if ok {
			return v
		} else {
		}
	} else {
	}
	return nil
}

func (me *ECSession) Check(host, session, uri string) bool {
	ip, _ := me.ParseHost(host)
	me.mutex.Lock()
	defer me.mutex.Unlock()
	sessionHost, ok := me.session[ip]
	if ok == false {
		return false
	}
	_, ok = sessionHost.sessionHost[session]
	return ok
}

func (me *ECSession) SetTimeout(host, session string, v int) {
	ip, _ := me.ParseHost(host)
	me.mutex.Lock()
	defer me.mutex.Unlock()
	sessionHost, ok := me.session[ip]
	if ok == false {
		sessionHost = new(ECSessionHost)
		sessionHost.host = ip
		sessionHost.sessionHost = make(map[string]*ECSessionItem)
		sessionHost.lastUpdate = xtime.UnixTime()
		me.session[ip] = sessionHost
	}
	if sessionHost == nil {
		return
	}
	s, ok := sessionHost.sessionHost[session]
	if ok {
		s.Timeout = v
		s.timeoutChanged = true
		if me.enableMemDb {
			me.changedSession[s.Session] = s
		}
	}
}

func (me *ECSession) deleteTimeoutSession() {
	me.mutex.Lock()
	defer me.mutex.Unlock()
	nowTime := xtime.UnixTime()
	if xtime.CompTime(nowTime, me.lastCheck) < 120 {
		return
	}
	me.lastCheck = nowTime
	for host, session := range me.session {
		if xtime.CompTime(session.lastUpdate, nowTime) > 3600*2 {
			delete(me.session, host)
			continue
		}
		for k, v := range session.sessionHost {
			if xtime.CompTime(nowTime, v.UpdateTime) > int64(v.Timeout) {
				dbg.Dbg("delete http session: ip:%s timeout:%d\n", v.Ip, v.Timeout)
				delete(session.sessionHost, k)
			}
		}
	}
}

func NewECSession(url string) *ECSession {
	s := new(ECSession)
	s.memUrl = url
	s.init()
	return s
}

func (me *ECSession) GetCookie(host, session, name string) interface{} {
	if session == "" {
		return nil
	}
	//Dbg.Dbg("Get Cookie: host:%s, session:%s, name:%s\n", host, session, name)
	v := me.Get(host, session, name)
	if v == nil && me.enableMemDb && me.memDBRead != nil {
		s, err := me.memDBRead.GetCookie(host, session)
		if err == nil {
			s.SyncCount = 1
			me.SetSession(host, session, s)
		} else {
			dbg.Dbg("%s\n", err.Error())
		}
	}
	return v
}

func (me *ECSession) SetCookie(host, uri string, session, name string, value interface{}, timeout int) error {
	err := me.Set(host, uri, session, name, value, timeout)
	s := me.GetSession(host, session)
	if s != nil && me.memDBWrite != nil {
		_, _ = me.memDBWrite.SetCookie(s)
	}
	return err
}

func (me *ECSession) DeleteCookie(host, session string) {
	me.Delete(host, session)
}

func (me *ECSession) CheckCookie(host, session, uri string) bool {
	if session == "" {
		return false
	}
	v := me.Check(host, session, uri)
	if v == false && me.memDBRead != nil {
		s, _ := me.memDBRead.GetCookie(host, session)
		if s != nil {
			s.SyncCount = 1
			me.SetSession(host, session, s)
			return true
		}
	}
	return v
}

func (me *ECSession) SetCookieTimeout(host, session string, v int) {
	me.SetTimeout(host, session, v)
	s := me.GetSession(host, session)
	if s != nil {
		//me.memDBWrite.SetCookie(s)
	}
}
