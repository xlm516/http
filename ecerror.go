package xhttp

import (
	"errors"
	"fmt"
)

type ECError struct {
	errorStr   string
	errorCode  int
	timeout    bool
	connFailed bool
}

func (me *ECError) GetCode() int {
	return me.errorCode
}

func (me *ECError) Error() string {
	return me.errorStr
}

func (me *ECError) ErrorCode() int {
	return me.errorCode
}

func (me *ECError) Timeout() bool {
	return me.timeout
}

func (me *ECError) ConnectFailed() bool {
	return me.connFailed
}

func NewECError(str string, code ...int) *ECError {
	err := new(ECError)
	err.errorStr = str
	if len(code) > 0 {
		err.errorCode = code[0]
	}
	return err
}

func GetECErrorCode(err error) int {
	var ecErr *ECError
	ok := errors.As(err, &ecErr)
	if ok {
		return ecErr.errorCode
	}
	fmt.Println("node ec error")
	return 0
}

func IsECError(err error) bool {
	var ECError *ECError
	ok := errors.As(err, &ECError)
	if ok {
		return true
	}

	return false
}

func ECErrorTimeout(err error) bool {
	var ecErr *ECError
	ok := errors.As(err, &ecErr)
	if ok {
		return ecErr.timeout
	}

	return false
}

func ECErrorConnectFailed(err error) bool {
	var ecErr *ECError
	ok := errors.As(err, &ecErr)
	if ok {
		return ecErr.connFailed
	}

	return false
}
