package xhttp

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gitee.com/xlm516/xtool/constvar"
	"gitee.com/xlm516/xtool/dbg"
	xfile "gitee.com/xlm516/xtool/file"
	xstring "gitee.com/xlm516/xtool/string"
	xtime "gitee.com/xlm516/xtool/time"
	"io/ioutil"
	"os"
	"os/exec"
	"strings"
	"sync"
	"time"
)

type CompilerConfig struct {
	Script string
	Source []string
}

type HtmlCompiler struct {
	fileMonitor interface{} //*filemonitor.FileMonitor  for embed
	restartFun  func()
	source      []string
	script      string
	mutex       *sync.Mutex
	changed     bool
	changedTime time.Time
	lastErr     string
}

var HtmlCompile *HtmlCompiler

func initCompiler() {
	tmpFilename := xstring.GetFileName(os.Args[0])
	if xfile.FileExist(constvar.ApplicationPath + "/" + tmpFilename + ".compiler.json") {
		tmpFilename = constvar.ApplicationPath + "/" + tmpFilename + ".compiler.json"
	} else {
		tmpFilename = constvar.ApplicationPath + "/compiler.json"
	}
	if xfile.FileExist(tmpFilename) {
		dbg.Dbg("Load Compile file:%s\n", tmpFilename)
	}
	data, err := ioutil.ReadFile(tmpFilename)
	if err != nil {
		return
	}
	var config CompilerConfig
	err = json.Unmarshal(data, &config)
	if err != nil {
		return
	}
	if config.Script != "" && xfile.FileExist(config.Script) && len(config.Source) > 0 {
		HtmlCompile = NewHtmlCompiler(config.Source, config.Script, nil)
		HtmlCompile.Start()
	}
}

func NewHtmlCompiler(source []string, script string, restartFun func()) *HtmlCompiler {
	c := &HtmlCompiler{}
	c.restartFun = restartFun
	c.script = script
	c.source = source
	c.mutex = new(sync.Mutex)
	return c
}

func (me *HtmlCompiler) SetRestartFunc(fun func()) {
	if me == nil {
		initCompiler()
		*me = *HtmlCompile
	}
	if me == nil {
		dbg.Dbg("xhttp: init compiler failed\n")
		return
	}
	me.restartFun = fun
}

func (me *HtmlCompiler) GetLastError() string {
	return me.lastErr
}

func (me *HtmlCompiler) GetLastTime() string {
	return xtime.Time2Str(me.changedTime.Unix())
}

func (me *HtmlCompiler) Start() error {
	err := me.StartMonitor()
	go me.DoCompiler()
	return err
}

func (me *HtmlCompiler) DoCompiler() {
	for {
		me.mutex.Lock()
		tmpChange := me.changed
		tmpChangeTime := me.changedTime
		me.mutex.Unlock()
		nowTime := time.Now()
		if tmpChange && nowTime.Sub(tmpChangeTime)/1000000 > 200 {
			me.mutex.Lock()
			me.changed = false
			me.changedTime = time.Now()
			me.mutex.Unlock()
			cmd := exec.Command(me.script)
			stderr, err := cmd.StderrPipe()
			if err != nil {
				fmt.Println("+++++++++++++++++++++++++++++++++++++++++++++++++++")
				fmt.Println(err)
				fmt.Println("---------------------------------------------------")
			}
			err = cmd.Start()
			if err != nil {
				fmt.Println("+++++++++++++++++++++++++++++++++++++++++++++++++++")
				fmt.Println(err)
				fmt.Println("---------------------------------------------------")
			} else {
				var buffer bytes.Buffer
				tmpBuf := make([]byte, 1024)
				for {
					n, err := stderr.Read(tmpBuf)
					if n > 0 {
						buffer.Write(tmpBuf[:n])
					}
					if err != nil {
						break
					}
				}
				stderr.Close()
				cmd.Wait()
				b := buffer.Bytes()
				if buffer.Len() > 0 && bytes.IndexByte(b, ':') > 0 {
					me.lastErr = buffer.String()
					fmt.Println("+++++++++++++++++++++++++++++++++++++++++++++++++++")
					fmt.Println(buffer.String())
					fmt.Println("---------------------------------------------------")
					fmt.Println("Compile Failed")
				} else {
					me.lastErr = ""
					fmt.Println("Compile OK")
					if me.restartFun != nil {
						me.restartFun()
						fmt.Println("Restart OK")
					} else {
						fmt.Println("Not Restart Function")
					}

				}
			}
		}
		time.Sleep(time.Millisecond * 100)
	}
}

func (me *HtmlCompiler) SetChanged(v bool) {
	me.mutex.Lock()
	defer me.mutex.Unlock()
	me.changed = v
	me.changedTime = time.Now()
}

func (me *HtmlCompiler) IfCreate(rootDir, name string) {
	ext := xfile.GetFileExt(name)
	ext = strings.ToLower(ext)
	if ext != ".go" {
		return
	}
	me.SetChanged(true)
}

func (me *HtmlCompiler) IfModify(rootDir, name string) {
	ext := xstring.GetFileExt(name)
	ext = strings.ToLower(ext)
	if ext != ".go" {
		return
	}
	me.SetChanged(true)
}

func (me *HtmlCompiler) IfDelete(rootDir, name string) {
	ext := xstring.GetFileExt(name)
	ext = strings.ToLower(ext)
	if ext != ".go" {
		return
	}
	me.SetChanged(true)
}

func (me *HtmlCompiler) IfRename(rootDir, name string) {
	ext := xstring.GetFileExt(name)
	ext = strings.ToLower(ext)
	if ext != ".go" {
		return
	}
	me.SetChanged(true)
}
