package xhttp

import (
	"bytes"
	"encoding/binary"
	"encoding/json"
	"errors"
	"fmt"
	"gitee.com/xlm516/xtool/dbg"
	"gitee.com/xlm516/xtool/encoding"
	xid "gitee.com/xlm516/xtool/id"
	"gitee.com/xlm516/xtool/klv"
	xlist "gitee.com/xlm516/xtool/list"
	xnet "gitee.com/xlm516/xtool/net"
	"gitee.com/xlm516/xtool/sys"
	xtime "gitee.com/xlm516/xtool/time"
	"strings"
	"sync"
	"time"
)

type ECSockRPCSendQueue struct {
	header ECSockMsgHeader
	data   []byte
}

type ECSockRPCClient struct {
	host      string
	conn      *xnet.ConnEx
	connTime  int64
	bExit     bool
	mutex     *sync.Mutex
	sendQueue *xlist.Queue
	pending   *ECSockRPCSendQueue

	rpcServer  *ECRPCServer
	clientId   string
	reqCh      chan []byte
	reqChMutex sync.Mutex
	msgType    int

	OnData       func(client *ECSockRPCClient, header *ECSockMsgHeader, data []byte)
	OnKlvRequest func(klvData *klv.KLV) *klv.KLV

	encode      bool
	bConnect    bool
	isLogin     bool
	recvTimeout int32
	lastWrite   int64
	headerLen   int

	lastSendKeepalive int64
}

func NewECSockRPCClient(host string) *ECSockRPCClient {
	client := &ECSockRPCClient{}
	client.host = host
	client.recvTimeout = 15
	client.mutex = new(sync.Mutex)
	client.sendQueue = xlist.NewQueue(10000)
	client.rpcServer = NewECRPCServer()
	client.clientId = "test"

	return client
}
func (me *ECSockRPCClient) SetEncode(v bool) {
	me.encode = v
}
func (me *ECSockRPCClient) SetHost(bindHost string) {
	me.conn.SetHost(bindHost)
}
func (me *ECSockRPCClient) SetClientId(id string) {
	me.clientId = id
}
func (me *ECSockRPCClient) WriteQueue(msgType int, data []byte, dataLen int) {
	q := new(ECSockRPCSendQueue)
	q.header.MsgLen = int32(dataLen)
	q.header.MsgType = int32(msgType)
	q.data = data
	me.sendQueue.PutNoWait(q)

}

func (me *ECSockRPCClient) Write(p []byte) (int, error) {
	return me.Send(MSG_TYPE_RESPONSE, p, len(p))
}

func (me *ECSockRPCClient) Send(msgType int, data []byte, dataLen int) (int, error) {
	if me.conn == nil || me.bConnect == false {
		return 0, errors.New("send Failed")
	}
	if me.encode {
		nonce := xid.NewShortKey()
		b, err := encoding.AesEncodeByKIV(data, nonce, nil, nil, false)
		if err != nil {
			dbg.Dbg("%s\n", err.Error())
			return 0, err
		}
		data = b
		dataLen = len(data)
	}

	outBuf := new(bytes.Buffer)
	if dataLen <= 0 {
		dataLen = len(data)
	}
	binary.Write(outBuf, binary.BigEndian, int32(dataLen))
	err := binary.Write(outBuf, binary.BigEndian, int32(msgType))
	if err != nil {
		fmt.Println(err)
	}
	idBuf := make([]byte, CLIENT_ID_LEN)
	if len(me.clientId) > CLIENT_ID_LEN {
		me.clientId = me.clientId[:CLIENT_ID_LEN]
	}
	copy(idBuf, []byte(me.clientId))
	outBuf.Write(idBuf)

	if data != nil {
		if len(data) != dataLen {
			outBuf.Write(data[:dataLen])
		} else {
			outBuf.Write(data)
		}
	}
	me.lastWrite = xtime.UnixTime()
	me.conn.SetWriteDeadline(time.Now().Add(time.Second * 30))
	me.mutex.Lock()
	defer me.mutex.Unlock()
	return me.conn.Write(outBuf.Bytes())
}

func (me *ECSockRPCClient) Connect() bool {
	if me.bConnect {
		return me.bConnect
	}
	me.bConnect, _ = me.conn.Connect()
	if me.bConnect {
		me.connTime = xtime.UnixTime()
		me.bConnect = true
		dbg.Dbg("Connect [%s] OK\n", me.host)
		me.SendKeepAlive()
	} else {
		//fmt.Printf("Connect [%s] Failed,Wait Retry\n", me.host)
	}

	return me.bConnect
}

func (me *ECSockRPCClient) Close() {
	if me.bConnect {
		me.bConnect = false
		me.conn.Close()
	}
	time.Sleep(time.Second * 1)
	dbg.Dbg("Close Connect\n")
}

func (me *ECSockRPCClient) ProcessData() {
	h := me.conn.GetHeaderMsg()
	if h == nil {
		dbg.Dbg("ECMsgHeader Error\n")
		return
	}
	headerMsg, ok := h.(*ECSockMsgHeader)
	if ok == false {
		dbg.Dbg("ECMsgHeader Error2\n")
		return
	}
	data := me.conn.GetData()
	if me.encode {
		b, _, err := encoding.AesDecodeByKIV(data, nil, nil, false)
		if err != nil {
			dbg.Dbg("%s\n", err.Error())
			return
		}
		data = b
	}
	if me.OnData != nil {
		me.OnData(me, headerMsg, data)
	}
	/*
		jsonObj := make(map[string]interface{})
		err := json.Unmarshal(me.conn.GetData(), jsonObj)
		if err != nil {
			Dbg.Dbg("%s\n", err.Error())
			return
		}
		method, ok := jsonObj["method"]
		if ok {
			Dbg.Dbg("Call Method: %+v\n", method)
		} else {

		}
	*/
	//Dbg.Dbg("Process Data: header=%+v, data=%s\n", headerMsg, me.conn.GetData())
	if headerMsg.MsgType == MSG_TYPE_RESPONSE || headerMsg.MsgType == MSG_TYPE_KLV_RESPONSE {
		me.reqChMutex.Lock()
		d := make([]byte, len(data))
		copy(d, data)
		if me.reqCh != nil {
			me.reqCh <- d
			close(me.reqCh)
			me.reqCh = nil
		}
		me.reqChMutex.Unlock()
	} else if headerMsg.MsgType == MSG_TYPE_REQUEST {
		ins := NewECInstance()
		ins.SetConnWriter(me)
		jsonObj := make(map[string]interface{})
		err := json.Unmarshal(data, &jsonObj)
		if err != nil {
			dbg.Dbg("%s\n", err.Error())
			return
		}
		me.msgType = MSG_TYPE_RESPONSE
		_, err = me.rpcServer.Call(ins, nil, jsonObj)
		if err != nil {
			dbg.Dbg("%s,Data: %+v\n", err.Error(), jsonObj)
			return
		}
		buf := ins.GetOutBuffer()
		if len(buf) > 0 {
			_, _ = me.Send(MSG_TYPE_RESPONSE, buf, len(buf))
		}
	} else if headerMsg.MsgType == MSG_TYPE_KLV_REQUEST {
		klvData := klv.NewKLV()
		err := klvData.Parse(data)
		if err != nil {
			dbg.Dbg("%s\n", err.Error())
			return
		}
		me.msgType = MSG_TYPE_KLV_RESPONSE
		if me.OnKlvRequest != nil {
			tmpKlv := me.OnKlvRequest(klvData)
			buf := new(bytes.Buffer)
			tmpKlv.Export(buf)
			_, _ = me.Send(MSG_TYPE_KLV_RESPONSE, buf.Bytes(), 0)
		}
	}
}

func (me *ECSockRPCClient) readLoop(args []interface{}, procInfo *sys.ProcInfo) {
	if me.bExit {
		procInfo.Exit(1)
		return
	}
	if me.Connect() == false {
		time.Sleep(time.Second * 5)
		return
	}
	ret, err, timeout := me.conn.ReadEx()
	if ret < 0 {
		dbg.Dbg("Error: %s\n", err.Error())
		me.Close()
		return
	}
	if ret > 0 {
		me.ProcessData()
	}
	if ret == 0 && timeout {
		if xtime.CompTime(xtime.UnixTime(), me.conn.GetLastUpdate()) >= int64(me.recvTimeout) {
			//Dbg.Dbg("Client receive timeout=%d\n", me.recvTimeout)
			if xtime.CompTime(xtime.UnixTime(), me.lastSendKeepalive) >= 3 {
				me.SendKeepAlive()
				me.lastSendKeepalive = xtime.UnixTime()
			}
		}
		if xtime.CompTime(xtime.UnixTime(), me.conn.GetLastUpdate()) >= int64(me.recvTimeout*3) {
			dbg.Dbg("close by timeout=%d\n", me.recvTimeout)
			me.Close()
		}
	}
	return
}

func (me *ECSockRPCClient) writeLoop(args []interface{}, procInfo *sys.ProcInfo) {
	if me.bExit {
		procInfo.Exit(1)
		return
	}
	time.Sleep(time.Millisecond * 50)
	if me.bConnect == false {
		return
	}
	for {
		if me.pending != nil {
			_, err := me.Send(int(me.pending.header.MsgType), me.pending.data,
				len(me.pending.data))
			if err == nil {
				me.pending = nil
				break
			}
		}

		v, err := me.sendQueue.GetNoWait()
		if err != nil {
			break
		}
		sndQ, ok := v.(*ECSockRPCSendQueue)
		if ok == false {
			break
		}
		_, err = me.Send(int(sndQ.header.MsgType), sndQ.data, len(sndQ.data))
		if err != nil {
			dbg.Dbg("%s\n", err.Error())
			me.pending = sndQ
			me.Close()
		}
	}
	time.Sleep(time.Millisecond * 500)
}

func (me *ECSockRPCClient) Start() error {
	me.bConnect = false
	me.bExit = false
	me.headerLen = 4

	index := strings.Index(me.host, ":")
	if index <= 0 {
		me.host += ":7789"
	}
	me.conn, _ = xnet.NewTcpClient(me.host, 5000)
	me.conn.SetRecvTimeout(5000)
	me.conn.SetSendTimeout(30000)
	me.conn.SetHeaderLen(8 + CLIENT_ID_LEN)
	me.conn.SetIf(me)
	sys.Process.CreateProc("ECSocketClient", me.readLoop, nil)
	sys.Process.CreateProc("ECSocketClient", me.writeLoop, nil)
	return nil
}

func (me *ECSockRPCClient) ParseHeader(b []byte) (dataLen int, header interface{}) {
	var msgLen int32
	var msgType int32
	rd := bytes.NewReader(b)
	err := binary.Read(rd, binary.BigEndian, &msgLen)
	if err != nil {
		fmt.Println(err)
		return
	}

	err = binary.Read(rd, binary.BigEndian, &msgType)
	if err != nil {
		fmt.Println(err)
		return
	}

	msgHeader := &ECSockMsgHeader{}
	msgHeader.MsgLen = msgLen
	msgHeader.MsgType = msgType
	dataLen = int(msgLen)
	copy(msgHeader.ClientId, b[8:])
	header = msgHeader
	return
}

func (me *ECSockRPCClient) SendKeepAlive() {
	data := make(map[string]interface{})
	data["Data"] = "hello"
	b, _ := json.Marshal(data)
	if b == nil {
		b = make([]byte, 0)
	}
	_, _ = me.Send(MSG_TYPE_KEEPALIVE, b, 0)
}

func (me *ECSockRPCClient) Call(method string, args ...interface{}) ([]interface{}, error) {
	return me._Call(nil, method, args...)
}
func (me *ECSockRPCClient) Call2(output interface{}, method string, args ...interface{}) error {
	_, err := me._Call(output, method, args...)
	return err
}
func (me *ECSockRPCClient) _Call(output interface{}, method string, args ...interface{}) ([]interface{}, error) {
	me.reqChMutex.Lock()
	if me.reqCh != nil {
		me.reqChMutex.Unlock()
		return nil, errors.New("requesting....")
	}
	rpcClient := NewECRPCClient("")
	reqData, err := rpcClient.BuildRequestData(method, args...)
	if err != nil {
		me.reqChMutex.Unlock()
		return nil, err
	}
	me.reqCh = make(chan []byte, 1)
	ch := me.reqCh
	_, err = me.Send(MSG_TYPE_REQUEST, reqData, len(reqData))
	if err != nil {
		close(me.reqCh)
		me.reqCh = nil
		me.reqChMutex.Unlock()
		return nil, err
	}
	me.reqChMutex.Unlock()

	var retData []byte
	select {
	case retData = <-ch:
		break
	case <-time.After(time.Second * time.Duration(me.recvTimeout)):
		dbg.Dbg("do request fatal error\n")
		return nil, errors.New("request timeout")
	}
	if retData != nil {
		rpcClient.ProcessJson(output, retData)
		return rpcClient.GetResult(), nil
	} else {
		return nil, errors.New("invalid response data")
	}
}

func (me *ECSockRPCClient) CallKLV(klvData *klv.KLV) (*klv.KLV, error) {
	me.reqChMutex.Lock()
	if me.reqCh != nil {
		me.reqChMutex.Unlock()
		return nil, errors.New("requesting")
	}
	buf := new(bytes.Buffer)
	klvData.Export(buf)
	me.reqCh = make(chan []byte, 1)
	ch := me.reqCh
	_, err := me.Send(MSG_TYPE_KLV_REQUEST, buf.Bytes(), 0)
	if err != nil {
		close(me.reqCh)
		me.reqCh = nil
		me.reqChMutex.Unlock()
		return nil, err
	}
	me.reqChMutex.Unlock()

	var retData []byte
	select {
	case retData = <-ch:
		break
	case <-time.After(time.Second * time.Duration(me.recvTimeout)):
		dbg.Dbg("Do Request fatal error\n")
		return nil, errors.New("request timeout")
	}
	if retData != nil {
		tmpKlv := klv.NewKLV()
		err := tmpKlv.Parse(retData)
		if err != nil {
			dbg.Dbg("%s\n", err.Error())
			return nil, err
		}
		return tmpKlv, nil
	} else {
		return nil, errors.New("invalid response data")
	}
}

func (me *ECSockRPCClient) AddFunc(name string, fun interface{}, level int) error {
	return me.rpcServer.AddFunc(name, fun, level)
}
