module gitee.com/xlm516/http

go 1.21

require (
	gitee.com/xlm516/xtool v0.0.0-20241227125932-0551d684c197
	github.com/gorilla/websocket v1.5.3
	github.com/valyala/fasthttp v1.57.0
	golang.org/x/net v0.32.0
)

require (
	github.com/andybalholm/brotli v1.1.1 // indirect
	github.com/bwmarrin/snowflake v0.3.0 // indirect
	github.com/codingsince1985/checksum v1.3.0 // indirect
	github.com/duke-git/lancet/v2 v2.3.3 // indirect
	github.com/fsnotify/fsnotify v1.8.0 // indirect
	github.com/iancoleman/orderedmap v0.3.0 // indirect
	github.com/klauspost/compress v1.17.11 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	golang.org/x/crypto v0.30.0 // indirect
	golang.org/x/exp v0.0.0-20221208152030-732eee02a75a // indirect
	golang.org/x/sys v0.28.0 // indirect
	golang.org/x/text v0.21.0 // indirect
)
