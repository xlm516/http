package xhttp

import (
	"gitee.com/xlm516/xtool/cast"
	xsync "gitee.com/xlm516/xtool/sync"
	"sync"
)

const (
	EC_JSON_NOT_NEGO = -101
)

const (
	JRPC_ERR_PARSE            = -32700
	JRPC_ERR_INVALID_REQUEST  = -32600
	JRPC_ERR_NOT_FOUND        = -32601
	JRPC_ERR_INVALID_ARGUMENT = -32602
	JPRC_ERR_IN_ERROR         = -32603
	/* vendor define */
	JRPC_CALL_OK       = 0
	JRPC_ERR_NOT_LOGIN = -32000
)

const (
	SECURITY_NULL = 0 /* 不采用安全模式 */
	/*
		必须设置回调函数：onAuth(req *xhttp.NegoRequest) (*xhttp.NegoUser, bool)
		返回协商结果，并且返回true
		例如：
		func onAuth(req *xhttp.NegoRequest) (*xhttp.NegoUser, bool) {
			nego := new(xhttp.NegoUser)
			nego.Mode = xhttp.SECURITY_NEGO_ZIP_3DES
			return nego, true
		}
	*/
	SECURITY_NEGO_ZIP_3DES = 1 /* 协商模式*/
	/*
	   客户端和服务器固定私钥模式加密
	*/
	SECURITY_HTTP_AES_KIV = 2 /* 客户端请求固定安全模式 */

)

const (
	ERROR_BASE     = 1000212
	ERROR_EXIT     = 0
	ERROR_GENEN    = 1
	ERROR_DIE_LOOP = 3
)

const (
	PRI_SUPER      = 1
	PRI_ADMIN_USER = 2
)

const (
	VM_ERROR_INVLIAD_DB  = 10001
	VM_ERROR_EXIST       = 10002
	VM_ERROR_NOT_EXIST   = 10003
	VM_ERROR_UNLOGINED   = 10004
	VM_ERROR_INVALID_SQL = 10005
)

const TB_username = "_user"
const TB_userPri = "_userpri"
const TB_loginLog = "_loginlog"

const Login_Session = "_login_"
const TB_RECORD_SQL = "_recordsql"

type UsersInfo struct {
	Token         string
	SessionStr    string //cookie session
	UserId        string
	Username      string
	Nickname      string
	Password      string
	Disabled      string
	Super         bool
	UserType      string
	DepartId      string
	DepartName    string
	WorkName      string
	Argument      map[string]interface{}
	ExpireTime    string
	UserPriMap    map[int]int
	UserPri       []int
	UserPriName   []string
	LastLoginTime string
	LastLoginIP   string
	CreateTime    string
	UpdateTime    int64
	Timeout       int64
	Memo          string
	UserData      sync.Map
}

type UsersInfoSlice []*UsersInfo

func (m UsersInfoSlice) Len() int {
	return len(m)
}
func (m UsersInfoSlice) Swap(i, j int) {
	m[i], m[j] = m[j], m[i]
}

// return false 交换
func (m UsersInfoSlice) Less(i, j int) bool {
	if m[i].Username < m[j].Username {
		return true
	} else if m[i].Username == m[j].Username && m[i].UpdateTime < m[j].UpdateTime {
		return true
	}
	return false
}

type UserPri struct {
	Id         int64  `dbName:"id" dbType:"bigint(20) NOT NULL AUTO_INCREMENT" dbKey:"1"`
	Ts         string `dbName:"ts" dbType:"timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP" dbKey:"1"`
	Del        int    `dbName:"del" dbKey:"1"`
	UserId     string
	PriId      string
	Pri        int
	Super      bool
	CreateTime int64
	Memo       string
}

var LoginMutex sync.Mutex
var IsCheckLogin cast.Bool
var DefaultDBHandler string
var LoginMapMutex sync.Mutex
var LoginMapUser map[string]*UsersInfo
var LoginMapSession map[string]*UsersInfo
var GlobalVMVar sync.Map

/*
var VMDBMutex sync.Mutex
var VMDBList map[string]*VMDB = make(map[string]*VMDB)
var RecordSqlDelete atom.Bool

func VMGetDB(handle string) *VMDB {
	VMDBMutex.Lock()
	defer VMDBMutex.Unlock()
	db, ok := VMDBList[handle]
	if ok {
		return db
	} else {
		return nil
	}
}
*/

type TimerInfo struct {
	Name     string
	JsFile   string
	RootDir  string
	Interval int64
	LastCall int64
}

var VMUserData map[string]interface{} = make(map[string]interface{})
var VMUserDataMutex *xsync.MutexEx
var VMTimerMutex sync.Mutex
var VMTimer map[string]*TimerInfo = make(map[string]*TimerInfo)
