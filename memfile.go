package xhttp

import (
	"bytes"
	"errors"
	xfile "gitee.com/xlm516/xtool/file"
	"gitee.com/xlm516/xtool/stream"
	xtime "gitee.com/xlm516/xtool/time"
	"sync"
)

type MemoryFiles struct {
	FileName   string
	Buffer     stream.BufferEx
	CreateTime int64
	UsedTime   int64
	Reload     bool
}

type MemoryFileMnmg struct {
	MemoryFile map[string]*MemoryFiles
	mutex      *sync.Mutex
}

func NewMemoryFileMnmg() *MemoryFileMnmg {
	m := &MemoryFileMnmg{}
	m.MemoryFile = make(map[string]*MemoryFiles)
	m.mutex = new(sync.Mutex)

	return m
}

func (me *MemoryFileMnmg) Lock() {
	me.mutex.Lock()
}

func (me *MemoryFileMnmg) Unlock() {
	me.mutex.Unlock()
}

func (me *MemoryFileMnmg) GetFile(fileName string) (bytes.Buffer, error) {
	me.Lock()
	defer me.Unlock()
	var b bytes.Buffer
	memFile, ok := me.MemoryFile[fileName]
	if ok {
		b.Write(memFile.Buffer.Bytes())
		return b, nil
	} else {
		return b, errors.New("Not found")
	}
}

func (me *MemoryFileMnmg) PutFile(fileName string, b bytes.Buffer) {
	me.Lock()
	defer me.Unlock()
	memFile, ok := me.MemoryFile[fileName]
	if ok {
		memFile.Buffer.Reset()
		memFile.Buffer.Write(b.Bytes())
		memFile.UsedTime = xtime.UnixTime()
	} else {
		memFile := &MemoryFiles{}
		memFile.Buffer.Write(b.Bytes())
		memFile.CreateTime = xtime.UnixTime()
		memFile.UsedTime = xtime.UnixTime()
		me.MemoryFile[fileName] = memFile
	}
}

func (me *MemoryFileMnmg) LoadFile(fileName string) error {
	me.Lock()
	defer me.Unlock()
	fh, err := xfile.OpenRead(fileName)
	if err != nil {
		return err
	}
	defer fh.Close()

	memFile, ok := me.MemoryFile[fileName]
	if ok {
		memFile.Buffer.Reset()
		memFile.UsedTime = xtime.UnixTime()
		memFile.CreateTime = xtime.UnixTime()
	} else {
		memFile = &MemoryFiles{}
		memFile.CreateTime = xtime.UnixTime()
		memFile.UsedTime = xtime.UnixTime()
		me.MemoryFile[fileName] = memFile
	}

	var b []byte = make([]byte, 4096)
	for {
		n, err := fh.Read(b)
		if n > 0 {
			memFile.Buffer.Write(b[:n])
		}
		if err != nil {
			break
		}
	}

	return nil
}
