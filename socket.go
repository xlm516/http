package xhttp

import (
	"bytes"
	"encoding/binary"
	"errors"
	"fmt"
	"gitee.com/xlm516/xtool/cast"
	"gitee.com/xlm516/xtool/dbg"
	xlist "gitee.com/xlm516/xtool/list"
	xnet "gitee.com/xlm516/xtool/net"
	"gitee.com/xlm516/xtool/sys"
	xtime "gitee.com/xlm516/xtool/time"
	"strings"
	"sync"
	"time"
)

const (
	MsgNormal = 1
)

type ECMsgHeader struct {
	msgLen  int32
	msgType int32
}

type ECSocketConn struct {
	*xnet.MySrvClient
	header     *ECMsgHeader
	sockServer *ECSocketServer
}

type ECSocketServer struct {
	*xnet.MyServer
	router   *ECRouterMgr
	bindHost string
	bExit    bool
}

func NewECSocketServer(bindHost string) *ECSocketServer {
	srv := new(ECSocketServer)
	srv.bindHost = bindHost
	srv.init()
	return srv
}

// /////////////////////////////////////////////////////////////////
func (me *ECSocketConn) Write(b []byte) (int, error) {
	v := me.SendNoHeader(MsgNormal, b, len(b))
	if v {
		return len(b), nil
	} else {
		return 0, errors.New("writer failed")
	}
}

///////////////////////////////////////////////////////////////////

func (me *ECSocketServer) init() {
	me.router = NewECRouterMgr()
}

func (me *ECSocketServer) GetRouter() *ECRouterMgr {
	return me.router
}

func (me *ECSocketServer) GetBind() string {
	return me.bindHost
}

func (me *ECSocketServer) GetBindPort() int {
	port := int(80)
	index := strings.IndexByte(me.bindHost, ':')
	if index >= 0 {
		port = cast.ToIntValue(me.bindHost[index+1:])
	}
	return port
}

func (me *ECSocketServer) ParseHeader(b []byte) (int, interface{}) {
	var msgLen int32
	var msgType int32
	msgHeader := &ECMsgHeader{}
	rd := bytes.NewReader(b)
	err := binary.Read(rd, binary.BigEndian, &msgLen)
	if err != nil {
		fmt.Println(err)
		return 0, msgHeader
	}

	err = binary.Read(rd, binary.BigEndian, &msgType)
	if err != nil {
		fmt.Println(err)
		return 0, msgHeader
	}

	msgHeader.msgLen = msgLen
	msgHeader.msgType = msgType
	dataLen := msgLen

	return int(dataLen), msgHeader
}

func (me *ECSocketServer) OnAccept(client *xnet.MySrvClient) bool {
	//me.cfg.Load("")
	c := &ECSocketConn{}
	c.MySrvClient = client
	c.sockServer = me
	client.SetUserData(c)
	if client.GetConn() != nil {
		client.GetConn().SetIf(me)
	}
	return true
}
func (me *ECSocketServer) OnClose(client *xnet.MySrvClient) {
	client.SetUserData(nil)
}

func (me *ECSocketServer) OnRequest(client *xnet.MySrvClient, data []byte) bool {
	conn := client.GetConn()
	if conn == nil {
		return false
	}
	h := conn.GetHeaderMsg()
	if h == nil {
		return false
	}
	msgHeader, ok := h.(*ECMsgHeader)
	if ok == false {
		dbg.Dbg("Get MsgHeader Failed\n")
		return false
	}
	clientConn := me.GetECSocketConn(client)
	if clientConn == nil {
		dbg.Dbg("client connection is null\n")
		return false
	}
	clientConn.header = msgHeader
	me.socketRouter(clientConn, data)
	return true
}

func (me *ECSocketServer) Start() (bool, error) {
	me.MyServer = xnet.NewMyServer(me.bindHost)
	if me.MyServer != nil {
		me.ServerName = "ECSocketServer"
		me.MyServer.SetInterface(me)
		return me.MyServer.Start()
	}

	return false, nil
}

func (me *ECSocketServer) GetECSocketConn(client *xnet.MySrvClient) *ECSocketConn {
	d := client.GetUserData()
	if d == nil {
		return nil
	}

	conn, ok := d.(*ECSocketConn)
	if ok {
		return conn
	} else {
		return nil
	}
}

func (me *ECSocketServer) socketRouter(conn *ECSocketConn, data []byte) {
	me.router.SockRouter(conn, data)
}

type ECSockClientSendQueue struct {
	header ECMsgHeader
	data   []byte
}

type ECSocketClient struct {
	host      string
	conn      *xnet.ConnEx
	connTime  int64
	bExit     bool
	mutex     *sync.Mutex
	sendQueue *xlist.Queue
	pending   *ECSockClientSendQueue

	OnData func(client *ECSocketClient, header *ECMsgHeader, data []byte)

	encodeMode  int
	bConnect    bool
	isLogin     bool
	recvTimeout int32
	lastWrite   int64
	headerLen   int
}

func (me *ECSocketClient) SetHost(bindHost string) {
	me.conn.SetHost(bindHost)
}

func (me *ECSocketClient) WriteQueue(msgType int, data []byte, dataLen int) {
	q := new(ECSockClientSendQueue)
	q.header.msgLen = int32(dataLen)
	q.header.msgType = int32(msgType)
	q.data = data
	me.sendQueue.PutNoWait(q)

}
func (me *ECSocketClient) Send(msgType int, data []byte, dataLen int) (int, error) {
	outBuf := new(bytes.Buffer)

	if dataLen <= 0 {
		dataLen = len(data)
	}

	binary.Write(outBuf, binary.BigEndian, int32(dataLen))
	err := binary.Write(outBuf, binary.BigEndian, int32(msgType))
	if err != nil {
		fmt.Println(err)
	}
	if data != nil {
		if len(data) != dataLen {
			outBuf.Write(data[:dataLen])
		} else {
			outBuf.Write(data)
		}
	}
	me.lastWrite = xtime.UnixTime()
	me.conn.SetWriteDeadline(time.Now().Add(time.Second * 30))
	me.mutex.Lock()
	defer me.mutex.Unlock()
	return me.conn.Write(outBuf.Bytes())
}

func (me *ECSocketClient) Connect() bool {
	if me.bConnect {
		return me.bConnect
	}
	me.bConnect, _ = me.conn.Connect()
	if me.bConnect {
		me.connTime = xtime.UnixTime()
		me.bConnect = true
		dbg.Dbg("Connect [%s] OK\n", me.host)
	} else {
		//fmt.Printf("Connect [%s] Failed,Wait Retry\n", me.host)
	}

	return me.bConnect
}

func (me *ECSocketClient) Close() {
	if me.bConnect {
		me.bConnect = false
		me.conn.Close()
	}
	time.Sleep(time.Second * 1)
	dbg.Dbg("Close Connect\n")
}

func (me *ECSocketClient) ProcessData() {
	h := me.conn.GetHeaderMsg()
	if h == nil {
		dbg.Dbg("ECMsgHeader Error\n")
		return
	}
	headerMsg, ok := h.(*ECMsgHeader)
	if ok == false {
		dbg.Dbg("ECMsgHeader Error2\n")
		return
	}

	//fmt.Println(headerMsg)
	if me.OnData != nil {
		me.OnData(me, headerMsg, me.conn.GetData())
	}
}

func (me *ECSocketClient) readLoop(args []interface{}, procInfo *sys.ProcInfo) {
	if me.bExit {
		procInfo.Exit(1)
		return
	}
	if me.Connect() == false {
		time.Sleep(time.Second * 5)
		return
	}

	ret, err, timeout := me.conn.ReadEx()
	if ret < 0 {
		fmt.Println("Error: ", err)
		me.Close()
		return
	}

	if ret > 0 {
		me.ProcessData()
	}

	if ret == 0 && timeout {
		if xtime.CompTime(xtime.UnixTime(), me.conn.GetLastUpdate()) >= int64(me.recvTimeout) {
			dbg.Dbg("client receive timeout=%d\n", me.recvTimeout)
		}
	}
	return
}

func (me *ECSocketClient) writeLoop(args []interface{}, procInfo *sys.ProcInfo) {
	if me.bExit {
		procInfo.Exit(1)
		return
	}
	if me.bConnect == false {
		return
	}
	for {
		if me.pending != nil {
			_, err := me.Send(int(me.pending.header.msgType), me.pending.data,
				len(me.pending.data))
			if err == nil {
				me.pending = nil
				break
			}
		}

		v, err := me.sendQueue.GetNoWait()
		if err != nil {
			break
		}
		sndQ, ok := v.(*ECSockClientSendQueue)
		if ok == false {
			break
		}
		_, err = me.Send(int(sndQ.header.msgType), sndQ.data, len(sndQ.data))
		if err != nil {
			me.pending = sndQ
			me.Close()
		}
	}
}

func (me *ECSocketClient) Start() error {
	me.bConnect = false
	me.bExit = false
	me.headerLen = 4

	index := strings.Index(me.host, ":")
	if index <= 0 {
		me.host += ":7789"
	}
	me.conn, _ = xnet.NewTcpClient(me.host, 5000)
	me.conn.SetRecvTimeout(5000)
	me.conn.SetSendTimeout(30000)
	me.conn.SetHeaderLen(8)
	me.conn.SetIf(me)
	sys.Process.CreateProc("ECSocketClient", me.readLoop, nil)
	sys.Process.CreateProc("ECSocketClient", me.writeLoop, nil)
	return nil
}

func (me *ECSocketClient) ParseHeader(b []byte) (dataLen int, header interface{}) {
	var msgLen int32
	var msgType int32
	rd := bytes.NewReader(b)
	err := binary.Read(rd, binary.BigEndian, &msgLen)
	if err != nil {
		fmt.Println(err)
		return
	}

	err = binary.Read(rd, binary.BigEndian, &msgType)
	if err != nil {
		fmt.Println(err)
		return
	}

	msgHeader := &ECMsgHeader{}
	msgHeader.msgLen = msgLen
	msgHeader.msgType = msgType
	dataLen = int(msgLen)
	header = msgHeader

	return
}

func NewECSocketClient(host string) *ECSocketClient {
	client := &ECSocketClient{}
	client.host = host
	client.recvTimeout = 35
	client.mutex = new(sync.Mutex)
	client.sendQueue = xlist.NewQueue(10000)

	return client
}
