package xhttp

import (
	"encoding/binary"
	"encoding/json"
	"errors"
	"gitee.com/xlm516/xtool/dbg"
	"gitee.com/xlm516/xtool/encoding"
	xfile "gitee.com/xlm516/xtool/file"
	xstring "gitee.com/xlm516/xtool/string"
	xtime "gitee.com/xlm516/xtool/time"
	"io"
	"os"
	"strings"
	"sync"
	"time"
)

const (
	MemMagic = 0x11BBCCDD
)

type FilePackHeader struct {
	Magic     int32
	HeaderLen byte
	Ver       byte
	Zip       byte
	Encode    byte
}

type MemFile struct {
	Data     []byte `json:"-"`
	Filename string
	Size     int64
	ModTime  int64
	Option   int32
}

type MemFileSys struct {
	Filename   string
	mutex      sync.Mutex
	files      map[string]*MemFile
	baseDir    string
	prefix     string
	isZip      bool
	isEncode   bool
	fileExt    string
	excludeExt string
	aesKey     []byte
	isDebug    bool
}

func NewMemFileSys() *MemFileSys {
	memfile := new(MemFileSys)
	memfile.init()
	return memfile
}

func (me *MemFileSys) init() {
	me.files = make(map[string]*MemFile)
	me.aesKey = []byte{23, 32, 12, 3, 87, 21, 32, 43, 11, 44, 67, 32, 43, 43, 45, 6, 8, 2, 14}
}

func (me *MemFileSys) SetDirArgument(baseDir, prefix string) {
	me.baseDir = baseDir
	me.prefix = prefix
}

func (me *MemFileSys) SetFileExt(ext string) {
	me.fileExt = ext
}

func (me *MemFileSys) SetExcludeFileExt(ext string) {
	me.excludeExt = strings.ToLower(ext)
}

func (me *MemFileSys) SetEncoding(zip bool, encode bool) {
	me.isZip = zip
	me.isEncode = encode
}

func (me *MemFileSys) Get(filename string) *MemFile {
	filename = strings.TrimLeft(filename, "./\\")
	f, ok := me.files[filename]
	if ok == false {
		filename = strings.ReplaceAll(filename, "\\", "/")
		f, ok = me.files[filename]
	}
	if ok {
		return f
	} else {
		return nil
	}
}

func (me *MemFileSys) Add(filename string, src []byte) {
	item := new(MemFile)
	item.Filename = filename
	item.Data = src
	item.Size = int64(len(src))
	item.ModTime = xtime.UnixTime()
	me.mutex.Lock()
	me.files[filename] = item
	me.mutex.Unlock()
}

func (me *MemFileSys) Read(filename string) ([]byte, error) {
	filename = strings.TrimLeft(filename, "./\\")
	f, ok := me.files[filename]
	if ok == false {
		return nil, errors.New("not find")
	}
	return f.Data, nil
}

func (me *MemFileSys) writerHeader(w io.Writer) {
	binary.Write(w, binary.BigEndian, int32(MemMagic))
	w.Write([]byte{3})
	w.Write([]byte{1})
	if me.isZip {
		w.Write([]byte{1})
	} else {
		w.Write([]byte{0})
	}
	if me.isEncode {
		w.Write([]byte{1})
	} else {
		w.Write([]byte{0})
	}
}

func (me *MemFileSys) compressFile(filename, saveFilename string, f *xfile.FileInfo, ioWriter io.Writer) error {
	b, err := xfile.ReadFile(filename)
	if err != nil {
		return err
	}
	fileData := b
	if me.isZip {
		fileData = encoding.Zip(b)
	}
	if me.isEncode {
		tmpB, err := encoding.AesEncrypt(fileData, me.aesKey)
		if err != nil {
			dbg.Dbg("Error: %s\n", err.Error())
		}
		if tmpB != nil {
			fileData = tmpB
		}
	}
	memFile := new(MemFile)
	memFile.ModTime = f.ModTime.Unix()
	saveFilename = strings.Replace(saveFilename, "\\", "/", -1)
	memFile.Data = fileData
	memFile.Filename = saveFilename
	memFile.Size = int64(len(fileData))
	headerBuf, err := json.Marshal(memFile)
	if err != nil {
		return err
	}
	if me.isEncode {
		tmpB, err := encoding.AesEncrypt(headerBuf, me.aesKey)
		if err != nil {
			dbg.Dbg("Error: %s\n", err.Error())
		}
		if tmpB != nil {
			headerBuf = tmpB
		}
	}
	headerLen := len(headerBuf)
	//Dbg.Dbg("Compress header: %d, dataLen:%d\n", headerLen, memFile.Size)
	binary.Write(ioWriter, binary.BigEndian, int32(MemMagic))
	binary.Write(ioWriter, binary.BigEndian, int32(headerLen))
	ioWriter.Write(headerBuf)
	ioWriter.Write(memFile.Data)
	dbg.Dbg("Compress file %s ok\n", saveFilename)
	return nil
}

func (me *MemFileSys) compressBuffer(saveFilename string, buf []byte, f *xfile.FileInfo, ioWriter io.Writer) error {
	fileData := buf
	if me.isZip {
		fileData = encoding.Zip(buf)
	}
	if me.isEncode {
		tmpB, err := encoding.AesEncrypt(fileData, me.aesKey)
		if err != nil {
			dbg.Dbg("Error: %s\n", err.Error())
		}
		if tmpB != nil {
			fileData = tmpB
		}
	}
	memFile := new(MemFile)
	memFile.ModTime = f.ModTime.Unix()
	saveFilename = strings.Replace(saveFilename, "\\", "/", -1)
	memFile.Data = fileData
	memFile.Filename = saveFilename
	memFile.Size = int64(len(fileData))
	headerBuf, err := json.Marshal(memFile)
	if err != nil {
		return err
	}
	if me.isEncode {
		tmpB, err := encoding.AesEncrypt(headerBuf, me.aesKey)
		if err != nil {
			dbg.Dbg("Error: %s\n", err.Error())
		}
		if tmpB != nil {
			headerBuf = tmpB
		}
	}
	headerLen := len(headerBuf)
	//Dbg.Dbg("Compress header: %d, dataLen:%d\n", headerLen, memFile.Size)
	binary.Write(ioWriter, binary.BigEndian, int32(MemMagic))
	binary.Write(ioWriter, binary.BigEndian, int32(headerLen))
	ioWriter.Write(headerBuf)
	ioWriter.Write(memFile.Data)
	dbg.Dbg("Compress file %s ok\n", saveFilename)
	return nil
}

func (me *MemFileSys) compress(rootDir string, w io.Writer) error {
	_, files, err := xfile.FindFileLimit(rootDir, me.fileExt, 4)
	if err != nil {
		dbg.Dbg("find file error: %s\n", err.Error())
		return err
	}
	excludeMap := make(map[string]int)
	items := strings.Split(me.excludeExt, " ")
	for _, v := range items {
		if v == "" {
			continue
		}
		excludeMap[v] = 1
	}
	for _, v := range files {
		if len(excludeMap) > 0 {
			ext := strings.ToLower(xstring.GetFileExt(v.Filename))
			_, ok := excludeMap[ext]
			if ok {
				continue
			}
		}
		saveFilename := v.FullName[len(rootDir)+1:]
		saveFilename = strings.ReplaceAll(saveFilename, "\\", "/")
		dbg.Dbg("start compile file: %s > %s\n", v.FullName, saveFilename)
		err = me.compressFile(v.FullName, saveFilename, v, w)
		if err != nil {
			dbg.Dbg("compress file error: %s\n", v.FullName)
			return err
		}
	}
	return err
}

func (me *MemFileSys) CompressToFile(rootDir string, filename string) error {
	rootDir = strings.TrimRight(rootDir, "/\\")
	fh, err := xfile.OpenFileEx(filename, os.O_CREATE|os.O_RDWR|os.O_TRUNC)
	if err != nil {
		return err
	}
	me.writerHeader(fh)
	for _, v := range me.files {
		fileInfo := xfile.FileInfo{}
		fileInfo.ModTime = time.Now()
		fileInfo.Filename = v.Filename
		fileInfo.Size = int64(len(v.Data))
		me.compressBuffer(v.Filename, v.Data, &fileInfo, fh)
	}
	defer fh.Close()
	return me.compress(rootDir, fh)
}

func (me *MemFileSys) decompress(rd io.Reader) error {
	me.files = make(map[string]*MemFile)
	header := new(FilePackHeader)
	binary.Read(rd, binary.BigEndian, &header.Magic)
	tmpBuf := make([]byte, 1)
	_, err := rd.Read(tmpBuf)
	if err != nil {
		return err
	}
	header.HeaderLen = tmpBuf[0]

	_, err = rd.Read(tmpBuf)
	if err != nil {
		return err
	}
	header.Ver = tmpBuf[0]

	_, err = rd.Read(tmpBuf)
	if err != nil {
		return err
	}
	header.Zip = tmpBuf[0]

	_, err = rd.Read(tmpBuf)
	if err != nil {
		return err
	}
	header.Encode = tmpBuf[0]

	for {
		magic := int32(0)
		dataLen := int32(0)
		err := binary.Read(rd, binary.BigEndian, &magic)
		if err != nil {
			break
		}
		if magic != MemMagic {
			dbg.Dbg("Magic: %X\n", magic)
			return errors.New("invalid magic")
		}
		err = binary.Read(rd, binary.BigEndian, &dataLen)
		if err != nil {
			dbg.Dbg("Error: %s\n", err.Error())
			return err
		}
		headerBuf := make([]byte, dataLen)
		_, err = rd.Read(headerBuf)
		if err != nil {
			dbg.Dbg("Error: %s\n", err.Error())
			return err
		}
		if header.Encode == 1 {
			retBuf, err := encoding.AesDecrypt(headerBuf, me.aesKey)
			if err == nil {
				headerBuf = retBuf
			}
		}
		fileInfo := new(MemFile)
		err = json.Unmarshal(headerBuf, fileInfo)
		if err != nil {
			dbg.Dbg("Error: %s\n", err.Error())
			return err
		}
		if fileInfo.Size > 0 {
			fileInfo.Data = make([]byte, fileInfo.Size)
			_, err = rd.Read(fileInfo.Data)
			if err != nil {
				dbg.Dbg("Error: %s\n", err.Error())
				return err
			}
			if header.Encode == 1 {
				retBuf, err := encoding.AesDecrypt(fileInfo.Data, me.aesKey)
				if err == nil {
					fileInfo.Data = retBuf
				}
			}
			if header.Zip == 1 {
				retBuf, err := encoding.UnzipByte2(fileInfo.Data)
				if err == nil {
					fileInfo.Data = retBuf
				}
			}
		}
		me.files[fileInfo.Filename] = fileInfo
		if me.isDebug {
			dbg.Dbg("load file: %s, size:%d\n", fileInfo.Filename, fileInfo.Size)
		}
	}
	return nil
}

func (me *MemFileSys) DecompressFile(filename string) error {
	me.Filename = filename
	fh, err := xfile.OpenFileEx(filename, os.O_RDWR)
	if err != nil {
		return err
	}
	defer fh.Close()
	return me.decompress(fh)
}
