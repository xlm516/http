//go:build GOHTTP_SERVER
// +build GOHTTP_SERVER

package xhttp

import (
	"encoding/json"
	"gitee.com/xlm516/xtool/cast"
	"gitee.com/xlm516/xtool/dbg"
	xlist "gitee.com/xlm516/xtool/list"
	"github.com/gorilla/websocket"
	"net/http"
	"time"
)

var (
	upGrader = websocket.Upgrader{
		// 允许跨域
		CheckOrigin: func(r *http.Request) bool {
			return true
		},
	}
)

func init() {
	goWsEchoHandle = _goWsEchoHandle
	goWsHandle = _goWsHandle
}

func _goWsEchoHandle(ins *ECInstance) {
	var wsConn *websocket.Conn
	var err error
	w, r, err := ins.GetHttpCtx().ToGoHttpHandle()
	if err != nil {
		dbg.Dbg("wsEchoHandle Error: %s\n", err.Error())
		return
	}
	wsConn, err = upGrader.Upgrade(w, r, nil)
	if err != nil {
		dbg.Dbg("wsEchoHandle Error: %s\n", err.Error())
		return
	}
	remoteAddr := wsConn.RemoteAddr().String()
	router := ins.Router
	defer func() {
		_ = wsConn.Close()
		router.wsMutex.Lock()
		delete(router.wsClientMap, remoteAddr)
		router.wsMutex.Unlock()
		dbg.Dbg("delete ws client: %s\n", remoteAddr)
	}()
	var data []byte
	var dataType int
	wsHandler := ins.Router.wsCallback
	rpcServer := ins.Router.wsRPCServer
	clientInfo := new(WsClientInfo)
	clientInfo.conn = wsConn
	clientInfo.sendBuf = xlist.NewQueue(10000)
	clientInfo.remoteAddr = remoteAddr
	router.wsMutex.Lock()
	router.wsClientMap[clientInfo.remoteAddr] = clientInfo
	router.wsMutex.Unlock()
	dbg.Dbg("add ws client: %s\n", remoteAddr)
	for {
		if dataType, data, err = wsConn.ReadMessage(); err != nil {
			break
		}
		l := len(data)
		if l > 64 {
			l = 64
		}
		dbg.Dbg("DataType: %d data: %s\n", dataType, string(data[:l]))
		if wsHandler != nil {
			r := new(WsRequest)
			r.Data = data
			r.DataType = dataType
			r.handler = wsConn
			r.remoteAddress = wsConn.RemoteAddr().String()
			w := new(WsResponse)
			err = wsHandler(clientInfo, r, w)
			if err != nil {
				dbg.Dbg("Error: %s\n", err.Error())
				return
			}
			if w.Data != nil && len(w.Data) > 0 {
				err = wsConn.WriteMessage(w.DataType, w.Data)
				if err != nil {
					return
				}
			}
		} else {
			if rpcServer != nil {
				jsonObj := make(map[string]interface{})
				err := json.Unmarshal(data, &jsonObj)
				if err == nil {
					_, err = rpcServer.Call(ins, nil, jsonObj)
					if err != nil {
						dbg.Dbg("Error: %s\n", err.Error())
					} else {
						err = wsConn.WriteMessage(dataType, ins.outBuffer.Bytes())
						if err != nil {
							return
						}
					}
				}
			}
		}
	}
}

func _goWsHandle(ins *ECInstance) {
	var wsConn *websocket.Conn
	var err error
	w, r, err := ins.GetHttpCtx().ToGoHttpHandle()
	if err != nil {
		dbg.Dbg("wsEchoHandle Error: %s\n", err.Error())
		return
	}
	wsConn, err = upGrader.Upgrade(w, r, nil)
	if err != nil {
		dbg.Dbg("wsEchoHandle Error: %s\n", err.Error())
		return
	}
	wsHandlerCall := ins.Router.wsCallback
	wsEventCall := ins.Router.wsEvent
	remoteAddr := wsConn.RemoteAddr().String()
	router := ins.Router
	var closeFlag cast.Bool
	defer func() {
		closeFlag.Set(true)
		_ = wsConn.Close()
		router.wsMutex.Lock()
		delete(router.wsClientMap, remoteAddr)
		router.wsMutex.Unlock()

		if wsEventCall != nil {
			e := new(WsEVent)
			e.RemoteAddress = remoteAddr
			e.Handler = wsConn
			e.Event = 1
			e.EventStr = "delete client"
			wsEventCall(e)
		}

		dbg.Dbg("delete ws client: %s\n", remoteAddr)
	}()
	var data []byte
	var dataType int
	clientInfo := new(WsClientInfo)
	clientInfo.conn = wsConn
	clientInfo.sendBuf = xlist.NewQueue(10000)
	clientInfo.remoteAddr = remoteAddr
	router.wsMutex.Lock()
	router.wsClientMap[clientInfo.remoteAddr] = clientInfo
	router.wsMutex.Unlock()
	dbg.Dbg("add ws client: %s\n", remoteAddr)
	if wsEventCall != nil {
		e := new(WsEVent)
		e.RemoteAddress = remoteAddr
		e.Handler = wsConn
		e.Event = 1
		e.EventStr = "add client"
		wsEventCall(e)
	}
	// write thread
	go func(client *WsClientInfo) {
		for {
			if closeFlag.Get() {
				return
			}
			v, _ := client.sendBuf.GetNoWait()
			if v != nil {
				buf, ok := v.(*WsSendBuf)
				if ok {
					err := client.conn.WriteMessage(buf.dataType, buf.data)
					if err != nil {
						dbg.Dbg("Send Error: %s\n", err.Error())
					}
				}
			} else {
				time.Sleep(time.Millisecond * 50)
			}
		}
	}(clientInfo)

	for {
		if dataType, data, err = wsConn.ReadMessage(); err != nil {
			break
		}
		l := len(data)
		if l > 64 {
			l = 64
		}
		dbg.Dbg("DataType: %d data: %s\n", dataType, string(data[:l]))
		if wsHandlerCall != nil {
			r := new(WsRequest)
			r.Data = data
			r.DataType = dataType
			r.handler = wsConn
			r.remoteAddress = wsConn.RemoteAddr().String()
			w := new(WsResponse)
			err = wsHandlerCall(clientInfo, r, w)
			if err != nil {
				dbg.Dbg("Error: %s\n", err.Error())
				return
			}
			if w.Data != nil && len(w.Data) > 0 {
				buf := new(WsSendBuf)
				buf.dataType = w.DataType
				buf.data = w.Data
				_ = clientInfo.sendBuf.PutNoWait(buf)
			}
		}
	}
}
