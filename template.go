package xhttp

import (
	"bytes"
	"html/template"
)

type ECTemplate struct {
	env       map[string]interface{}
	outBuffer *bytes.Buffer
}

func NewECTemplate() *ECTemplate {
	tp := new(ECTemplate)
	tp.init()
	return tp
}

func (me *ECTemplate) init() {
	me.env = make(map[string]interface{})
}

func (me *ECTemplate) SetVar(name string, value interface{}) {
	if me.env == nil {
		me.env = make(map[string]interface{})
	}
	me.env[name] = value
}

func (me *ECTemplate) RunFiles(filename ...string) error {
	me.outBuffer = nil
	t, err := template.ParseFiles(filename...)
	if err != nil {
		return err
	}
	outBuf := new(bytes.Buffer)
	me.outBuffer = outBuf
	err = t.Execute(outBuf, me.env)
	return err
}

func (me *ECTemplate) RunBuffer(text string) error {
	me.outBuffer = nil
	t, err := template.New("page").Parse(text)
	if err != nil {
		return err
	}
	outBuf := new(bytes.Buffer)
	me.outBuffer = outBuf
	err = t.Execute(outBuf, me.env)
	return err
}

func (me *ECTemplate) GetBuffer() []byte {
	if me.outBuffer != nil {
		return me.outBuffer.Bytes()
	} else {
		return make([]byte, 0)
	}
}
