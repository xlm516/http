package xhttp

import (
	"fmt"
	"time"
)

func funcHello(ins *ECInstance) string {
	return "aaaaaa"
}

func TestHttpRpc() {
	echttp := NewECHttp()
	echttp.AddBind("", ":8881")
	rpcSer := NewECRPCServerEncrpty()
	rpcSer.SetSessionKey("hello1234")
	rpcSer.AddFunc("hello", funcHello, 0)
	echttp.GetRouter().AddRouter("/rpc", rpcSer, true)
	echttp.Start()

	rpcCli := NewECRPCClientEncrpty("127.0.0.1:8881/rpc")
	rpcCli.SetSessionKey("hello1234")
	for {
		tmpStr := ""
		err := rpcCli.Call2(&tmpStr, "hello")
		if err != nil {
			fmt.Println(err)
		} else {
			fmt.Println(tmpStr)
		}
		time.Sleep(time.Second)
	}
}

func main() {

}
