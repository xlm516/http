package xhttp

import (
	"encoding/json"
	"fmt"
	"gitee.com/xlm516/http/common"
	"gitee.com/xlm516/http/gohttp/gohttpserver"
	"gitee.com/xlm516/xtool/dbg"
	xfile "gitee.com/xlm516/xtool/file"
	xstring "gitee.com/xlm516/xtool/string"
	"gitee.com/xlm516/xtool/sys"
	"net/http"
	"strings"
)

/*
	func (me *ECRouterMgr) getHttpPostData(r *http.Request, maxData int64) ([]byte, error) {
		var jsonData bytes.Buffer
		buf := make([]byte, 2000)
		dataLen := r.ContentLength
		if maxData == 0 {
			maxData = 10*1024*1024
		}
		if dataLen > maxData {
			return nil, errors.New("Too larger data")
		}
		if dataLen <= 0 {
			return nil, errors.New("No Content-Length")
		}
		for {
			n, err := r.Body.Read(buf)
			nextLen := int(dataLen) - jsonData.Len()
			if n >= nextLen {
				n = nextLen
			}
			if nextLen <= 0 {
				break
			}
			jsonData.Write(buf[:n])
			if err != nil {
				break
			}
		}
		return jsonData.Bytes(), nil
	}
*/
func (me *ECRouterMgr) initHttpEnv(ctx *common.ECHttpContext, bind *ECHttpBinds) map[string]interface{} {
	httpEnv := make(map[string]interface{})
	httpEnv["HttpRemoteAddr"] = ctx.Request.RemoteAddress()
	httpEnv["_method"] = ctx.Request.Method()
	httpEnv["_host"] = ctx.Request.Host()
	httpEnv["_remote"] = ctx.Request.RemoteAddress()
	httpEnv["_userAgent"] = ctx.Request.UserAgent()

	reqUri := ctx.Request.UrlPath()
	httpEnv["_uri"] = reqUri
	httpEnv["_rootUri"] = ctx.Request.UrlPath()

	lines := strings.Split(ctx.Request.RemoteAddress(), ":")
	httpEnv["_ip"] = lines[0]
	var request map[string]interface{} = make(map[string]interface{})
	maxUpload := bind.MaxUpload
	if maxUpload == 0 {
		maxUpload = 20 * 1024 * 1024
	}
	err := ctx.Request.ParseMultipartForm(maxUpload)
	if err != nil {
		cType := ""
		tmpHeader := ctx.Request.GetHttpHeader("Content-Type")
		if len(tmpHeader) > 0 {
			cType = strings.ToLower(tmpHeader[0])
		}
		if cType == "multipart/form-data" {
			dbg.Dbg("%s, maxUpload: %d\n", err.Error(), maxUpload)
		}
	}
	form, err := ctx.Request.GetForms()
	if err != nil {
		fmt.Println("GetForms Failed: ", err)
	}
	for k, v := range form {
		if len(v) > 0 {
			request[k] = v[0]
		}
	}
	httpEnv["_request"] = request
	return httpEnv
}

func (me *ECRouterMgr) SetCrossOrigin(host string) {
	me.crossOrigin = host
}

func (me *ECRouterMgr) SetCrossHeaders(header string) {
	me.crossHeaders = header
}

func (me *ECRouterMgr) SetCrossMethod(method string) {
	me.crossMethod = method
}

func (me *ECRouterMgr) HttpRouter(bind *ECHttpBinds, ctx *common.ECHttpContext) {
	if len(bind.WhiteIPList) > 0 && bind.EnableWhiteIP {
		tmpIPs := strings.Split(ctx.Request.RemoteAddress(), ":")
		if len(tmpIPs) > 0 {
			_, ok := bind.WhiteIPListMap[tmpIPs[0]]
			if ok == false {
				dbg.Dbg("Check White List failed for %s\n", tmpIPs[0])
				_ = ctx.Request.Close()
				return
			}
		}

	}
	tmpUrl := ""
	requestUrl := ctx.Request.RequestURI()
	if ctx.Request.UrlPath() != "" {
		tmpUrl = ctx.Request.UrlPath()
		index := strings.Index(tmpUrl, "?")
		if index > 0 {
			tmpUrl = tmpUrl[:index]
		}
	} else {
		tmpUrl = requestUrl
		index := strings.Index(tmpUrl, "?")
		if index > 0 {
			tmpUrl = tmpUrl[:index]
		}
	}
	router, secondUrl := me.GetRouter(bind.Name, tmpUrl)
	if router == nil {
		router, secondUrl = me.GetRouter(bind.Name, tmpUrl+"/")
		if router != nil {
			index := strings.Index(requestUrl, "?")
			if index > 0 {
				tmpUrl = requestUrl[:index] + "/?" + requestUrl[index+1:]
			} else {
				tmpUrl = requestUrl + "/"
			}
			_ = ctx.Response.Redirect(tmpUrl, 302)
			return
		}
	}
	if router == nil {
		if me.NotFoundFunc != nil {
			ins := NewECInstance()
			cookiePath := me.GetCookieRouter(tmpUrl)
			ins.httpInData.cookieSessionName = GetSessionName(cookiePath, bind.GetBindPort())
			ins.httpInData.cookiePath = cookiePath
			ins.httpInData.reqUri = tmpUrl
			ins.httpInData.httpBind = bind
			ins.httpInData.httpCtx = ctx
			ins.httpInData.httpEx = bind.GetHttpEx()

			session := ctx.Request.GetCookieSlice(GetSessionName(cookiePath, bind.GetBindPort()))
			if session != nil {
				for _, c := range session {
					ins.httpInData.sessionNames = append(ins.httpInData.sessionNames, c.Value)
					if ins.CheckSession(c.Value) {
						ins.httpInData.sessionName = c.Value
					}
				}
			}
			if me.NotFoundFunc(ins, nil) {
				return
			}
		}
		dbg.Dbg("Get Router Failed for %s, bindName=%s\n", tmpUrl, bind.Name)
		tmpStr := fmt.Sprintf("Not found %s\n", tmpUrl)
		_ = ctx.Response.SetHttpCode(404)
		_ = ctx.Response.WriteString(tmpStr)
		return
	}
	fName, fFile, fLine := router.GetFuncInfo()
	if fName != "" && me.routerDebug {
		dbg.Dbg("Router Info, name: %s, File: %s, line: %d\n", fName, fFile, fLine)
	}
	ins := NewECInstance()
	cookiePath := me.GetCookieRouter(tmpUrl)
	ins.httpInData.cookiePath = cookiePath
	ins.httpInData.reqUri = tmpUrl
	ins.Router = router
	ins.Http = bind.GetECHttp()
	contentType := ctx.Request.GetHttpHeaderOne("Content-Type", nil)
	isJson := false
	contentType = strings.ToLower(contentType)
	if "application/json" == contentType {
		isJson = true
	}
	ins.httpInData.httpEnv = me.initHttpEnv(ctx, bind)
	if ins.httpInData.httpEnv == nil {
		ins.httpInData.httpEnv = make(map[string]interface{})
	}
	ins.httpInData.httpEnv["_routerRoot"] = router.GetPath()
	if ins.httpCode > 0 {
		_ = ctx.Response.SetHttpCode(ins.httpCode)
		ins.httpCode = 0
	}
	maxUpload := bind.MaxUpload
	data, err := ctx.Request.ReadBody(maxUpload)
	if err == nil {
		ins.data = data
		if isJson || true {
			_ = json.Unmarshal(data, &ins.jsonObj)
		}
	} else {
		if strings.Index(err.Error(), "Content-Length") < 0 {
			dbg.Dbg("getHttpPostData: %s\n", err.Error())
		}
	}
	ins.httpInData.httpBind = bind
	ins.httpInData.httpCtx = ctx
	ins.httpInData.httpEx = bind.http
	ins.httpInData.SecondUrl = secondUrl
	ins.httpInData.host = ctx.Request.RemoteAddress()

	ins.httpInData.cookieSessionName = GetSessionName(cookiePath, bind.GetBindPort())
	session := ctx.Request.GetCookieSlice(GetSessionName(cookiePath, bind.GetBindPort()))
	if session != nil && len(session) > 0 {
		for _, c := range session {
			ins.httpInData.sessionNames = append(ins.httpInData.sessionNames, c.Value)
			if ins.CheckSession(c.Value) {
				ins.httpInData.sessionName = c.Value
			}
		}
	}

	err = me.doRouter(ins, router)
	for k, v := range ins.httpOutData.httpHeader {
		_ = ctx.Response.SetHttpHeader(k, v)
	}
	if me.crossOrigin != "" {
		ctx.Response.SetHttpHeader("Access-Control-Allow-Origin", me.crossOrigin)
	}
	if me.crossHeaders != "" {
		ctx.Response.SetHttpHeader("Access-Control-Allow-Headers", me.crossHeaders)
	}
	if me.crossMethod != "" {
		ctx.Response.SetHttpHeader("Access-Control-Allow-Methods", me.crossMethod)
	}
	if len(ins.httpOutData.httpCookie) > 0 {
		_ = ctx.Response.DelHttpHeader("Set-Cookie")
		for k, v := range ins.httpOutData.httpCookie {
			c := new(common.ECCookieInfo)
			c.Name = k
			c.Value = v
			c.Path = ins.httpOutData.cookiePath
			_ = ctx.Response.SetCookie(c)
		}
	}
	if ins.httpCode > 0 {
		ctx.Response.SetHttpCode(ins.httpCode)
	}
	if ins.RedirectFile {
		ins.httpInData.httpCtx.Request.ModifyUrlPath(ins.RedirectFileName)
		tmpPath := sys.FormatDir(ins.RedirectFilePath)
		dbg.Dbg("RedirectFilePath: %s, name: %s\n", tmpPath, ins.RedirectFileName)
		var tmpHandler interface{}
		if ins.httpInData.httpCtx.HttpType == common.FASTHTTP {
			//tmpHandler = fasthttpserver.NewFastHttpFileHandler(tmpPath)
			//fasthttpserver.FastHttpFileHandler(ins.httpInData.httpCtx, tmpHandler)
		} else {
			tmpHandler = gohttpserver.NewGoHttpFileHandler(tmpPath)
			gohttpserver.GoHttpFileHandler(ins.GetHttpCtx(), tmpHandler)
		}
		//tmpHandler := http.FileServer(http.FileSystem(http.Dir(tmpPath)))
		//tmpHandler.ServeHTTP(ins.GetHttpWriter(), ins.GetHttpRequest())
		return
	}

	if ins.outBuffer.Len() > 0 {
		_ = ctx.Response.SetHttpHeader("Content-Length", fmt.Sprintf("%d", ins.outBuffer.Len()))
		err := ctx.Response.Write(ins.outBuffer.Bytes())
		if err != nil {
			dbg.Dbg("Ctx Response: %+v, url:%s\n", err, ins.RequestURI())
		}
	} else if err != nil {

	}
}

/*
args[0] = fs
args[1] = rootDir
args[2] = 404File
*/
func (me *ECRouterMgr) doFileServer(ins *ECInstance, router *ECRouter) error {
	if router.callback != nil {
		fileUrl := ins.httpInData.httpCtx.Request.UrlPath()
		ok, err := router.callback(ins, router, fileUrl)
		if ok == false {
			if ins.RedirectFile {
				ins.httpInData.httpCtx.Request.ModifyUrlPath(ins.RedirectFileName)
				tmpPath := sys.FormatDir(ins.RedirectFilePath)
				dbg.Dbg("RedirectFilePath: %s, name: %s\n", tmpPath, ins.RedirectFileName)
				var tmpHandler interface{}
				if ins.httpInData.httpCtx.HttpType == common.FASTHTTP {
					//tmpHandler = fasthttpserver.NewFastHttpFileHandler(tmpPath)
					//fasthttpserver.FastHttpFileHandler(ins.httpInData.httpCtx, tmpHandler)
				} else {
					tmpHandler = gohttpserver.NewGoHttpFileHandler(tmpPath)
					gohttpserver.GoHttpFileHandler(ins.GetHttpCtx(), tmpHandler)
				}
				//tmpHandler := http.FileServer(http.FileSystem(http.Dir(tmpPath)))
				//tmpHandler.ServeHTTP(ins.GetHttpWriter(), ins.GetHttpRequest())
				return nil
			}
			if err != "" {
				ins.WriteString(err)
			}
			return nil
		}
	}
	isIndexPage := false

	fileUrl := ins.httpInData.httpCtx.Request.UrlPath()
	dbg.Dbg("request URL: %s\n", fileUrl)
	tmpLen := len(fileUrl)
	if tmpLen > 0 && fileUrl[tmpLen-1] == '/' {
		fileUrl += "index"
	}
	if fileUrl == "/index" {
		isIndexPage = true
	}
	if fileUrl == "/" {
		fileUrl = "/index"
		isIndexPage = true
	} else {
		tmpLen := len(fileUrl)
		if tmpLen > 0 && fileUrl[tmpLen-1] == '/' {
			fileUrl += "index"
			isIndexPage = true
		} else {
			tmpIndex := strings.LastIndexByte(fileUrl, '/')
			if tmpIndex > 0 {
				if fileUrl[tmpIndex+1:] == "index" {
					isIndexPage = true
				}
			}
		}
	}
	fileExt := strings.ToLower(xstring.GetFileExt(fileUrl))
	if len(router.path) > 1 {
		tmpLen := len(router.path)
		fileUrl = fileUrl[tmpLen-1:]
		//ins.httpInData.httpCtx.Request.ModifyUrlPath(fileUrl)
	}
	file404 := ""
	rootDir := ""
	filename := fileUrl
	if len(router.args) >= 1 {
		rootDir = router.args[0]
		filename = xstring.ConnPath(rootDir, fileUrl)
	}
	if len(router.args) >= 2 {
		file404 = router.args[1]
	}
	if me.enableTpl && len(router.args) >= 1 && (fileExt == ".html" || fileExt == ".htm") {
		filename = "?"
		fileUrl = "?"
		fileExt = "?"
		ins.httpInData.httpCtx.Request.ModifyUrlPath("?")
	}
	if fileUrl != "/" && fileUrl != "?" && (fileExt == "" || fileExt == ".tpl") {
		tmpFile := filename + ".htm"
		bExist, bDir := xfile.FileExist2(tmpFile)
		if bExist && bDir == false {
			filename = tmpFile
			fileUrl += ".htm"
		} else {
			tmpFile := xstring.ConnPath(filename, "index.htm")
			bExist, bDir = xfile.FileExist2(tmpFile)
			if bExist && bDir == false {
				filename = tmpFile
				fileUrl = xstring.ConnPath(fileUrl, "index.htm")
			}
		}
		if bExist && bDir == false {
			fileExt = ".htm"
		}
		if bExist == false {
			tmpFile := filename + ".html"
			bExist, bDir := xfile.FileExist2(tmpFile)
			if bExist && bDir == false {
				filename = tmpFile
				fileUrl += ".html"
			} else {
				tmpFile := xstring.ConnPath(filename, "index.html")
				bExist, bDir = xfile.FileExist2(tmpFile)
				if bExist && bDir == false {
					filename = tmpFile
					fileUrl = xstring.ConnPath(fileUrl, "index.html")
				}
			}
			if bExist && bDir == false {
				fileExt = ".html"
			}
		}
	}
	dbg.Dbg("handler File %s\n", filename)
	if fileUrl != "/" && xfile.FileExist(filename) == false {
		if me.NotFoundFunc != nil {
			if me.NotFoundFunc(ins, router) {
				return nil
			}
		}
		if file404 != "" && xfile.FileExist(file404) {
			tpl := NewECTemplate()
			tpl.SetVar("ErrorCode", "404")
			tpl.SetVar("ErrorString", "Not Found")
			_ = tpl.RunFiles(file404)
			_ = ins.httpInData.httpCtx.Response.SetHttpCode(404)
			_ = ins.httpInData.httpCtx.Response.Write(tpl.GetBuffer())
			return nil
		}
	}

	if (me.enableTpl) && (fileExt == ".html" || fileExt == ".htm") && len(router.args) >= 1 {
		htmlTpl := router.htmlTpl
		if htmlTpl != nil && me.enableCompiler {
			router.compileMutex.Lock()
			htmlTpl.Env = ins.GetHttpEnv()
			err := htmlTpl.Compiler(filename)
			router.compileMutex.Unlock()
			if err != nil {
				dbg.Dbg("%s\n", err.Error())
			}
			fileUrlPath := xstring.GetFilePath(fileUrl)
			fileUrlName := xstring.GetFileName(fileUrl)
			newFileUrl := xstring.ConnPath(router.GetPath(), fileUrlPath, ".tmp", fileUrlName)
			if isIndexPage {
				ins.AddHttpHeader("Content-Type", "text/html")
				tmpFileName := xstring.ConnPath(rootDir, fileUrlPath, ".tmp", fileUrlName)
				b, err := xfile.ReadFile(tmpFileName)
				if err != nil {
					dbg.Dbg("Error: %s\n", err)
					ins.WriteString(err.Error())
				} else {
					ins.Write(b)
				}
				return nil
			}
			ins.httpInData.httpCtx.Request.ModifyUrlPath(newFileUrl)
		} else if isIndexPage == false {
			urlPath := ins.httpInData.httpCtx.Request.UrlPath()
			ins.httpInData.httpCtx.Request.ModifyUrlPath(urlPath + fileExt)
		}
	}
	if xfile.FileExist(filename) == false {
		ins.httpInData.httpCtx.Response.SetHttpCode(404)
		ins.WriteConn([]byte("Not Found"))
		return nil
	}
	if ins.httpInData.httpCtx.HttpType == common.FASTHTTP {
		//fasthttpserver.FastHttpFileHandler(ins.httpInData.httpCtx, router.handler)
	} else {
		//gohttpserver.GoHttpFileHandler(ins.httpInData.httpCtx, router.handler)
		tmpHandle, _ := router.handler.(http.Handler)
		gohttpserver.GoHttpFileHandler(ins.httpInData.httpCtx, http.StripPrefix(router.GetPath(), tmpHandle))
	}
	return nil
}

func (me *ECInstance) InstanceToHttpWriter() {
	allHttpHeader := me.httpInData.httpCtx.Request.GetHttpHeaderAll()
	for _, v := range allHttpHeader {
		me.httpInData.httpCtx.Response.DelHttpHeader(v.Name)
		me.httpInData.httpCtx.Response.SetHttpHeader(v.Name, v.Value[0])
	}
	if len(me.httpOutData.httpCookie) > 0 {
		me.httpInData.httpCtx.Response.DelHttpHeader("Set-Cookie")
		for k, v := range me.httpOutData.httpCookie {
			c := new(common.ECCookieInfo)
			c.Name = k
			c.Value = v
			c.Path = me.httpOutData.cookiePath
			dbg.Dbg("Set Cookie: %+v, k=%s v=%s\n", c, k, v)
			me.httpInData.httpCtx.Response.SetCookie(c)
		}
	}
}
