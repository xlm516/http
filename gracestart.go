//go:build linux || darwin || freebsd || netbsd || openbsd
// +build linux darwin freebsd netbsd openbsd

package xhttp

import (
	"fmt"
	"gitee.com/xlm516/xtool/dbg"
	"os"
	"syscall"
	"time"
)

func (me *ECHttp) GraceRestart2(wait bool) error {
	var fds []uintptr = make([]uintptr, 0)
	environList := []string{}
	fds = append(fds, os.Stdin.Fd())
	environList = append(environList, "_stdin")
	fds = append(fds, os.Stdout.Fd())
	environList = append(environList, "_stdout")
	fds = append(fds, os.Stderr.Fd())
	environList = append(environList, "_stderr")
	for _, v := range me.binds {
		fd, err := GetListenFd(v.http.GetListener())
		if err == nil {
			fds = append(fds, fd)
			environList = append(environList, "_fd"+v.Bind)
		}
	}
	for _, v := range me.tlsBinds {
		fd, err := GetListenFd(v.http.GetListener())
		if err == nil {
			fds = append(fds, fd)
			environList = append(environList, "_fd"+v.Bind)
		}
	}
	path := os.Args[0]
	for _, value := range os.Environ() {
		if value[0] != '_' {
			environList = append(environList, value)
		}
	}
	execSpec := &syscall.ProcAttr{
		Env:   environList,
		Files: fds,
	}

	fork, err := syscall.ForkExec(path, os.Args, execSpec)
	if err != nil {
		fmt.Printf("failed to forkexec: %v\n", err)
		return err
	}

	for _, v := range me.binds {
		v.http.CloseListen()
	}
	for _, v := range me.tlsBinds {
		v.http.CloseListen()
	}
	fmt.Printf("start new process success, pid %d.\n", fork)
	if wait == false {
		os.Exit(0)
	}
	for {
		count := 0
		for _, v := range me.binds {
			httpEx := v.http
			if httpEx == nil {
				continue
			}
			httpEx.Lock()
			count = len(httpEx.GetConnectList())
			httpEx.Unlock()
			if count > 0 {
				break
			}
		}
		if count <= 0 {
			for _, v := range me.tlsBinds {
				httpEx := v.http
				if httpEx == nil {
					continue
				}
				httpEx.Lock()
				count = len(httpEx.GetConnectList())
				httpEx.Unlock()
				if count > 0 {
					break
				}
			}
		}
		if count <= 0 {
			dbg.Dbg("old program exit ok\n")
			os.Exit(0)
		} else {
			time.Sleep(time.Millisecond * 300)
		}
	}
	return nil
}
