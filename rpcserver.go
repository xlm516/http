package xhttp

import (
	"encoding/json"
	"errors"
	"fmt"
	"gitee.com/xlm516/xtool/cast"
	"gitee.com/xlm516/xtool/dbg"
	"gitee.com/xlm516/xtool/encoding"
	xid "gitee.com/xlm516/xtool/id"
	xmap "gitee.com/xlm516/xtool/map"
	xreflect "gitee.com/xlm516/xtool/reflect"
	"gitee.com/xlm516/xtool/sys"
	xtime "gitee.com/xlm516/xtool/time"
	"reflect"
	"strconv"
	"strings"
)

type NegoUser struct {
	Token  string
	PriKey string
	Mode   int
}
type NegoRequest struct {
	Token string
}

type NegoResponse struct {
	Token      string
	AuthMode   int
	CookieMode int
}

type RPCConnData struct {
	User    string
	Nonce   string
	IsLogin bool
}

type ECRPCArgument struct {
	Name      string
	Type      reflect.Type
	Kind      reflect.Kind
	FieldName string
}

type ECRPCFunction struct {
	Name       string
	Fun        reflect.Value
	Level      int
	IsVariadic bool
	Input      []*ECRPCArgument
	Output     []*ECRPCArgument
	UserData   interface{}
}

type ECRPCHandler interface {
	Call(ins *ECInstance, env map[string]interface{}, jsonObj map[string]interface{}) ([]interface{}, error)
}

type ECRPCServer struct {
	funcList        map[string]*ECRPCFunction
	FunCheckPri     func(ins *ECInstance, f *ECRPCFunction, arg map[string]interface{}) (int, string)
	secMode         int //security mode
	cookieMode      bool
	OnAuth          func(req *NegoRequest) (*NegoUser, bool)
	jsonIdInterval  int
	autoResultToMap bool
	sessionKey      string
}

func NewECRPCServer() *ECRPCServer {
	s := new(ECRPCServer)
	s.init()
	return s
}

func NewECRPCServerEncrpty() *ECRPCServer {
	s := new(ECRPCServer)
	s.init()
	s.SetSecurityMode(SECURITY_HTTP_AES_KIV)
	s.SetJsonIdMaxInterval(900)
	s.SetResultToMap(true)
	return s
}

func (me *ECRPCServer) init() {
	me.funcList = make(map[string]*ECRPCFunction)
	//me.cookieMode = true
	me.jsonIdInterval = 300
	me.autoResultToMap = true
	me.AddFunc("nego", me.Negotiation, 0)
}

func (me *ECRPCServer) SetResultToMap(v bool) {
	me.autoResultToMap = v
}
func (me *ECRPCServer) SetSessionKey(key string) {
	me.sessionKey = key
}

/*
*
设置最大的jsonid的时间偏差
*/
func (me *ECRPCServer) SetJsonIdMaxInterval(v int) {
	me.jsonIdInterval = v
}

/*
xhttp.SECURITY_NEGO_ZIP_3DES
*/
func (me *ECRPCServer) SetSecurityMode(v int) {
	me.secMode = v
}

func (me *ECRPCServer) EnableCookie(v bool) {
	me.cookieMode = v
}

func (me *ECRPCServer) GetSecurityMode(v int) (int, string) {
	retStr := ""
	switch me.secMode {
	case SECURITY_NULL:
		retStr = "NULL"
	case SECURITY_NEGO_ZIP_3DES:
		retStr = "Zip_3DES"
	}
	return v, retStr
}

func (me *ECRPCServer) functionArgRetTypes(funt reflect.Type) (targs, tout []reflect.Type) {
	targs = make([]reflect.Type, funt.NumIn())
	for i := range targs {
		targs[i] = funt.In(i)
	}
	tout = make([]reflect.Type, funt.NumOut())
	for i := range tout {
		tout[i] = funt.Out(i)
	}
	return
}

func (me *ECRPCServer) AddFunc(name string, fun interface{}, level int) error {
	return me.AddFuncWithData(name, fun, level, nil)
}

func (me *ECRPCServer) AddFuncWithData(name string, fun interface{}, level int, data interface{}) error {
	if fun == nil {
		return errors.New("function is null")
	}
	t := reflect.TypeOf(fun)
	if t.Kind() != reflect.Func {
		return errors.New(fmt.Sprintf("Not Function is %s", t.Kind()))
	}
	var funv reflect.Value
	switch ff := fun.(type) {
	case reflect.Value:
		funv = ff
	default:
		funv = reflect.ValueOf(fun)
	}
	funt := funv.Type()
	in, out := me.functionArgRetTypes(funt)

	newFunc := &ECRPCFunction{}
	newFunc.Fun = funv
	newFunc.Name = name
	newFunc.Level = level
	newFunc.UserData = data
	newFunc.IsVariadic = funt.IsVariadic()
	for _, v := range in {
		newArg := &ECRPCArgument{}
		newArg.Kind = v.Kind()
		if v.Kind() == reflect.Ptr {
			newArg.Type = v.Elem()
		} else {
			newArg.Type = v
		}
		newArg.Name = fmt.Sprintf("%s", v.Kind())
		switch v.Kind() {
		case reflect.Ptr:
			v2 := v.Elem()
			newArg.FieldName = v2.Name()
		case reflect.Interface:
			t := reflect.TypeOf(v)
			switch t.Kind() {
			case reflect.Ptr:
				v2 := t.Elem()
				newArg.FieldName = v2.Name()
			default:
				newArg.FieldName = v.Name()
			}
		default:
			newArg.FieldName = v.Name()
		}
		index := strings.IndexByte(newArg.FieldName, '.')
		if index >= 0 {
			newArg.FieldName = newArg.FieldName[index+1:]
		}
		newFunc.Input = append(newFunc.Input, newArg)
	}

	for _, v := range out {
		newArg := &ECRPCArgument{}
		newArg.Kind = v.Kind()
		newArg.Type = v
		newArg.Name = fmt.Sprintf("%s", v.Kind())
		newFunc.Output = append(newFunc.Output, newArg)
	}
	me.funcList[name] = newFunc
	return nil

}

func (me *ECRPCServer) GetInterfaceType(val interface{}) reflect.Kind {
	var funv reflect.Value
	switch ff := val.(type) {
	case reflect.Value:
		funv = ff
	default:
		funv = reflect.ValueOf(val)
	}
	return funv.Kind()
}

func (me *ECRPCServer) floatToStr(f float64) string {
	tmpStr := fmt.Sprintf("%f", f)
	dotIndex := strings.Index(tmpStr, ".")
	index := 0
	strLen := len(tmpStr)
	if dotIndex > 0 && strLen > 0 {
		for i := strLen - 1; i >= 0; i-- {
			if tmpStr[i] != '0' {
				index = i
				break
			}
		}
	}
	if dotIndex == index {
		tmpStr = tmpStr[:index]
	} else if index+1 < strLen {
		tmpStr = tmpStr[:index+1]
	}

	return tmpStr
}

func (me *ECRPCServer) responseError(ins *ECInstance, code int, errStr string) {
	ins.rpcData.errorCode = code
	ins.rpcData.errorString = errStr
	//nonce := ""
	if code == JRPC_ERR_NOT_LOGIN {
		var connData *RPCConnData
		tmpData := ins.GetConnUserData()
		if tmpData == nil {
			connData = &RPCConnData{}
			ins.SetConnUserData(connData)
		} else {
			connData = tmpData.(*RPCConnData)
			if connData == nil {
				fmt.Println("Get ConnData Failed")
			}
		}

		if connData != nil {
			connData.Nonce = xid.NewKey28()
		}
		//nonce = connData.Nonce
	}

	mapRet := make(map[string]interface{})
	mapRet["id"] = ins.rpcData.rpcId
	if ins.rpcData.rpcVer != "" {
		mapRet["jsonrpc"] = ins.rpcData.rpcVer
	}
	if ins.rpcData.errorCode != 0 {
		rpcErr := make(map[string]interface{})
		rpcErr["code"] = ins.rpcData.errorCode
		rpcErr["message"] = ins.rpcData.errorString
		//rpcErr["nonce"] = nonce
		mapRet["error"] = rpcErr
	} else {
		mapRet["result"] = nil

	}
	b, _ := json.Marshal(mapRet)
	if b != nil {
		ins.WriteConn(b)
	}
}

func (me *ECRPCServer) rpcError(ins *ECInstance, code int, errStr string) {
	ins.rpcData.errorCode = code
	ins.rpcData.errorString = errStr
	mapRet := make(map[string]interface{})
	mapRet["id"] = ins.rpcData.rpcId
	if ins.rpcData.rpcVer != "" {
		mapRet["jsonrpc"] = ins.rpcData.rpcVer
	}
	if ins.rpcData.errorCode != 0 {
		rpcErr := make(map[string]interface{})
		rpcErr["code"] = ins.rpcData.errorCode
		rpcErr["message"] = ins.rpcData.errorString
		mapRet["error"] = rpcErr
	} else {
		mapRet["result"] = nil

	}
	b, _ := json.Marshal(mapRet)
	if b != nil {
		ins.WriteConn(b)
	}
}

func (me *ECRPCServer) Negotiation(ins *ECInstance, negoReq NegoRequest) *NegoResponse {
	var negoUser *NegoUser
	if me.OnAuth != nil {
		negoUser, _ = me.OnAuth(&negoReq)
	} else {
		if me.secMode == SECURITY_NEGO_ZIP_3DES {
			dbg.Dbg("ECRPCServer don't set function OnAuth\n")
		}
	}
	if negoUser == nil {
		ins.JsonRPCError(EC_JSON_NOT_NEGO, "Auth Failed")
		return nil
	}
	mapRet := make(map[string]interface{})
	mapRet["id"] = ins.rpcData.rpcId
	if ins.rpcData.rpcVer != "" {
		mapRet["jsonrpc"] = ins.rpcData.rpcVer
	}
	negoResp := &NegoResponse{}
	negoResp.AuthMode = SECURITY_NEGO_ZIP_3DES
	if me.cookieMode {
		negoResp.CookieMode = 1
	}
	if me.cookieMode {
		ins.SetSession("userinfo", negoUser, "")
	} else {
		ins.SetConnUserData(negoUser)
	}
	return negoResp
}

func (me *ECRPCServer) getClientInfo(ins *ECInstance) *NegoUser {
	var negoUser *NegoUser
	if me.cookieMode {
		v := ins.GetSession("userinfo")
		if v != nil {
			negoUser, _ = v.(*NegoUser)
		}
	} else {
		v := ins.GetConnUserData()
		if v != nil {
			negoUser, _ = v.(*NegoUser)
		}
	}
	return negoUser
}

func (me *ECRPCServer) Call(ins *ECInstance, env map[string]interface{}, jsonObj map[string]interface{}) ([]interface{}, error) {
	if ins == nil {
		return nil, errors.New("invalid argument")
	}
	var clientNonce string
	var negoUser *NegoUser = me.getClientInfo(ins)
	if jsonObj == nil && ins.data != nil && negoUser != nil {
		data, decodeHeader := JsonRPCDecodeData(negoUser.Mode, ins.data, negoUser.PriKey)
		if data != nil && len(data) > 0 {
			err := json.Unmarshal(data, &jsonObj)
			if err != nil {
				dbg.Dbg("parse JsonErr: %s\n", err.Error())
			}
		}
		clientNonce = decodeHeader.Nonce
	}
	if me.secMode == SECURITY_HTTP_AES_KIV {
		data, kiv, err := encoding.AesDecodeByKIV(ins.data, nil, nil, true)
		if err != nil {
			dbg.Dbg("%s\n", err.Error())
			return nil, err
		}
		if data != nil && len(data) > 0 {
			err := json.Unmarshal(data, &jsonObj)
			if err != nil {
				dbg.Dbg("parse JsonErr: %s\n", err.Error())
			}
		}
		clientNonce = kiv.Nonce
		sessionKey := xmap.MapAny(jsonObj).String("sessionKey")
		if me.sessionKey != "" {
			if sessionKey != encoding.Md5(me.sessionKey) {
				me.rpcError(ins, JRPC_ERR_INVALID_ARGUMENT, "invalid session key")
				return nil, errors.New("invalid session key")
			}
		}
	}

	if jsonObj != nil && me.jsonIdInterval > 0 {
		tmpI := xmap.MapAny(jsonObj).Int64("id")
		tmpTime := tmpI / 10000
		if xtime.CompTime(tmpTime, xtime.UnixTime()) >= int64(me.jsonIdInterval) {
			dbg.Dbg("invalid json id, already expires by %d interval=%d, IP:%s\n",
				xtime.CompTime(tmpTime, xtime.UnixTime()), me.jsonIdInterval, ins.RemoteAddr())
			me.rpcError(ins, JRPC_ERR_INVALID_ARGUMENT, fmt.Sprintf("invalid json id, already expires by %d", me.jsonIdInterval))
			return nil, errors.New("invalid json id, already expires" + fmt.Sprintf("%d", me.jsonIdInterval))
		}
	}

	name := xmap.MapAny(jsonObj).String("method")
	if me.secMode == SECURITY_NEGO_ZIP_3DES && negoUser == nil && name != "nego" {
		me.rpcError(ins, EC_JSON_NOT_NEGO, "do not negotiation "+name)
		dbg.Dbg("do not negotiation %s\n", name)
		return make([]interface{}, 0), nil
	}

	if jsonObj == nil {
		return nil, errors.New("json data invalid")
	}
	if me.secMode == SECURITY_NEGO_ZIP_3DES && name != "nego" {
		if negoUser == nil {
			return nil, errors.New("nego failed")
		}
	}
	f, ok := me.funcList[name]
	if ok == false {
		//Dbg.Dbg("Not Found %s\n", name)
		me.responseError(ins, JRPC_ERR_NOT_FOUND, "Not Found")
		return nil, errors.New(fmt.Sprintf("Function [%s] Not Found", name))
	}
	ins.Router.userData = f.UserData
	var args []interface{}
	args = append(args, ins)
	params, ok := jsonObj["params"]
	if ok == false {
		return nil, errors.New("no result field")
	}
	tmpI := xmap.MapAny(jsonObj).Int64("id")
	tmpTime := tmpI / 10000
	if me.jsonIdInterval > 0 && xtime.CompTime(tmpTime, xtime.UnixTime()) >= int64(me.jsonIdInterval) {
		dbg.Dbg("invalid json id, already expires, %d\n", tmpI)
		return nil, errors.New("invalid json id, already expires")
	}
	ins.rpcData.rpcId = uint64(tmpI)
	ins.rpcData.rpcVer = xmap.MapAny(jsonObj).String("jsonrpc")
	tmpArgs, ok := params.([]interface{})
	if ok {
		for _, v := range tmpArgs {
			args = append(args, v)
		}
	} else {
		args = append(args, params)
	}
	ins.rpcArgument = tmpArgs
	if me.FunCheckPri != nil {
		isLogin := ins.GetConnData("login")
		if isLogin != "1" {
			args := make(map[string]interface{})
			args["json"] = jsonObj
			if ins.httpInData.httpBind != nil {
				args["conn"] = ins.httpInData.httpBind.http.GetConnInfo(ins.RemoteAddr())
			}
			args["ip"] = ins.RemoteAddr()
			code, errStr := me.FunCheckPri(ins, f, args)
			if code != 0 {
				me.responseError(ins, JRPC_ERR_NOT_LOGIN, "Not Login")
				return nil, errors.New(errStr)
			}
			ins.SetConnData("login", "1")
		}
	}
	if f.IsVariadic == false && len(args) != len(f.Input) {
		me.responseError(ins, JRPC_ERR_INVALID_ARGUMENT, "Invalid Argument")
		return nil, errors.New(fmt.Sprintf("%s: Argument Count Invalid, input=%d, define=%d", name, len(args), len(f.Input)))
	}
	inArg := make([]reflect.Value, 0, 5)
	inArg = append(inArg, reflect.ValueOf(ins))
	for i := 1; i < len(f.Input); i++ {
		if i >= len(args) {
			break
		}
		if f.IsVariadic && i == len(f.Input)-1 {
			break
		}
		argType := me.GetInterfaceType(args[i])
		if argType == reflect.Float32 || argType == reflect.Float64 {
			tmpF, ok := args[i].(float64)
			if ok == false {
				tmpF2, _ := args[i].(float32)
				if tmpF2 != 0 {
					tmpF = float64(tmpF2)
				}
			}
			tmpStr := me.floatToStr(tmpF)
			dotFlag := strings.IndexByte(tmpStr, '.')
			var intv int64
			intv, _ = strconv.ParseInt(tmpStr, 10, 64)
			errFlag := false
			if f.Input[i].Kind != reflect.Float32 && f.Input[i].Kind != reflect.Float64 && f.Input[i].Kind != reflect.Interface {
				if dotFlag > 0 {
					dbg.Dbg("Argument #%d is not %s\n", i, f.Input[i].Name)
					me.responseError(ins, JRPC_ERR_INVALID_ARGUMENT, fmt.Sprintf("Argument #%d is not %s", i, f.Input[i].Name))
					return nil, fmt.Errorf("argument #%d is not %s", i, f.Input[i].Name)
				}
			}
			/*
				v := reflect.New(f.Input[i].Type)
				goapi.ReflectFillField(v, -1, args[i])
				inArg = append(inArg, v.Elem())
			*/
			switch f.Input[i].Kind {
			case reflect.Int:
				if intv > 0xFFFFFFFF {
					errFlag = true
					break
				}
				inArg = append(inArg, reflect.ValueOf(int(intv)))
			case reflect.Uint:
				if intv > 0xFFFFFFFF {
					errFlag = true
					break
				}
				inArg = append(inArg, reflect.ValueOf(uint(intv)))
			case reflect.Int32:
				if intv > 0xFFFFFFFF {
					errFlag = true
					break
				}
				inArg = append(inArg, reflect.ValueOf(int32(intv)))
			case reflect.Uint32:
				if intv > 0xFFFFFFFF {
					errFlag = true
					break
				}
				inArg = append(inArg, reflect.ValueOf(uint32(intv)))
			case reflect.Int64:
				inArg = append(inArg, reflect.ValueOf(int64(intv)))
			case reflect.Uint64:
				inArg = append(inArg, reflect.ValueOf(uint64(intv)))
			case reflect.Bool:
				if intv != 0 {
					inArg = append(inArg, reflect.ValueOf(true))
				} else {
					inArg = append(inArg, reflect.ValueOf(false))
				}
			case reflect.Int16:
				inArg = append(inArg, reflect.ValueOf(int16(intv)))
			case reflect.Uint16:
				inArg = append(inArg, reflect.ValueOf(uint16(intv)))
			case reflect.Int8:
				inArg = append(inArg, reflect.ValueOf(int8(intv)))
			case reflect.Uint8:
				inArg = append(inArg, reflect.ValueOf(uint8(intv)))
			case reflect.Float32:
				inArg = append(inArg, reflect.ValueOf(float32(tmpF)))
			case reflect.Float64:
				inArg = append(inArg, reflect.ValueOf(float64(tmpF)))
			case reflect.String:
				//errFlag = true
				inArg = append(inArg, reflect.ValueOf(tmpStr))
			case reflect.Interface:
				inArg = append(inArg, reflect.ValueOf(args[i]))
			case reflect.Slice:
				inf := reflect.New(f.Input[i].Type)
				err := xreflect.ReflectFillSlice(inf.Interface(), args[i])
				if err != nil {
					me.responseError(ins, JRPC_ERR_INVALID_ARGUMENT, err.Error())
					return nil, err
				}
				inArg = append(inArg, inf.Elem())
			default:
				fmt.Println("Unknown Type: ", f.Input[i].Kind)
			}
			if errFlag && f.IsVariadic == false {
				dbg.Dbg("argument #%d is not %s\n", i, f.Input[i].Name)
				me.responseError(ins, JRPC_ERR_INVALID_REQUEST, fmt.Sprintf("Argument #%d is not %s\n", i, f.Input[i].Name))
				return nil, fmt.Errorf("argument #%d is not %s", i, f.Input[i].Name)
			}
			continue
		}
		if (f.Input[i].Kind == reflect.Struct || f.Input[i].Kind == reflect.Ptr || f.Input[i].Kind == reflect.Map) && argType == reflect.Map {
			tmpMap, _ := args[i].(map[string]interface{})
			if tmpMap != nil {
				tmpObject := reflect.New(f.Input[i].Type)
				err := xreflect.ReflectFillStruct(tmpObject.Interface(), tmpMap)
				if err != nil {
					return nil, err
				}
				if f.Input[i].Kind == reflect.Struct && tmpObject.Type().Kind() == reflect.Ptr {
					inArg = append(inArg, tmpObject.Elem())
				} else if f.Input[i].Kind == reflect.Map {
					inArg = append(inArg, tmpObject.Elem())
				} else {
					inArg = append(inArg, tmpObject)
				}

			} else {
				return nil, errors.New("Input is not Map")
			}
			continue
		}

		if f.Input[i].Kind == reflect.Slice {
			inf := reflect.New(f.Input[i].Type)
			err := xreflect.ReflectFillSlice(inf.Interface(), args[i])
			if err != nil {
				me.responseError(ins, JRPC_ERR_INVALID_ARGUMENT, err.Error())
				return nil, err
			}
			inArg = append(inArg, inf.Elem())
			continue
		}
		isOk := false
		switch argType {
		case reflect.String:
			switch f.Input[i].Kind {
			case reflect.Int64:
				inArg = append(inArg, reflect.ValueOf(cast.ToInt64Value(args[i])))
				isOk = true
			case reflect.Int:
				inArg = append(inArg, reflect.ValueOf(cast.ToIntValue(args[i])))
				isOk = true
			}
		}
		if isOk {
			continue
		}
		if argType != f.Input[i].Kind && f.Input[i].Kind != reflect.Interface && f.IsVariadic == false {
			dbg.Dbg("argument #%d is not %s\n", i, f.Input[i].Name)
			me.responseError(ins, JRPC_ERR_INVALID_REQUEST, fmt.Sprintf("argument #%d is not %s", i, f.Input[i].Name))
			return nil, fmt.Errorf("argument #%d is not %s by %s", i, f.Input[i].Name, name)
		}
		inArg = append(inArg, reflect.ValueOf(args[i]))
	}
	if f.IsVariadic && len(args) >= len(f.Input) {
		varidaicType := f.Input[len(f.Input)-1].Type.Elem().Kind()
		for i := len(f.Input) - 1; i < len(args); i++ {
			argType := reflect.TypeOf(args[i]).Kind()
			var intV int64
			var dotFlag int
			if argType == reflect.Float64 || argType == reflect.Float32 {
				tmpF, ok := args[i].(float64)
				if ok == false {
					tmpF2, _ := args[i].(float32)
					if tmpF2 != 0 {
						tmpF = float64(tmpF2)
					}
				}
				tmpStr := me.floatToStr(tmpF)
				dotFlag = strings.IndexByte(tmpStr, '.')
				intV, _ = strconv.ParseInt(tmpStr, 10, 64)
				if dotFlag > 0 && (varidaicType != reflect.Float64 && varidaicType != reflect.Float32) {
					dbg.Dbg("argument #%d is not %s\n", i, f.Input[i].Name)
					me.responseError(ins, JRPC_ERR_INVALID_REQUEST, fmt.Sprintf("argument #%d is not %s", i, f.Input[i].Name))
					return nil, fmt.Errorf("argument #%d is not %s", i, varidaicType)
				}
				switch varidaicType {
				case reflect.Int:
					inArg = append(inArg, reflect.ValueOf(int(intV)))
				case reflect.Int32:
					inArg = append(inArg, reflect.ValueOf(int32(intV)))
				case reflect.Int64:
					inArg = append(inArg, reflect.ValueOf(intV))
				case reflect.Uint:
					inArg = append(inArg, reflect.ValueOf(uint(intV)))
				case reflect.Uint64:
					inArg = append(inArg, reflect.ValueOf(uint64(intV)))
				default:
					if varidaicType != argType && varidaicType != reflect.Interface {
						dbg.Dbg("argument #%d is not %s:%s\n", i, varidaicType, argType)
						return nil, fmt.Errorf("argument #%d is not %s", i, varidaicType)
					} else {
						inArg = append(inArg, reflect.ValueOf(args[i]))
					}
				}
			} else {
				if varidaicType != argType && varidaicType != reflect.Interface {
					dbg.Dbg("Argument #%d is not %s\n", i, f.Input[i].Name)
					return nil, fmt.Errorf("Argument #%d is not %s", i, varidaicType)
				} else {
					inArg = append(inArg, reflect.ValueOf(args[i]))
				}
			}
		}
	}
	//Dbg.Dbg("Call Function: %s, argLen=%d\n",name, len(inArg))
	defer sys.RecoverFunc("", 0)
	var out []reflect.Value
	if f.IsVariadic {
		out = f.Fun.Call(inArg)
	} else {
		out = f.Fun.Call(inArg)
	}
	retOut := make([]interface{}, 0)
	if out != nil && len(out) > 0 {
		for _, v := range out {
			if v.CanInterface() {
				retOut = append(retOut, v.Interface())
			} else {
				retOut = append(retOut, nil)
			}
		}
	} else {
		return nil, nil
	}
	mapRet := make(map[string]interface{})
	mapRet["id"] = ins.rpcData.rpcId
	if ins.rpcData.rpcVer != "" {
		mapRet["jsonrpc"] = ins.rpcData.rpcVer
	}
	if ins.rpcData.errorCode != 0 {
		rpcErr := make(map[string]interface{})
		rpcErr["code"] = ins.rpcData.errorCode
		rpcErr["message"] = ins.rpcData.errorString
		mapRet["error"] = rpcErr
	} else {
		if len(retOut) == 1 && me.autoResultToMap {
			mapRet["result"] = retOut[0]
		} else {
			mapRet["result"] = retOut
		}

	}
	b, _ := json.Marshal(mapRet)
	if b != nil {
		if negoUser != nil && me.secMode != SECURITY_NULL {
			b2 := JsonRPCEncodeData(negoUser.Mode, b, negoUser.PriKey, clientNonce)
			if b2 != nil {
				ins.Write(b2)
			} else {
				dbg.Dbg("Json RPC Encode failed\n")
			}
		} else if me.secMode == SECURITY_HTTP_AES_KIV {
			b2, err := encoding.AesEncodeByKIV(b, clientNonce, nil, nil, true)
			if err != nil {
				dbg.Dbg("%s\n", err.Error())
			}
			if b2 != nil {
				ins.Write(b2)
			}
		} else {
			ins.Write(b)
		}
	}
	return nil, nil
}
