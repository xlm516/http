package xhttp

import (
	"bytes"
	"errors"
	"fmt"
	"gitee.com/xlm516/xtool/dbg"
	xfile "gitee.com/xlm516/xtool/file"
	xstring "gitee.com/xlm516/xtool/string"
	"os"
	"strings"
)

type XStream interface {
	Getc() (byte, error)
	UnGetc() (int64, error)
	Gets() (string, error)
	Close() error
}

type HtmlDepend struct {
	files []string
}

type HtmlTpl struct {
	Env           map[string]interface{}
	LastErrorFile string
	RootDir       string
	htmlDepend    map[string]*HtmlDepend
	nowLevel      int
	resultBuffer  bytes.Buffer
}

func NewHtmlTpl(rootDir string) *HtmlTpl {
	tp := new(HtmlTpl)
	tp.htmlDepend = make(map[string]*HtmlDepend)
	tp.RootDir = rootDir
	return tp
}

func (me *HtmlTpl) doCompile(file, tmpFile string) error {
	me.resultBuffer.Reset()
	err, _ := me.loadFromFile(file, 0)
	if err != nil {
		os.Remove(tmpFile)
		return err
	}
	fh, err := xfile.OpenFileEx(tmpFile, os.O_CREATE|os.O_RDWR|os.O_TRUNC)
	if err != nil {
		os.Remove(tmpFile)
		return err
	}
	fh.Write(me.resultBuffer.Bytes())
	return nil
}

func (me *HtmlTpl) GetEnvVar(name string) (string, bool) {
	if me.Env == nil {
		return "", false
	}
	v, ok := me.Env[name]
	if ok {
		return fmt.Sprintf("%+v", v), true
	}
	inf, ok := me.Env["_request"]
	if ok {
		arg, ok := inf.(map[string]interface{})
		if ok && arg != nil {
			v, ok := arg[name]
			if ok {
				return fmt.Sprintf("%+v", v), true
			}
		}
	}
	return "", false
}

func (me *HtmlTpl) Compiler(file string) error {
	htmlDep, _ := me.htmlDepend[file]
	filePath := xstring.GetFilePath(file)
	fileName := xstring.GetFileName(file)
	tmpFile := xstring.ConnPath(filePath, ".tmp", fileName)
	geneFileInfo, geneFileInfoErr := os.Stat(tmpFile)
	fileInfo, fileInfoErr := os.Stat(file)
	compileFlag := false
	if fileInfoErr != nil {
		return fileInfoErr
	}
	if geneFileInfoErr != nil || htmlDep == nil {
		compileFlag = true
	}

	if compileFlag == false && fileInfo.ModTime().Sub(geneFileInfo.ModTime()) > 0 {
		compileFlag = true
	}

	if compileFlag {
		dbg.Dbg("Start Compile=%s\n", file)
		xfile.Mkdir(xstring.ConnPath(filePath, ".tmp"))
		return me.doCompile(file, tmpFile)
	}
	if htmlDep != nil {
		for _, v := range htmlDep.files {
			tmpFileInfo, tmpFileInfoErr := os.Stat(v)
			if tmpFileInfoErr == nil {
				if tmpFileInfo.ModTime().Sub(geneFileInfo.ModTime()) > 0 {
					compileFlag = true
					break
				}
			}
		}
	}
	if compileFlag {
		dbg.Dbg("Start Compile=%s\n", file)
		xfile.Mkdir(xstring.ConnPath(filePath, ".tmp"))
		return me.doCompile(file, tmpFile)
	}
	return nil
}

func (me *HtmlTpl) OpenFileStream(filename string) (XStream, error) {
	fh, fhErr := xfile.OpenRead(filename)
	if fhErr != nil {
		fh.Close()
		me.LastErrorFile = filename
		return nil, fhErr
	}

	return fh, nil
}

func (me *HtmlTpl) loadFromFile(filename string, level int) (error, string) {
	var jsFlag = false
	var isString = false
	var err error
	var c1 byte
	var jsBuffer bytes.Buffer

	level++
	me.nowLevel = level
	if level >= 10 {
		return errors.New(filename + ":loadFromFile: too deep"), ""
	}
	defer func() {
		level--
		me.nowLevel = level
		jsBuffer.Reset()
	}()
	htmlDep := new(HtmlDepend)
	me.htmlDepend[filename] = htmlDep
	var stream XStream
	stream, err = me.OpenFileStream(filename)
	if err != nil {
		return err, ""
	}
	defer stream.Close()
	for {
		c1, err = stream.Getc()
		if err != nil {
			break
		}
		//fmt.Println("C1: ", string(c1), "  js:", jsFlag)
		//fmt.Println("buffer: ", jsBuffer.String())
		switch c1 {
		case '<':
			if jsFlag {
				jsBuffer.WriteByte(c1)
				break
			}
			c1, err = stream.Getc()
			if err != nil {
				return err, ""
			}
			if c1 == '?' {
				jsFlag = true
			} else if c1 == '-' {
				line, err := stream.Gets()
				if err != nil {
					return err, ""
				} else {
					err, code := me.doSubCommand(htmlDep, filename, line, level)
					if err != nil {
						return err, code
					} else {
						continue
					}
				}
			} else {
				me.resultBuffer.WriteByte('<')
				stream.UnGetc()
			}

		case '?':
			if jsFlag == false {
				me.resultBuffer.WriteByte(c1)
				break
			}

			c1, err = stream.Getc()
			if err != nil {
				return err, ""
			}
			if c1 == '>' && isString == false {
				jsFlag = false
				//fmt.Println("jsscript:", jsBuffer.String())
				err = nil
				if err != nil {
					me.LastErrorFile = filename
					tmpStr := jsBuffer.String()
					jsBuffer.Reset()
					return err, tmpStr
				}
				jsBuffer.Reset()
			} else {
				jsBuffer.WriteByte('?')
				stream.UnGetc()
			}

		case '\\':
			if jsFlag {
				jsBuffer.WriteByte(c1)
			} else {
				me.resultBuffer.WriteByte(c1)
			}

			c1, err = stream.Getc()
			if err != nil {
				break
			}
			if jsFlag {
				jsBuffer.WriteByte(c1)
			} else {
				me.resultBuffer.WriteByte(c1)
			}

		case '"':
			if jsFlag {
				isString = !isString
				jsBuffer.WriteByte(c1)
			} else {
				me.resultBuffer.WriteByte(c1)
			}

		default:
			if jsFlag {
				jsBuffer.WriteByte(c1)
			} else {
				me.resultBuffer.WriteByte(c1)
			}
		}
	}
	fmt.Println(jsBuffer.String())
	err = nil
	if err != nil {
		me.LastErrorFile = filename
		tmpStr := jsBuffer.String()
		jsBuffer.Reset()
		return err, tmpStr
	}
	jsBuffer.Reset()

	return nil, ""
}

func (me *HtmlTpl) doSubCommand(htmlDep *HtmlDepend, parentFile string, line string, level int) (error, string) {
	//fmt.Println("doSubCommand:", line, " Level:", level)
	orgLine := line
	line = strings.Trim(line, " \t\r\n")
	if line == "" || len(line) <= 2 {
		return errors.New("SubCommand is invalid"), ""
	}
	l := len(line)
	if line[l-2] != '-' || line[l-1] != '>' {
		me.resultBuffer.WriteString("<-")
		me.resultBuffer.WriteString(orgLine)
		return nil, ""
	}
	line = line[:l-2]
	index := strings.IndexByte(line, ' ')
	if index <= 0 {
		return me.loadFromFile(xstring.ConnPath(me.RootDir, line), level)
	}
	act := line[:index]
	arg := line[index+1:]
	arg = strings.Trim(arg, " \t\r\n")
	switch act {
	case "include":
		index = strings.IndexByte(arg, ' ')
		inclFile := arg
		if index > 0 {
			inclFile = arg[:index]
			arg = arg[index+1:]
			items := strings.Split(arg, "=")
			if len(items) >= 2 && me.Env != nil {
				name := strings.Trim(items[0], " \t\r\n")
				value := strings.Trim(items[1], " \t\r\n")
				v, ok := me.GetEnvVar(name)
				if ok == false {
					dbg.Dbg("Not found Env: %s\n", name)
					return nil, ""
				} else {
					if value != v {
						dbg.Dbg("Env: %s not match Value, %s,%+v\n", name, value, v)
						return nil, ""
					}
				}
			}
		}
		tmpFile := xstring.ConnPath(xstring.GetFilePath(parentFile), inclFile)
		if xfile.FileExist(tmpFile) == false {
			tmpFile = xstring.ConnPath(me.RootDir, inclFile)
		}
		if xfile.FileExist(tmpFile) == false {
			dbg.Dbg("include file %s not found by %s\n", inclFile, parentFile)
		}
		htmlDep.files = append(htmlDep.files, tmpFile)
		return me.loadFromFile(tmpFile, level)

	case "include2":
		me.loadFromFile(xstring.ConnPath(me.RootDir, arg), level)
		return nil, ""

	case "include_once":
		return me.loadFromFile(xstring.ConnPath(me.RootDir, arg), level)

	default:
		if act != "" && act[0] == '$' {
			act = act[1:]
		}
		v, ok := me.GetEnvVar(act)
		if ok {
			me.resultBuffer.WriteString(v)
			return nil, ""
		}
		dbg.Dbg("unknown %s\n", act)
	}
	return nil, ""
}
